<?php 

$server = "localhost";
$mysql_user = "root";
$mysql_pass = "";
$db_name = "smgts_db";

if($_POST['submitconfig']) {

	$server = $_POST['server'];
	$mysql_user = $_POST['user'];
	$mysql_pass = $_POST['pass'];
	$db_name = $_POST['dbname'];
    
    $File = "connection.php"; 
    $Handle = fopen($File, 'w');
	
	$Data = "<?php\n
	\$server = \"$server\";\n
	\$mysql_user = \"$mysql_user\";\n
	\$mysql_pass = \"$mysql_pass\";\n
	\$db_name = \"$db_name\";\n
	\$con = mysql_connect(\"\$server\",\"\$mysql_user\",\"\$mysql_pass\");\n
	if (!\$con) { die('Could not connect: ' . mysql_error()); }\n
	mysql_select_db(\"\$db_name\", \$con);
	\n?>"; 

    fwrite($Handle, $Data); 
	
    fclose($Handle); 
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Project Novo SMGTS (For Spurgeon School) Pre-requisite Config</title>
<style type="text/css">
body {
	font-family:Tahoma, Geneva, sans-serif;
	font-size:12px;	
	color:#666;
}
hr {
	border:none;
	height:1px;
	background-color:#CCC;
	margin-top:0px;	
}
#content {
	text-align:justify;
	line-height:18px;
}
#title {
	margin-bottom:2px;	
	font-size:18px;
	font-family:Georgia, "Times New Roman", Times, serif;
}
#fields td {
	padding:6px;
	background-color:#ededed;
	border-top:8px solid #FFF;
}
</style>
</head>

<body topmargin="0" leftmargin="0" rightmargin="0">
<?php
if($_POST['submitconfig']) {
?>
<div style="background-color:#70ff4f; margin:0px; text-align:center; padding:10px; border:1px solid #CCC">Connection File Updated. Please delete config.php in the smgts directory. Afterwards, you may proceed <a href="index.php">here</a></div>
<?php
}
?>
<div style="width:800px; margin-left:auto; margin-right:auto; margin-top:25px">
<center><img src="images/configsmgtslogo.png" /><br /><br /></center>
<p id="title">Welcome</p>
<hr />
<p id="content">Welcome to the Project Novo SMGTS v1.1 configuration page. It is recommended that the current user has knowledge with regards to PHP and MySQL in order for this installation process to be of success. All you have to do is to fill in the neccessary information below and you'll be on your way to using the (Spurgeon) School Management System.</p>
<p id="title">Information Needed</p>
<hr />
<p id="content">Please provide the following information. Don't worry, you can change these settings later.</p>
<form method="post">
<table id="fields" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td width="200px"><b>Server</b></td><td><input type="text" name="server" value="<?php echo $server; ?>" /></td>
</tr>
<tr>
<td><b>MySQL Username</b></td><td><input type="text" name="user" value="<?php echo $mysql_user; ?>" /></td>
</tr>
<tr>
<td><b>MySQL Password</b></td><td><input type="password" name="pass" value="<?php echo $mysql_pass; ?>" /></td>
</tr>
<tr>
<td><b>Database Name</b></td><td><input type="text" name="dbname" value="<?php echo $db_name; ?>" /></td>
</tr>
</table><br />

<input type="submit" name="submitconfig" value="Configure Connection">
</form>
</div>
</body>
</html>
