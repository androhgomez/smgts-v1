-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 23, 2013 at 09:12 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `smgts`
--

-- --------------------------------------------------------

--
-- Table structure for table `bulletin`
--

CREATE TABLE IF NOT EXISTS `bulletin` (
  `bulletin_num` int(11) NOT NULL,
  `posted_date` date NOT NULL,
  `posted_time` time NOT NULL,
  `employee_id` varchar(9) NOT NULL,
  `title` varchar(200) NOT NULL,
  `bulletin_message` text NOT NULL,
  PRIMARY KEY (`bulletin_num`),
  KEY `employee_id` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bulletin`
--

INSERT INTO `bulletin` (`bulletin_num`, `posted_date`, `posted_time`, `employee_id`, `title`, `bulletin_message`) VALUES
(1, '2013-01-04', '19:53:16', '2011-0001', 'Welcome to SMGTS!', 'SMGTS or School Management System is an application to be deployed on a local server. The core features are the following: <br />\r\n<br /><br />\r\n<ul><br />\r\n<li>Grade System</li><br />\r\n<li>Tuition Management</li><br />\r\n<li>Class and Loading Management</li><br />\r\n</ul>'),
(2, '2013-01-04', '19:54:06', '2011-0001', 'Welcome to SMGTS!', 'A project still under development.'),
(3, '2013-01-08', '20:18:50', '2011-0001', 'Welcome to SMGTS!', 'This is the Spurgeon School Management System created by Project Novo.<br />\r\n<br />\r\nIt avails the following:<br />\r\n- Assessment and Enrollment of Students<br />\r\n- Scheduling of Classes<br />\r\n- Detailed Progress Reports regarding subjects'),
(4, '2013-01-29', '06:51:22', '2011-0001', 'Annouyncement', 'Not final grade yet');

-- --------------------------------------------------------

--
-- Table structure for table `bulletin_allow`
--

CREATE TABLE IF NOT EXISTS `bulletin_allow` (
  `bulletin_num` int(11) NOT NULL,
  `group_name` varchar(45) NOT NULL,
  PRIMARY KEY (`bulletin_num`,`group_name`),
  KEY `group_name` (`group_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bulletin_allow`
--

INSERT INTO `bulletin_allow` (`bulletin_num`, `group_name`) VALUES
(3, 'Guardian'),
(4, 'Guardian'),
(3, 'Registrar'),
(3, 'System Admin'),
(4, 'System Admin'),
(3, 'Teacher'),
(4, 'Teacher');

-- --------------------------------------------------------

--
-- Table structure for table `category_percentage`
--

CREATE TABLE IF NOT EXISTS `category_percentage` (
  `subject_code` varchar(45) NOT NULL,
  `dep_code` varchar(45) NOT NULL,
  `category` varchar(45) NOT NULL,
  `percentage` decimal(5,2) NOT NULL,
  PRIMARY KEY (`subject_code`,`dep_code`,`category`),
  KEY `dep_code` (`dep_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_percentage`
--

INSERT INTO `category_percentage` (`subject_code`, `dep_code`, `category`, `percentage`) VALUES
('Math Grade 1', 'Mathematics', 'Exam', 40.00),
('Math Grade 1', 'Mathematics', 'HW', 10.00),
('Math Grade 1', 'Mathematics', 'Project', 15.00),
('Math Grade 1', 'Mathematics', 'Recitation', 25.00),
('Math Grade 1', 'Mathematics', 'SW', 10.00),
('WORLITT 5', 'Philosophy and Literature', 'Attendance', 5.00),
('WORLITT 5', 'Philosophy and Literature', 'Homework', 15.00),
('WORLITT 5', 'Philosophy and Literature', 'Mid Quarter Exam', 20.00),
('WORLITT 5', 'Philosophy and Literature', 'Participation', 10.00),
('WORLITT 5', 'Philosophy and Literature', 'Projects', 30.00),
('WORLITT 5', 'Philosophy and Literature', 'Quarter Exam', 20.00);

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE IF NOT EXISTS `class` (
  `class_name` varchar(45) NOT NULL,
  `level_id` varchar(45) NOT NULL,
  `class_adviser` varchar(9) DEFAULT NULL,
  `co_adviser` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`class_name`,`level_id`),
  KEY `class_adviser` (`class_adviser`),
  KEY `co_adviser` (`co_adviser`),
  KEY `level_id` (`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`class_name`, `level_id`, `class_adviser`, `co_adviser`) VALUES
('2-A', 'Grade 2', NULL, NULL),
('3-A', 'Grade 3', NULL, NULL),
('4-A', 'Grade 4', NULL, NULL),
('St. John', 'Grade 1', NULL, NULL),
('St. Peter', 'Grade 1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `class_level`
--

CREATE TABLE IF NOT EXISTS `class_level` (
  `level_id` varchar(45) NOT NULL,
  `category` int(1) DEFAULT NULL,
  `indexnum` int(1) NOT NULL,
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_level`
--

INSERT INTO `class_level` (`level_id`, `category`, `indexnum`) VALUES
('Grade 1', 1, 1),
('Grade 2', 1, 2),
('Grade 3', 1, 3),
('Grade 4', 1, 8),
('Grade 5', 1, 5),
('Grade 6', 1, 6),
('Grade 7', 1, 7);

-- --------------------------------------------------------

--
-- Table structure for table `connection`
--

CREATE TABLE IF NOT EXISTS `connection` (
  `student_id` varchar(9) NOT NULL,
  `guardian_id` int(11) NOT NULL,
  `relationship` varchar(45) NOT NULL,
  `user_name` varchar(45) NOT NULL,
  PRIMARY KEY (`student_id`,`guardian_id`,`relationship`),
  KEY `guardian_id` (`guardian_id`),
  KEY `user_name` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `connection`
--

INSERT INTO `connection` (`student_id`, `guardian_id`, `relationship`, `user_name`) VALUES
('1516-0001', 3, 'father', 'archenemy41'),
('1516-0001', 3, 'guardian', 'archenemy41'),
('1516-0001', 3, 'mother', 'archenemy41'),
('1314-0002', 2, 'father', 'hpotter'),
('1314-0002', 2, 'guardian', 'hpotter'),
('1314-0002', 2, 'mother', 'hpotter'),
('1314-0001', 1, 'father', 'limsiacofamily'),
('1314-0001', 1, 'guardian', 'limsiacofamily'),
('1314-0001', 1, 'mother', 'limsiacofamily');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `dep_code` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `unofficial` varchar(3) NOT NULL,
  PRIMARY KEY (`dep_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`dep_code`, `description`, `unofficial`) VALUES
('Mathematics', '', 'no'),
('Philosophy and Literature', 'Philosophy and Literature courses for higher years', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `employee_id` varchar(9) NOT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `mname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `address_home` varchar(200) DEFAULT NULL,
  `address_mail` varchar(200) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `citizenship` varchar(45) DEFAULT NULL,
  `civil_status` varchar(45) DEFAULT NULL,
  `spouse` varchar(45) DEFAULT NULL,
  `sss_number` varchar(12) DEFAULT NULL,
  `pagibig_number` varchar(45) DEFAULT NULL,
  `philhealth_number` varchar(45) DEFAULT NULL,
  `TIN` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `user_name` varchar(45) NOT NULL,
  PRIMARY KEY (`employee_id`),
  KEY `user_name` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`employee_id`, `fname`, `mname`, `lname`, `birthdate`, `gender`, `address_home`, `address_mail`, `email`, `citizenship`, `civil_status`, `spouse`, `sss_number`, `pagibig_number`, `philhealth_number`, `TIN`, `image`, `user_name`) VALUES
('2011-0001', 'Gerard Igmedio', 'Sorrera', 'Cruz', '1992-03-25', 'm', 'Pasay City', 'VAB, Pasay City', 'Filipino', 'Filipino', 'single', '', '', '', '', '', 'emp_img/gerard.jpg', 'admin'),
('2013-0001', 'Kristina Teresa', 'Sorrera', 'Cruz', '1990-12-15', 'f', '9006-70 SDU Recto Extension, Camp Colonel Jesus Villamor Air Base, Pasay City', '9006-70 SDU Recto Extension, Camp Colonel Jesus Villamor Air Base, Pasay City', 'Filipino', 'Filipino', 'single', '', '', '', '', '', 'emp_img/noimage.jpg', 'kristinascruz');

-- --------------------------------------------------------

--
-- Table structure for table `enroll`
--

CREATE TABLE IF NOT EXISTS `enroll` (
  `student_id` varchar(9) NOT NULL,
  `school_year` varchar(45) NOT NULL,
  `class_name` varchar(45) NOT NULL,
  `level_id` varchar(45) NOT NULL,
  `estatus` varchar(45) DEFAULT NULL,
  `payment_option` varchar(45) DEFAULT NULL,
  `date_enrolled` date NOT NULL,
  PRIMARY KEY (`student_id`,`school_year`),
  KEY `class_name` (`class_name`),
  KEY `level_id` (`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enroll`
--

INSERT INTO `enroll` (`student_id`, `school_year`, `class_name`, `level_id`, `estatus`, `payment_option`, `date_enrolled`) VALUES
('1314-0001', '2013-2014', 'St. John', 'Grade 1', 'enrolled', 'quarterly', '2013-01-22'),
('1314-0001', '2014-2015', 'St. John', 'Grade 1', 'enrolled', 'annual', '2013-03-01'),
('1314-0002', '2013-2014', 'St. John', 'Grade 1', 'enrolled', 'annual', '2013-03-01'),
('1314-0002', '2014-2015', 'St. John', 'Grade 1', 'enrolled', 'annual', '2013-03-01'),
('1314-0002', '2015-2016', '3-A', 'Grade 3', 'enrolled', 'annual', '2013-03-02'),
('1314-0002', '2016-2017', '4-A', 'Grade 4', 'assessed', 'annual', '2013-03-03');

-- --------------------------------------------------------

--
-- Table structure for table `fees`
--

CREATE TABLE IF NOT EXISTS `fees` (
  `fee_id` int(11) NOT NULL,
  `level_id` varchar(45) DEFAULT NULL,
  `fee_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`fee_id`),
  KEY `level_id` (`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fees`
--

INSERT INTO `fees` (`fee_id`, `level_id`, `fee_type`) VALUES
(1, 'Grade 1', 'tuition'),
(2, 'Grade 1', 'tuition'),
(3, 'Grade 1', 'tuition'),
(4, 'Grade 1', 'other'),
(5, 'Grade 1', 'other'),
(6, 'Grade 1', 'other'),
(7, 'Grade 6', 'tuition'),
(8, 'Grade 6', 'tuition'),
(9, 'Grade 6', 'tuition'),
(10, 'Grade 3', 'tuition'),
(11, 'Grade 3', 'tuition'),
(12, 'Grade 3', 'tuition'),
(13, 'Grade 4', 'tuition'),
(14, 'Grade 4', 'tuition'),
(15, 'Grade 4', 'tuition');

-- --------------------------------------------------------

--
-- Table structure for table `gradeitems`
--

CREATE TABLE IF NOT EXISTS `gradeitems` (
  `level_id` varchar(45) NOT NULL,
  `class_name` varchar(45) NOT NULL,
  `subject_code` varchar(45) NOT NULL,
  `gi_code` varchar(45) NOT NULL,
  `quarter` int(11) NOT NULL,
  `school_year` varchar(9) NOT NULL,
  `category` varchar(45) NOT NULL,
  PRIMARY KEY (`level_id`,`class_name`,`subject_code`,`gi_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gradeitems`
--

INSERT INTO `gradeitems` (`level_id`, `class_name`, `subject_code`, `gi_code`, `quarter`, `school_year`, `category`) VALUES
('Grade 1', 'St. John', 'Math Grade 1', '1', 1, '2013-2014', 'HW'),
('Grade 1', 'St. John', 'Math Grade 1', '2', 1, '2013-2014', 'HW'),
('Grade 1', 'St. John', 'WORLITT 5', 'Final Exam', 1, '2013-2014', 'Quarter Exam'),
('Grade 1', 'St. John', 'WORLITT 5', 'HW1', 1, '2013-2014', 'Homework'),
('Grade 1', 'St. John', 'WORLITT 5', 'HW2', 1, '2013-2014', 'Homework'),
('Grade 1', 'St. John', 'WORLITT 5', 'HW3', 1, '2013-2014', 'Homework'),
('Grade 1', 'St. John', 'WORLITT 5', 'HW4', 1, '2013-2014', 'Homework'),
('Grade 1', 'St. John', 'WORLITT 5', 'HW5', 1, '2013-2014', 'Homework'),
('Grade 1', 'St. John', 'WORLITT 5', 'Mid-Exam', 1, '2013-2014', 'Mid Quarter Exam'),
('Grade 1', 'St. John', 'WORLITT 5', 'Participation', 1, '2013-2014', 'Participation'),
('Grade 1', 'St. John', 'WORLITT 5', 'PR1', 1, '2013-2014', 'Projects'),
('Grade 1', 'St. John', 'WORLITT 5', 'PR2', 1, '2013-2014', 'Projects'),
('Grade 1', 'St. John', 'WORLITT 5', 'PR3', 1, '2013-2014', 'Projects'),
('Grade 1', 'St. John', 'WORLITT 5', 'PR4', 1, '2013-2014', 'Projects'),
('Grade 1', 'St. John', 'WORLITT 5', 'Total Attendance', 1, '2013-2014', 'Attendance');

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE IF NOT EXISTS `grades` (
  `subject_code` varchar(45) NOT NULL,
  `student_id` varchar(45) NOT NULL,
  `school_year` varchar(45) NOT NULL,
  `quarter` int(1) NOT NULL,
  `gi_code` varchar(45) NOT NULL,
  `student_grade` decimal(5,2) NOT NULL,
  PRIMARY KEY (`subject_code`,`student_id`,`school_year`,`quarter`,`gi_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`subject_code`, `student_id`, `school_year`, `quarter`, `gi_code`, `student_grade`) VALUES
('Math Grade 1', '1314-0001', '2013-2014', 1, '1', 60.00),
('WORLITT 5', '1314-0001', '2013-2014', 1, 'Final Exam', 100.00),
('WORLITT 5', '1314-0001', '2013-2014', 1, 'HW1', 100.00),
('WORLITT 5', '1314-0001', '2013-2014', 1, 'HW2', 97.00),
('WORLITT 5', '1314-0001', '2013-2014', 1, 'HW3', 100.00),
('WORLITT 5', '1314-0001', '2013-2014', 1, 'HW4', 100.00),
('WORLITT 5', '1314-0001', '2013-2014', 1, 'HW5', 95.00),
('WORLITT 5', '1314-0001', '2013-2014', 1, 'Mid-Exam', 76.00),
('WORLITT 5', '1314-0001', '2013-2014', 1, 'Participation', 75.00),
('WORLITT 5', '1314-0001', '2013-2014', 1, 'PR1', 97.00),
('WORLITT 5', '1314-0001', '2013-2014', 1, 'PR2', 88.00),
('WORLITT 5', '1314-0001', '2013-2014', 1, 'PR3', 97.00),
('WORLITT 5', '1314-0001', '2013-2014', 1, 'PR4', 87.00),
('WORLITT 5', '1314-0001', '2013-2014', 1, 'Total Attendance', 98.00);

-- --------------------------------------------------------

--
-- Table structure for table `group_permissions`
--

CREATE TABLE IF NOT EXISTS `group_permissions` (
  `group_name` varchar(45) NOT NULL,
  `is_VIP` int(11) NOT NULL,
  `control_add` int(11) DEFAULT NULL,
  `control_edit` int(11) DEFAULT NULL,
  `control_delete` int(11) DEFAULT NULL,
  `ref_students` int(11) DEFAULT NULL,
  `ref_guardian` int(11) DEFAULT NULL,
  `ref_employee` int(11) DEFAULT NULL,
  `ref_update_img` int(11) NOT NULL,
  `ref_assess_enroll` int(11) NOT NULL,
  `ref_department` int(11) DEFAULT NULL,
  `ref_class` int(11) DEFAULT NULL,
  `ref_class_level` int(11) DEFAULT NULL,
  `ref_fees` int(11) DEFAULT NULL,
  `ref_payment_dues` int(11) DEFAULT NULL,
  `ref_subject_list` int(11) DEFAULT NULL,
  `ref_schedule` int(11) DEFAULT NULL,
  `ref_grade_item` int(11) DEFAULT NULL,
  `ref_grade_item_type` int(11) DEFAULT NULL,
  `ref_grade` int(11) DEFAULT NULL,
  `ref_connection` int(11) DEFAULT NULL,
  `ref_timeslot` int(11) NOT NULL,
  `ref_update_adviser` int(11) NOT NULL,
  `user_access` int(11) DEFAULT NULL,
  `user_groups` int(11) DEFAULT NULL,
  `sys_system_default` int(11) DEFAULT NULL,
  `sys_bulletin` int(11) NOT NULL,
  `sys_backup` int(11) NOT NULL,
  `sys_log` int(11) NOT NULL,
  `ref_all_load` int(11) NOT NULL,
  `delete_logs` int(11) NOT NULL,
  `mail_all` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_permissions`
--

INSERT INTO `group_permissions` (`group_name`, `is_VIP`, `control_add`, `control_edit`, `control_delete`, `ref_students`, `ref_guardian`, `ref_employee`, `ref_update_img`, `ref_assess_enroll`, `ref_department`, `ref_class`, `ref_class_level`, `ref_fees`, `ref_payment_dues`, `ref_subject_list`, `ref_schedule`, `ref_grade_item`, `ref_grade_item_type`, `ref_grade`, `ref_connection`, `ref_timeslot`, `ref_update_adviser`, `user_access`, `user_groups`, `sys_system_default`, `sys_bulletin`, `sys_backup`, `sys_log`, `ref_all_load`, `delete_logs`, `mail_all`) VALUES
('Guardian', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('No Access / Disabled', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0),
('Registrar', 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, NULL, NULL, 1, 0, 1, 1, 1, 0),
('System Admin', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0),
('Teacher', 0, 0, 1, 0, 0, NULL, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `guardian`
--

CREATE TABLE IF NOT EXISTS `guardian` (
  `guardian_id` int(11) NOT NULL,
  `relationship` varchar(45) NOT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `mname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `occupation` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone_landline` varchar(45) DEFAULT NULL,
  `phone_mobile` varchar(45) DEFAULT NULL,
  `emergency_contact` int(11) DEFAULT NULL,
  `living_with` int(11) DEFAULT NULL,
  PRIMARY KEY (`guardian_id`,`relationship`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guardian`
--

INSERT INTO `guardian` (`guardian_id`, `relationship`, `fname`, `mname`, `lname`, `occupation`, `email`, `phone_landline`, `phone_mobile`, `emergency_contact`, `living_with`) VALUES
(1, 'father', 'Edgar', 'Allan', 'Limsiaco', 'Seaman', 'androhgomez@gmail.com', '911-1111', '09112223333', 1, 1),
(1, 'guardian', '', '', '', '', 'andriodx77@yahoo.com', '', '', 0, 0),
(1, 'mother', 'Geraldine', 'Ricafort', 'Limsiaco', 'Accountant', '', '911-1111', '09445556666', 1, 1),
(2, 'father', 'James', 'J', 'Potter', 'wizard', '', '1289034098`', '1290-380-1239', 1, 1),
(2, 'guardian', 'Sirius', 'B', 'Black', 'dog', '', '190823908123', '-019230-9120', 1, 1),
(2, 'mother', 'Lily', 'D', 'Evans', 'witch', '', '091238901238', '0-12930-1039', 1, 1),
(3, 'father', 'first', 'middle', 'last', 'asd', 'qwkejaskld', '129038', '128390-12328', 1, 1),
(3, 'guardian', 'first', 'middle', 'last', 'asd', 'aklsjdlkajsd', '109283908', '01928390128', 1, 1),
(3, 'mother', 'first', 'middle', 'last', 'asd', 'alksjdlaksdjqw', '19028390183', '10928390183908', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `optional`
--

CREATE TABLE IF NOT EXISTS `optional` (
  `fee_id` int(11) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `cost` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`fee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `other`
--

CREATE TABLE IF NOT EXISTS `other` (
  `fee_id` int(11) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `cost` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`fee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `other`
--

INSERT INTO `other` (`fee_id`, `description`, `cost`) VALUES
(4, 'Computer Lab', '100'),
(5, 'Computer Fee', '0.0'),
(6, 'System Fee', '1000');

-- --------------------------------------------------------

--
-- Table structure for table `payment_dues`
--

CREATE TABLE IF NOT EXISTS `payment_dues` (
  `payment_type` varchar(45) NOT NULL,
  `installment` int(11) NOT NULL,
  `due_date` date NOT NULL,
  `school_year` varchar(9) NOT NULL,
  PRIMARY KEY (`payment_type`,`installment`,`school_year`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_dues`
--

INSERT INTO `payment_dues` (`payment_type`, `installment`, `due_date`, `school_year`) VALUES
('quarterly', 2, '0000-00-00', '2012-2013'),
('quarterly', 2, '2013-09-28', '2013-2014'),
('quarterly', 2, '0000-00-00', '2015-2016'),
('quarterly', 3, '0000-00-00', '2012-2013'),
('quarterly', 3, '2013-12-14', '2013-2014'),
('quarterly', 3, '0000-00-00', '2015-2016'),
('quarterly', 4, '0000-00-00', '2012-2013'),
('quarterly', 4, '2014-02-27', '2013-2014'),
('quarterly', 4, '0000-00-00', '2015-2016'),
('semi-annual', 2, '0000-00-00', '2012-2013'),
('semi-annual', 2, '2013-11-30', '2013-2014'),
('semi-annual', 2, '0000-00-00', '2015-2016');

-- --------------------------------------------------------

--
-- Table structure for table `payment_status`
--

CREATE TABLE IF NOT EXISTS `payment_status` (
  `student_id` varchar(9) NOT NULL,
  `school_year` varchar(9) NOT NULL,
  `installment` int(1) NOT NULL,
  PRIMARY KEY (`student_id`,`school_year`,`installment`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_status`
--

INSERT INTO `payment_status` (`student_id`, `school_year`, `installment`) VALUES
('1314-0001', '2013-2014', 2);

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE IF NOT EXISTS `schedule` (
  `schedule_day` varchar(15) NOT NULL,
  `level_id` varchar(45) NOT NULL,
  `class_name` varchar(45) NOT NULL,
  `timeslot_num` int(11) NOT NULL,
  `subject_code` varchar(45) NOT NULL,
  `dep_code` varchar(45) NOT NULL,
  `room_code` varchar(45) NOT NULL,
  PRIMARY KEY (`schedule_day`,`level_id`,`class_name`,`timeslot_num`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`schedule_day`, `level_id`, `class_name`, `timeslot_num`, `subject_code`, `dep_code`, `room_code`) VALUES
('friday', 'Grade 1', 'St. John', 1, 'Math Grade 1', 'Mathematics', '301'),
('monday', 'Grade 1', 'St. John', 1, 'Math Grade 1', 'Mathematics', '301'),
('monday', 'Grade 3', '3-A', 1, 'Dark Arts', 'Philosophy and Literature', 'n/a'),
('monday', 'Grade 4', '4-A', 1, 'Geometry', 'Mathematics', 'n/a'),
('thursday', 'Grade 1', 'St. John', 1, 'WORLITT 5', 'Philosophy and Literature', '666'),
('tuesday', 'Grade 1', 'St. John', 1, 'WORLITT 5', 'Philosophy and Literature', '666'),
('tuesday', 'Grade 4', '4-A', 1, 'Dark Arts', 'Philosophy and Literature', 'n/a'),
('wednesday', 'Grade 1', 'St. John', 1, 'Math Grade 1', 'Mathematics', '666');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `student_id` varchar(9) NOT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `mname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `address_home` text,
  `address_mail` text,
  `citizenship` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`student_id`, `fname`, `mname`, `lname`, `birthdate`, `gender`, `address_home`, `address_mail`, `citizenship`, `image`) VALUES
('1314-0001', 'Keiko Ellaine', 'Ricafort', 'Limsiaco', '2001-08-01', 'f', 'Parañaque City', 'Parañaque City', 'Filipino', 'student_img/noimage.jpg'),
('1314-0002', 'Harry', 'J', 'Potter', '1995-01-01', 'm', 'wedfr', 'qwed', 'qawdeqawsd', 'student_img/noimage.jpg'),
('1516-0001', 'Caelica', 'M', 'Mercury', '1995-01-01', 'f', 'qweqwe', 'qweqwe', 'qweqwe', 'student_img/noimage.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `student_has_fees`
--

CREATE TABLE IF NOT EXISTS `student_has_fees` (
  `student_id` varchar(9) NOT NULL,
  `fee_id` int(11) NOT NULL,
  `adjustments` decimal(8,2) DEFAULT NULL,
  `school_year` varchar(45) NOT NULL,
  PRIMARY KEY (`student_id`,`fee_id`,`school_year`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_has_fees`
--

INSERT INTO `student_has_fees` (`student_id`, `fee_id`, `adjustments`, `school_year`) VALUES
('1314-0001', 1, 0.00, '2013-2014'),
('1314-0001', 1, 0.00, '2014-2015'),
('1314-0001', 2, 0.00, '2013-2014'),
('1314-0001', 2, 0.00, '2014-2015'),
('1314-0001', 3, 0.00, '2013-2014'),
('1314-0001', 3, 0.00, '2014-2015'),
('1314-0001', 4, 0.00, '2013-2014'),
('1314-0001', 4, 0.00, '2014-2015'),
('1314-0001', 5, 0.00, '2014-2015'),
('1314-0001', 6, 0.00, '2014-2015'),
('1314-0002', 1, 0.00, '2013-2014'),
('1314-0002', 1, 0.00, '2014-2015'),
('1314-0002', 2, 0.00, '2013-2014'),
('1314-0002', 2, 0.00, '2014-2015'),
('1314-0002', 3, 0.00, '2013-2014'),
('1314-0002', 3, 0.00, '2014-2015'),
('1314-0002', 4, 0.00, '2013-2014'),
('1314-0002', 4, 0.00, '2014-2015'),
('1314-0002', 5, 0.00, '2013-2014'),
('1314-0002', 5, 0.00, '2014-2015'),
('1314-0002', 6, 0.00, '2013-2014'),
('1314-0002', 6, 0.00, '2014-2015'),
('1314-0002', 13, 0.00, '2016-2017'),
('1314-0002', 14, 0.00, '2016-2017'),
('1314-0002', 15, 0.00, '2016-2017');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE IF NOT EXISTS `subjects` (
  `subject_code` varchar(45) NOT NULL,
  `dep_code` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `employee_id` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`subject_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`subject_code`, `dep_code`, `description`, `employee_id`) VALUES
('Dark Arts', 'Philosophy and Literature', 'askljdskl;ajd', NULL),
('Geometry', 'Mathematics', 'Geometry', NULL),
('Math Grade 1', 'Mathematics', '', '2011-0001'),
('WORLITT 5', 'Philosophy and Literature', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `system_default`
--

CREATE TABLE IF NOT EXISTS `system_default` (
  `system_default` int(11) NOT NULL,
  `school_year` varchar(45) NOT NULL,
  `quarter` int(11) NOT NULL,
  PRIMARY KEY (`system_default`,`school_year`,`quarter`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_default`
--

INSERT INTO `system_default` (`system_default`, `school_year`, `quarter`) VALUES
(0, '2013-2014', 1);

-- --------------------------------------------------------

--
-- Table structure for table `system_logs`
--

CREATE TABLE IF NOT EXISTS `system_logs` (
  `log_num` int(11) NOT NULL,
  `module` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `employee_id` varchar(9) NOT NULL,
  `date_processed` date NOT NULL,
  `time_processed` time NOT NULL,
  PRIMARY KEY (`log_num`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_logs`
--

INSERT INTO `system_logs` (`log_num`, `module`, `description`, `employee_id`, `date_processed`, `time_processed`) VALUES
(1, 'User Groups', 'Permission "ref_class" for "Teacher" was SET to Yes', '2011-0001', '2013-01-04', '19:50:42'),
(2, 'Assess & Enroll', 'An Adjustment value of ''-50'' for Fee ID ''4'' was given to Student ID ''1314-0001''', '2011-0001', '2013-01-04', '19:58:45'),
(3, 'Assess & Enroll', 'An Adjustment value of ''-50'' for Fee ID ''4'' was given to Student ID ''1314-0001''', '2011-0001', '2013-01-04', '19:58:46'),
(4, 'Assess & Enroll', 'Student ID ''1314-0001'' - Status changed to: ''enrolled''', '2011-0001', '2013-01-04', '20:00:11'),
(5, 'Assess & Enroll', 'An Adjustment value of ''0'' for Fee ID ''4'' was given to Student ID ''1314-0001''', '2011-0001', '2013-01-22', '15:16:57'),
(6, 'Assess & Enroll', 'Student ID ''1314-0001'' - Status changed to: ''enrolled''', '2011-0001', '2013-01-22', '15:17:19'),
(7, 'Assess & Enroll', 'Student ID ''1314-0001'' - Status changed to: ''enrolled''', '2011-0001', '2013-01-22', '15:17:35'),
(8, 'Assess & Enroll', 'Student ID ''1314-0001'' - Status changed to: ''enrolled''', '2011-0001', '2013-01-22', '15:18:19'),
(9, 'Assess & Enroll', 'Student ID ''1314-0001'' - Status changed to: ''enrolled''', '2011-0001', '2013-01-22', '15:19:11'),
(10, 'Assess & Enroll', 'Student ID ''1314-0001'' - Status changed to: ''enrolled''', '2011-0001', '2013-01-22', '15:20:06'),
(11, 'Assess & Enroll', 'Student ID ''1314-0001'' - Status changed to: ''enrolled''', '2011-0001', '2013-01-22', '15:21:11'),
(12, 'Assess & Enroll', 'Student ID ''1314-0001'' - Status changed to: ''enrolled''', '2011-0001', '2013-01-22', '15:21:19'),
(13, 'Assess & Enroll', 'Student ID ''1314-0001'' - Status changed to: ''enrolled''', '2011-0001', '2013-01-22', '15:25:12'),
(14, 'Assess & Enroll', 'Student ID ''1314-0001'' - Status changed to: ''assessed''', '2011-0001', '2013-01-22', '15:27:52'),
(15, 'Assess & Enroll', 'Student ID ''1314-0001'' - Status changed to: ''enrolled''', '2011-0001', '2013-01-22', '15:27:58'),
(16, 'Assess & Enroll', 'Student ID ''1314-0001'' - Status changed to: ''assessed''', '2011-0001', '2013-01-22', '15:39:36'),
(17, 'Assess & Enroll', 'Student ID ''1314-0001'' - Status changed to: ''enrolled''', '2011-0001', '2013-01-22', '15:39:55'),
(18, 'Assess & Enroll', 'Student ID ''1314-0001'' - Status changed to: ''assessed''', '2011-0001', '2013-01-22', '15:40:02'),
(19, 'Assess & Enroll', 'Student ID ''1314-0001'' - Status changed to: ''enrolled''', '2011-0001', '2013-01-22', '15:40:35'),
(20, 'Assess & Enroll', 'Student ID ''1314-0001'' - Status changed to: ''assessed''', '2011-0001', '2013-01-22', '15:40:40'),
(21, 'Assess & Enroll', 'Student ID ''1314-0001'' - Status changed to: ''assessed''', '2011-0001', '2013-01-22', '15:42:15'),
(22, 'Assess & Enroll', 'Student ID ''1314-0001'' - Status changed to: ''assessed''', '2011-0001', '2013-01-22', '15:42:42'),
(23, 'Assess & Enroll', 'Student ID ''1314-0001'' - Status changed to: ''enrolled''', '2011-0001', '2013-01-22', '16:03:59'),
(24, 'Assess & Enroll', 'Student ID ''1314-0001'' - Status changed to: ''enrolled''', '2011-0001', '2013-02-28', '15:34:25'),
(25, 'Assess & Enroll', 'Student ID ''1314-0001'' - Status changed to: ''enrolled''', '2011-0001', '2013-03-01', '15:01:35'),
(26, 'Assess & Enroll', 'Student ID ''1314-0002'' - Status changed to: ''enrolled''', '2011-0001', '2013-03-01', '15:11:18'),
(27, 'Assess & Enroll', 'Student ID ''1314-0002'' - Status changed to: ''enrolled''', '2011-0001', '2013-03-01', '15:12:47'),
(28, 'Assess & Enroll', 'Student ID ''1314-0002'' - Status changed to: ''enrolled''', '2011-0001', '2013-03-02', '13:07:10'),
(29, 'Assess & Enroll', 'Student ID ''1314-0002'' - Status changed to: ''enrolled''', '2011-0001', '2013-03-02', '20:01:47'),
(30, 'Assess & Enroll', 'Student ID ''1314-0002'' - Status changed to: ''assessed''', '2011-0001', '2013-03-03', '08:01:20'),
(31, 'Assess & Enroll', 'Student ID ''1314-0002'' - Status changed to: ''enrolled''', '2011-0001', '2013-03-03', '08:01:25');

-- --------------------------------------------------------

--
-- Table structure for table `timeslot`
--

CREATE TABLE IF NOT EXISTS `timeslot` (
  `timeslot_num` int(11) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  PRIMARY KEY (`timeslot_num`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timeslot`
--

INSERT INTO `timeslot` (`timeslot_num`, `start_time`, `end_time`) VALUES
(1, '09:30:00', '11:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tuition`
--

CREATE TABLE IF NOT EXISTS `tuition` (
  `fee_id` int(11) NOT NULL,
  `payment_option` varchar(45) DEFAULT NULL,
  `upon_enrollment` decimal(8,2) DEFAULT NULL,
  `installment` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`fee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tuition`
--

INSERT INTO `tuition` (`fee_id`, `payment_option`, `upon_enrollment`, `installment`) VALUES
(1, 'annual', 30000.00, 0.00),
(2, 'semi_annual', 15500.00, 15500.00),
(3, 'quarterly', 10200.00, 10300.00),
(7, 'annual', 0.00, 0.00),
(8, 'semi_annual', 0.00, 0.00),
(9, 'quarterly', 0.00, 0.00),
(10, 'annual', 20000.00, 0.00),
(11, 'semi_annual', 10000.00, 10000.00),
(12, 'quarterly', 5000.00, 5000.00),
(13, 'annual', 20000.00, 0.00),
(14, 'semi_annual', 10000.00, 0.00),
(15, 'quarterly', 5000.00, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `user_access`
--

CREATE TABLE IF NOT EXISTS `user_access` (
  `user_name` varchar(45) NOT NULL,
  `user_pass` char(128) NOT NULL,
  `group_name` varchar(45) NOT NULL,
  `is_emp` int(11) NOT NULL,
  PRIMARY KEY (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_access`
--

INSERT INTO `user_access` (`user_name`, `user_pass`, `group_name`, `is_emp`) VALUES
('admin', 'ch7gWRPwJEc2g', 'System Admin', 1),
('admon', 'chvQ7Ow41U4RU', 'System Admin', 1),
('archenemy41', 'ch7gWRPwJEc2g', 'guardian', 0),
('hpotter', 'password', 'guardian', 0),
('kristinascruz', 'p@ssw0rd', 'Teacher', 1),
('limsiacofamily', 'p@ssw0rd', 'guardian', 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bulletin`
--
ALTER TABLE `bulletin`
  ADD CONSTRAINT `bulletin_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE;

--
-- Constraints for table `bulletin_allow`
--
ALTER TABLE `bulletin_allow`
  ADD CONSTRAINT `bulletin_allow_ibfk_1` FOREIGN KEY (`bulletin_num`) REFERENCES `bulletin` (`bulletin_num`) ON UPDATE CASCADE,
  ADD CONSTRAINT `bulletin_allow_ibfk_2` FOREIGN KEY (`group_name`) REFERENCES `group_permissions` (`group_name`) ON UPDATE CASCADE;

--
-- Constraints for table `category_percentage`
--
ALTER TABLE `category_percentage`
  ADD CONSTRAINT `category_percentage_ibfk_1` FOREIGN KEY (`subject_code`) REFERENCES `subjects` (`subject_code`) ON UPDATE CASCADE,
  ADD CONSTRAINT `category_percentage_ibfk_2` FOREIGN KEY (`dep_code`) REFERENCES `departments` (`dep_code`) ON UPDATE CASCADE;

--
-- Constraints for table `class`
--
ALTER TABLE `class`
  ADD CONSTRAINT `class_ibfk_2` FOREIGN KEY (`level_id`) REFERENCES `class_level` (`level_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `class_ibfk_3` FOREIGN KEY (`class_adviser`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `class_ibfk_4` FOREIGN KEY (`co_adviser`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE;

--
-- Constraints for table `connection`
--
ALTER TABLE `connection`
  ADD CONSTRAINT `connection_ibfk_1` FOREIGN KEY (`guardian_id`) REFERENCES `guardian` (`guardian_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `connection_ibfk_2` FOREIGN KEY (`user_name`) REFERENCES `user_access` (`user_name`) ON UPDATE CASCADE;

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`user_name`) REFERENCES `user_access` (`user_name`) ON UPDATE CASCADE;

--
-- Constraints for table `enroll`
--
ALTER TABLE `enroll`
  ADD CONSTRAINT `enroll_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `enroll_ibfk_2` FOREIGN KEY (`class_name`) REFERENCES `class` (`class_name`) ON UPDATE CASCADE,
  ADD CONSTRAINT `enroll_ibfk_3` FOREIGN KEY (`level_id`) REFERENCES `class` (`level_id`) ON UPDATE CASCADE;

--
-- Constraints for table `fees`
--
ALTER TABLE `fees`
  ADD CONSTRAINT `fees_ibfk_1` FOREIGN KEY (`level_id`) REFERENCES `class_level` (`level_id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
