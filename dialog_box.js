// global variables //
var TIMER = 5;
var SPEED = 10;
var WRAPPER = 'content';

// calculate the current window width //
function pageWidth() {
  return window.innerWidth != null ? window.innerWidth : document.documentElement && document.documentElement.clientWidth ? document.documentElement.clientWidth : document.body != null ? document.body.clientWidth : null;
}

// calculate the current window height //
function pageHeight() {
  return window.innerHeight != null? window.innerHeight : document.documentElement && document.documentElement.clientHeight ? document.documentElement.clientHeight : document.body != null? document.body.clientHeight : null;
}

// calculate the current window vertical offset //
function topPosition() {
  return typeof window.pageYOffset != 'undefined' ? window.pageYOffset : document.documentElement && document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop ? document.body.scrollTop : 0;
}

// calculate the position starting at the left of the window //
function leftPosition() {
  return typeof window.pageXOffset != 'undefined' ? window.pageXOffset : document.documentElement && document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft ? document.body.scrollLeft : 0;
}

function cFL(string)
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function showList()
{
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("listalllevels").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","listlevels.php",true);
xmlhttp.send();
}


// build/show the dialog box, populate the data and call the fadeDialog function //
function showDialog(title,message,type,autohide) {
  if(!type) {
    type = 'error';
  }
  var dialog;
  var dialogheader;
  var dialogclose;
  var dialogtitle;
  var dialogcontent;
  var dialogmask;
  if(!document.getElementById('dialog')) {
    dialog = document.createElement('div');
    dialog.id = 'dialog';
    dialogheader = document.createElement('div');
    dialogheader.id = 'dialog-header';
    dialogtitle = document.createElement('div');
    dialogtitle.id = 'dialog-title';
    dialogclose = document.createElement('div');
    dialogclose.id = 'dialog-close'
    dialogcontent = document.createElement('div');
    dialogcontent.id = 'dialog-content';
    dialogmask = document.createElement('div');
    dialogmask.id = 'dialog-mask';
    document.body.appendChild(dialogmask);
    document.body.appendChild(dialog);
    dialog.appendChild(dialogheader);
    dialogheader.appendChild(dialogtitle);
    dialogheader.appendChild(dialogclose);
    dialog.appendChild(dialogcontent);;
    dialogclose.setAttribute('onclick','hideDialog()');
    dialogclose.onclick = hideDialog;
  } else {
    dialog = document.getElementById('dialog');
    dialogheader = document.getElementById('dialog-header');
    dialogtitle = document.getElementById('dialog-title');
    dialogclose = document.getElementById('dialog-close');
    dialogcontent = document.getElementById('dialog-content');
    dialogmask = document.getElementById('dialog-mask');
    dialogmask.style.visibility = "visible";
    dialog.style.visibility = "visible";
  }
  dialog.style.opacity = .00;
  dialog.style.filter = 'alpha(opacity=0)';
  dialog.alpha = 0;
  var width = pageWidth();
  var height = pageHeight();
  var left = leftPosition();
  var top = topPosition();
  var dialogwidth = dialog.offsetWidth;
  var dialogheight = dialog.offsetHeight;
  var topposition = top + (height / 3) - (dialogheight / 2);
  var leftposition = left + (width / 2) - (dialogwidth / 2);
  dialog.style.top = topposition + "px";
  dialog.style.left = leftposition + "px";
  dialogheader.className = type + "header";
  dialogtitle.innerHTML = title;
  dialogcontent.className = type;
  dialogcontent.innerHTML = message; // NORMAL MESSAGE
  var content = document.getElementById(WRAPPER);
  dialogmask.style.height = content.offsetHeight + 'px';
  dialog.timer = setInterval("fadeDialog(1)", TIMER);
  if(autohide) {
    dialogclose.style.visibility = "hidden";
    window.setTimeout("hideDialog()", (autohide * 1000));
  } else {
    dialogclose.style.visibility = "visible";
  }
}

function changetuition(clevel,feeid,option,part) {
  if(part == 0) {
	part = "Upon Enrollment";
  }
  else {
    part = "Installment";
  }
  switch(option)
  {
	case 0:
	option = "Annual";
	break;
	case 1:
	option = "Semi-Annual";
	break;
	case 2:
	option = "Quarterly";
	
	break;
  }
  var title = 'Change Amount (Tuition)';
  var type = 'prompt';
  var message = "Change Amount: "+cFL(option)+" : "+part;
  var dialog;
  var dialogheader;
  var dialogclose;
  var dialogtitle;
  var dialogcontent;
  var dialogmask;
  if(!document.getElementById('dialog')) {
    dialog = document.createElement('div');
    dialog.id = 'dialog';
    dialogheader = document.createElement('div');
    dialogheader.id = 'dialog-header';
    dialogtitle = document.createElement('div');
    dialogtitle.id = 'dialog-title';
    dialogclose = document.createElement('div');
    dialogclose.id = 'dialog-close'
    dialogcontent = document.createElement('div');
    dialogcontent.id = 'dialog-content';
    dialogmask = document.createElement('div');
    dialogmask.id = 'dialog-mask';
    document.body.appendChild(dialogmask);
    document.body.appendChild(dialog);
    dialog.appendChild(dialogheader);
    dialogheader.appendChild(dialogtitle);
    dialogheader.appendChild(dialogclose);
    dialog.appendChild(dialogcontent);;
    dialogclose.setAttribute('onclick','hideDialog()');
    dialogclose.onclick = hideDialog;
  } else {
    dialog = document.getElementById('dialog');
    dialogheader = document.getElementById('dialog-header');
    dialogtitle = document.getElementById('dialog-title');
    dialogclose = document.getElementById('dialog-close');
    dialogcontent = document.getElementById('dialog-content');
    dialogmask = document.getElementById('dialog-mask');
    dialogmask.style.visibility = "visible";
    dialog.style.visibility = "visible";
  }
  dialog.style.opacity = .00;
  dialog.style.filter = 'alpha(opacity=0)';
  dialog.alpha = 0;
  var width = pageWidth();
  var height = pageHeight();
  var left = leftPosition();
  var top = topPosition();
  var dialogwidth = dialog.offsetWidth;
  var dialogheight = dialog.offsetHeight;
  var topposition = top + (height / 3) - (dialogheight / 2);
  var leftposition = left + (width / 2) - (dialogwidth / 2);
  dialog.style.top = topposition + "px";
  dialog.style.left = leftposition + "px";
  dialogheader.className = type + "header";
  dialogtitle.innerHTML = title;
  dialogcontent.className = type;
  dialogcontent.innerHTML = message + "<br /><br />\
	<form method='GET'>\
	<input type='text' name='changeval' />\
	<input type='hidden' name='level' value='"+clevel+"' />\
	<input type='hidden' name='feeid' value='"+feeid+"' />\
	<input type='hidden' name='part' value='"+part+"' />\
	<input type='submit' name='changetuition' value='Submit'>\
	</form>"; // NORMAL MESSAGE
  var content = document.getElementById(WRAPPER);
  dialogmask.style.height = content.offsetHeight + 'px';
  dialog.timer = setInterval("fadeDialog(1)", TIMER);
  dialogclose.style.visibility = "visible";

}

function addnewfee(clevel,feetype) {
  var title = 'Add New Fee (Others)';
  var message = 'Input a New Name';
  var type = 'prompt';
  var dialog;
  var dialogheader;
  var dialogclose;
  var dialogtitle;
  var dialogcontent;
  var dialogmask;
  if(!document.getElementById('dialog')) {
    dialog = document.createElement('div');
    dialog.id = 'dialog';
    dialogheader = document.createElement('div');
    dialogheader.id = 'dialog-header';
    dialogtitle = document.createElement('div');
    dialogtitle.id = 'dialog-title';
    dialogclose = document.createElement('div');
    dialogclose.id = 'dialog-close'
    dialogcontent = document.createElement('div');
    dialogcontent.id = 'dialog-content';
    dialogmask = document.createElement('div');
    dialogmask.id = 'dialog-mask';
    document.body.appendChild(dialogmask);
    document.body.appendChild(dialog);
    dialog.appendChild(dialogheader);
    dialogheader.appendChild(dialogtitle);
    dialogheader.appendChild(dialogclose);
    dialog.appendChild(dialogcontent);;
    dialogclose.setAttribute('onclick','hideDialog()');
    dialogclose.onclick = hideDialog;
  } else {
    dialog = document.getElementById('dialog');
    dialogheader = document.getElementById('dialog-header');
    dialogtitle = document.getElementById('dialog-title');
    dialogclose = document.getElementById('dialog-close');
    dialogcontent = document.getElementById('dialog-content');
    dialogmask = document.getElementById('dialog-mask');
    dialogmask.style.visibility = "visible";
    dialog.style.visibility = "visible";
  }
  dialog.style.opacity = .00;
  dialog.style.filter = 'alpha(opacity=0)';
  dialog.alpha = 0;
  var width = pageWidth();
  var height = pageHeight();
  var left = leftPosition();
  var top = topPosition();
  var dialogwidth = dialog.offsetWidth;
  var dialogheight = dialog.offsetHeight;
  var topposition = top + (height / 3) - (dialogheight / 2);
  var leftposition = left + (width / 2) - (dialogwidth / 2);
  dialog.style.top = topposition + "px";
  dialog.style.left = leftposition + "px";
  dialogheader.className = type + "header";
  dialogtitle.innerHTML = title;
  dialogcontent.className = type;
  dialogcontent.innerHTML = message + "<br /><br />\
	<form method='GET'>\
	<input type='text' name='newotherfee' />\
	<input type='hidden' name='level' value='"+clevel+"' />\
	<input type='hidden' name='feetype' value='"+feetype+"' />\
	<input type='submit' name='submitnewfee' value='Submit'>\
	</form>"; // NORMAL MESSAGE
  var content = document.getElementById(WRAPPER);
  dialogmask.style.height = content.offsetHeight + 'px';
  dialog.timer = setInterval("fadeDialog(1)", TIMER);
  dialogclose.style.visibility = "visible";
}

function changeofee(clevel,feetype,feeid,feename) {
  var title = 'Change Amount (' + feetype + ")";
  var message = 'Change Amount For : ' + feename;
  var type = 'prompt';
  var dialog;
  var dialogheader;
  var dialogclose;
  var dialogtitle;
  var dialogcontent;
  var dialogmask;
  if(!document.getElementById('dialog')) {
    dialog = document.createElement('div');
    dialog.id = 'dialog';
    dialogheader = document.createElement('div');
    dialogheader.id = 'dialog-header';
    dialogtitle = document.createElement('div');
    dialogtitle.id = 'dialog-title';
    dialogclose = document.createElement('div');
    dialogclose.id = 'dialog-close'
    dialogcontent = document.createElement('div');
    dialogcontent.id = 'dialog-content';
    dialogmask = document.createElement('div');
    dialogmask.id = 'dialog-mask';
    document.body.appendChild(dialogmask);
    document.body.appendChild(dialog);
    dialog.appendChild(dialogheader);
    dialogheader.appendChild(dialogtitle);
    dialogheader.appendChild(dialogclose);
    dialog.appendChild(dialogcontent);;
    dialogclose.setAttribute('onclick','hideDialog()');
    dialogclose.onclick = hideDialog;
  } else {
    dialog = document.getElementById('dialog');
    dialogheader = document.getElementById('dialog-header');
    dialogtitle = document.getElementById('dialog-title');
    dialogclose = document.getElementById('dialog-close');
    dialogcontent = document.getElementById('dialog-content');
    dialogmask = document.getElementById('dialog-mask');
    dialogmask.style.visibility = "visible";
    dialog.style.visibility = "visible";
  }
  dialog.style.opacity = .00;
  dialog.style.filter = 'alpha(opacity=0)';
  dialog.alpha = 0;
  var width = pageWidth();
  var height = pageHeight();
  var left = leftPosition();
  var top = topPosition();
  var dialogwidth = dialog.offsetWidth;
  var dialogheight = dialog.offsetHeight;
  var topposition = top + (height / 3) - (dialogheight / 2);
  var leftposition = left + (width / 2) - (dialogwidth / 2);
  dialog.style.top = topposition + "px";
  dialog.style.left = leftposition + "px";
  dialogheader.className = type + "header";
  dialogtitle.innerHTML = title;
  dialogcontent.className = type;
  dialogcontent.innerHTML = message + "<br /><br />\
	<form method='GET'>\
	<input type='text' name='feeamount' />\
	<input type='hidden' name='level' value='"+clevel+"' />\
	<input type='hidden' name='feetype' value='"+feetype+"' />\
	<input type='hidden' name='feeid' value='"+feeid+"' />\
	<input type='submit' name='changemisc' value='Submit'>\
	</form>"; // NORMAL MESSAGE
  var content = document.getElementById(WRAPPER);
  dialogmask.style.height = content.offsetHeight + 'px';
  dialog.timer = setInterval("fadeDialog(1)", TIMER);
  dialogclose.style.visibility = "visible";
}

function deleteother(clevel,feeid,feename,feetype) {
  var title = 'Delete Fee (Other)';
  var message = 'Delete : ' + feename;
  var type = 'Warning';
  var dialog;
  var dialogheader;
  var dialogclose;
  var dialogtitle;
  var dialogcontent;
  var dialogmask;
  if(!document.getElementById('dialog')) {
    dialog = document.createElement('div');
    dialog.id = 'dialog';
    dialogheader = document.createElement('div');
    dialogheader.id = 'dialog-header';
    dialogtitle = document.createElement('div');
    dialogtitle.id = 'dialog-title';
    dialogclose = document.createElement('div');
    dialogclose.id = 'dialog-close'
    dialogcontent = document.createElement('div');
    dialogcontent.id = 'dialog-content';
    dialogmask = document.createElement('div');
    dialogmask.id = 'dialog-mask';
    document.body.appendChild(dialogmask);
    document.body.appendChild(dialog);
    dialog.appendChild(dialogheader);
    dialogheader.appendChild(dialogtitle);
    dialogheader.appendChild(dialogclose);
    dialog.appendChild(dialogcontent);;
    dialogclose.setAttribute('onclick','hideDialog()');
    dialogclose.onclick = hideDialog;
  } else {
    dialog = document.getElementById('dialog');
    dialogheader = document.getElementById('dialog-header');
    dialogtitle = document.getElementById('dialog-title');
    dialogclose = document.getElementById('dialog-close');
    dialogcontent = document.getElementById('dialog-content');
    dialogmask = document.getElementById('dialog-mask');
    dialogmask.style.visibility = "visible";
    dialog.style.visibility = "visible";
  }
  dialog.style.opacity = .00;
  dialog.style.filter = 'alpha(opacity=0)';
  dialog.alpha = 0;
  var width = pageWidth();
  var height = pageHeight();
  var left = leftPosition();
  var top = topPosition();
  var dialogwidth = dialog.offsetWidth;
  var dialogheight = dialog.offsetHeight;
  var topposition = top + (height / 3) - (dialogheight / 2);
  var leftposition = left + (width / 2) - (dialogwidth / 2);
  dialog.style.top = topposition + "px";
  dialog.style.left = leftposition + "px";
  dialogheader.className = type + "header";
  dialogtitle.innerHTML = title;
  dialogcontent.className = type;
  dialogcontent.innerHTML = message + "<br /><br /><a href='manage_fees.php?delete=yes&level="+clevel+"&feetype="+feetype+"&selectedfee="+feeid+"'><img src='../images/confirmdelete.png' /></a>"; // NORMAL MESSAGE
  var content = document.getElementById(WRAPPER);
  dialogmask.style.height = content.offsetHeight + 'px';
  dialog.timer = setInterval("fadeDialog(1)", TIMER);
  dialogclose.style.visibility = "visible";
}

function adjustments(studentid,feedes,feeid) {
  var title = 'Adjustment Value (' + feedes + ")";
  var message = 'New value for adjustments : ' + feedes;
  var type = 'prompt';
  var dialog;
  var dialogheader;
  var dialogclose;
  var dialogtitle;
  var dialogcontent;
  var dialogmask;
  if(!document.getElementById('dialog')) {
    dialog = document.createElement('div');
    dialog.id = 'dialog';
    dialogheader = document.createElement('div');
    dialogheader.id = 'dialog-header';
    dialogtitle = document.createElement('div');
    dialogtitle.id = 'dialog-title';
    dialogclose = document.createElement('div');
    dialogclose.id = 'dialog-close'
    dialogcontent = document.createElement('div');
    dialogcontent.id = 'dialog-content';
    dialogmask = document.createElement('div');
    dialogmask.id = 'dialog-mask';
    document.body.appendChild(dialogmask);
    document.body.appendChild(dialog);
    dialog.appendChild(dialogheader);
    dialogheader.appendChild(dialogtitle);
    dialogheader.appendChild(dialogclose);
    dialog.appendChild(dialogcontent);;
    dialogclose.setAttribute('onclick','hideDialog()');
    dialogclose.onclick = hideDialog;
  } else {
    dialog = document.getElementById('dialog');
    dialogheader = document.getElementById('dialog-header');
    dialogtitle = document.getElementById('dialog-title');
    dialogclose = document.getElementById('dialog-close');
    dialogcontent = document.getElementById('dialog-content');
    dialogmask = document.getElementById('dialog-mask');
    dialogmask.style.visibility = "visible";
    dialog.style.visibility = "visible";
  }
  dialog.style.opacity = .00;
  dialog.style.filter = 'alpha(opacity=0)';
  dialog.alpha = 0;
  var width = pageWidth();
  var height = pageHeight();
  var left = leftPosition();
  var top = topPosition();
  var dialogwidth = dialog.offsetWidth;
  var dialogheight = dialog.offsetHeight;
  var topposition = top + (height / 3) - (dialogheight / 2);
  var leftposition = left + (width / 2) - (dialogwidth / 2);
  dialog.style.top = topposition + "px";
  dialog.style.left = leftposition + "px";
  dialogheader.className = type + "header";
  dialogtitle.innerHTML = title;
  dialogcontent.className = type;
  dialogcontent.innerHTML = message + "<br /><br />\
	<form method='GET'>\
	<input type='text' name='feeadj' />\
	<input type='hidden' name='eid' value='"+studentid+"' />\
	<input type='hidden' name='feeid' value='"+feeid+"' />\
	<input type='submit' name='adj' value='Submit'>\
	</form>"; // NORMAL MESSAGE
  var content = document.getElementById(WRAPPER);
  dialogmask.style.height = content.offsetHeight + 'px';
  dialog.timer = setInterval("fadeDialog(1)", TIMER);
  dialogclose.style.visibility = "visible";
}

function aedelete(sid,sy,name) {
  var title = 'Delete Enrollment/Assessment Data';
  var message = "Name: " + name +'<br />Student ID : ' + sid + "<br />School Year : " + sy;
  var type = 'Warning';
  var dialog;
  var dialogheader;
  var dialogclose;
  var dialogtitle;
  var dialogcontent;
  var dialogmask;
  if(!document.getElementById('dialog')) {
    dialog = document.createElement('div');
    dialog.id = 'dialog';
    dialogheader = document.createElement('div');
    dialogheader.id = 'dialog-header';
    dialogtitle = document.createElement('div');
    dialogtitle.id = 'dialog-title';
    dialogclose = document.createElement('div');
    dialogclose.id = 'dialog-close'
    dialogcontent = document.createElement('div');
    dialogcontent.id = 'dialog-content';
    dialogmask = document.createElement('div');
    dialogmask.id = 'dialog-mask';
    document.body.appendChild(dialogmask);
    document.body.appendChild(dialog);
    dialog.appendChild(dialogheader);
    dialogheader.appendChild(dialogtitle);
    dialogheader.appendChild(dialogclose);
    dialog.appendChild(dialogcontent);;
    dialogclose.setAttribute('onclick','hideDialog()');
    dialogclose.onclick = hideDialog;
  } else {
    dialog = document.getElementById('dialog');
    dialogheader = document.getElementById('dialog-header');
    dialogtitle = document.getElementById('dialog-title');
    dialogclose = document.getElementById('dialog-close');
    dialogcontent = document.getElementById('dialog-content');
    dialogmask = document.getElementById('dialog-mask');
    dialogmask.style.visibility = "visible";
    dialog.style.visibility = "visible";
  }
  dialog.style.opacity = .00;
  dialog.style.filter = 'alpha(opacity=0)';
  dialog.alpha = 0;
  var width = pageWidth();
  var height = pageHeight();
  var left = leftPosition();
  var top = topPosition();
  var dialogwidth = dialog.offsetWidth;
  var dialogheight = dialog.offsetHeight;
  var topposition = top + (height / 3) - (dialogheight / 2);
  var leftposition = left + (width / 2) - (dialogwidth / 2);
  dialog.style.top = topposition + "px";
  dialog.style.left = leftposition + "px";
  dialogheader.className = type + "header";
  dialogtitle.innerHTML = title;
  dialogcontent.className = type;
  dialogcontent.innerHTML = message + "<br /><br /><a href='assess_enroll.php?delete=yes&eid="+sid+"&sy="+sy+"'><img src='../images/confirmdelete.png' /></a>"; // NORMAL MESSAGE
  var content = document.getElementById(WRAPPER);
  dialogmask.style.height = content.offsetHeight + 'px';
  dialog.timer = setInterval("fadeDialog(1)", TIMER);
  dialogclose.style.visibility = "visible";
}

function changedate(pt,sy,installment) {
  var title = 'Change Due Date For ' + pt;
  var message = 'School Year : ' + sy + '<br />Installment # : ' + installment + '<br /><br /><font color="red">Note: Input Format Month/Day/Year</font>';
  var type = 'prompt';
  var dialog;
  var dialogheader;
  var dialogclose;
  var dialogtitle;
  var dialogcontent;
  var dialogmask;
  if(!document.getElementById('dialog')) {
    dialog = document.createElement('div');
    dialog.id = 'dialog';
    dialogheader = document.createElement('div');
    dialogheader.id = 'dialog-header';
    dialogtitle = document.createElement('div');
    dialogtitle.id = 'dialog-title';
    dialogclose = document.createElement('div');
    dialogclose.id = 'dialog-close'
    dialogcontent = document.createElement('div');
    dialogcontent.id = 'dialog-content';
    dialogmask = document.createElement('div');
    dialogmask.id = 'dialog-mask';
    document.body.appendChild(dialogmask);
    document.body.appendChild(dialog);
    dialog.appendChild(dialogheader);
    dialogheader.appendChild(dialogtitle);
    dialogheader.appendChild(dialogclose);
    dialog.appendChild(dialogcontent);;
    dialogclose.setAttribute('onclick','hideDialog()');
    dialogclose.onclick = hideDialog;
  } else {
    dialog = document.getElementById('dialog');
    dialogheader = document.getElementById('dialog-header');
    dialogtitle = document.getElementById('dialog-title');
    dialogclose = document.getElementById('dialog-close');
    dialogcontent = document.getElementById('dialog-content');
    dialogmask = document.getElementById('dialog-mask');
    dialogmask.style.visibility = "visible";
    dialog.style.visibility = "visible";
  }
  dialog.style.opacity = .00;
  dialog.style.filter = 'alpha(opacity=0)';
  dialog.alpha = 0;
  var width = pageWidth();
  var height = pageHeight();
  var left = leftPosition();
  var top = topPosition();
  var dialogwidth = dialog.offsetWidth;
  var dialogheight = dialog.offsetHeight;
  var topposition = top + (height / 3) - (dialogheight / 2);
  var leftposition = left + (width / 2) - (dialogwidth / 2);
  dialog.style.top = topposition + "px";
  dialog.style.left = leftposition + "px";
  dialogheader.className = type + "header";
  dialogtitle.innerHTML = title;
  dialogcontent.className = type;
  dialogcontent.innerHTML = message + "<br /><br />\
	<form method='GET'>\
	<input type='hidden' name='pt' value='"+pt+"' />\
	<select name='mon' id='mon'>\
        <option value='1'>January</option>\
        <option value='2'>February</option>\
        <option value='3'>March</option>\
        <option value='4'>April</option>\
        <option value='5'>May</option>\
        <option value='6'>June</option>\
        <option value='7'>July</option>\
        <option value='8'>August</option>\
        <option value='9'>September</option>\
        <option value='10'>October</option>\
        <option value='11'>November</option>\
        <option value='12'>December</option>\
    </select>\
	<select name='day' id='day'>\
        <option value='1'>1</option>\
        <option value='2'>2</option>\
        <option value='3'>3</option>\
        <option value='4'>4</option>\
        <option value='5'>5</option>\
        <option value='6'>6</option>\
        <option value='7'>7</option>\
        <option value='8'>8</option>\
        <option value='9'>9</option>\
        <option value='10'>10</option>\
		<option value='11'>11</option>\
        <option value='12'>12</option>\
        <option value='13'>13</option>\
        <option value='14'>14</option>\
        <option value='15'>15</option>\
        <option value='16'>16</option>\
        <option value='17'>17</option>\
        <option value='18'>18</option>\
        <option value='19'>19</option>\
        <option value='20'>20</option>\
        <option value='21'>21</option>\
        <option value='22'>22</option>\
        <option value='23'>23</option>\
        <option value='24'>24</option>\
        <option value='25'>25</option>\
        <option value='26'>26</option>\
        <option value='27'>27</option>\
        <option value='28'>28</option>\
        <option value='29'>29</option>\
		<option value='30'>30</option>\
        <option value='31'>31</option>\
      </select>\
	<input type='text' name='year' maxlength='4' size='4' style='padding:1px'>\
	<input type='hidden' name='sy' value='"+sy+"' />\
	<input type='hidden' name='in' value='"+installment+"' />\
	<input type='submit' name='chgdate' value='Submit'>\
	</form>"; // NORMAL MESSAGE
  var content = document.getElementById(WRAPPER);
  dialogmask.style.height = content.offsetHeight + 'px';
  dialog.timer = setInterval("fadeDialog(1)", TIMER);
  dialogclose.style.visibility = "visible";
}

function assignadv(empid) {
  var title = 'Assign as Adviser / Co-Adviser';
  var message = 'Select Level & Section';
  var type = 'prompt';
  var dialog;
  var dialogheader;
  var dialogclose;
  var dialogtitle;
  var dialogcontent;
  var dialogmask;
  if(!document.getElementById('dialog')) {
    dialog = document.createElement('div');
    dialog.id = 'dialog';
    dialogheader = document.createElement('div');
    dialogheader.id = 'dialog-header';
    dialogtitle = document.createElement('div');
    dialogtitle.id = 'dialog-title';
    dialogclose = document.createElement('div');
    dialogclose.id = 'dialog-close'
    dialogcontent = document.createElement('div');
    dialogcontent.id = 'dialog-content';
    dialogmask = document.createElement('div');
    dialogmask.id = 'dialog-mask';
    document.body.appendChild(dialogmask);
    document.body.appendChild(dialog);
    dialog.appendChild(dialogheader);
    dialogheader.appendChild(dialogtitle);
    dialogheader.appendChild(dialogclose);
    dialog.appendChild(dialogcontent);;
    dialogclose.setAttribute('onclick','hideDialog()');
    dialogclose.onclick = hideDialog;
  } else {
    dialog = document.getElementById('dialog');
    dialogheader = document.getElementById('dialog-header');
    dialogtitle = document.getElementById('dialog-title');
    dialogclose = document.getElementById('dialog-close');
    dialogcontent = document.getElementById('dialog-content');
    dialogmask = document.getElementById('dialog-mask');
    dialogmask.style.visibility = "visible";
    dialog.style.visibility = "visible";
  }
  dialog.style.opacity = .00;
  dialog.style.filter = 'alpha(opacity=0)';
  dialog.alpha = 0;
  var width = pageWidth();
  var height = pageHeight();
  var left = leftPosition();
  var top = topPosition();
  var dialogwidth = dialog.offsetWidth;
  var dialogheight = dialog.offsetHeight;
  var topposition = top + (height / 3) - (dialogheight / 2);
  var leftposition = left + (width / 2) - (dialogwidth / 2);
  dialog.style.top = topposition + "px";
  dialog.style.left = leftposition + "px";
  dialogheader.className = type + "header";
  dialogtitle.innerHTML = title;
  dialogcontent.className = type;
  dialogcontent.innerHTML = message + "<br /><br />\
	<form action='sections.php' method='GET'>\
	<div id='listalllevels'></div>\
	<div id='sectiontd'></div><br />\
	<select name='assignas' style='width:150px'>\
	<option value='class_adviser'>Adviser</option>\
	<option value='co_adviser'>Co-Adviser</option>\
	<select><br />\
	<input type='hidden' name='empid' value='"+empid+"' />\
	<input type='submit' name='updateadv' value='Submit'>\
	</form>"; // NORMAL MESSAGE
  var content = document.getElementById(WRAPPER);
  dialogmask.style.height = content.offsetHeight + 'px';
  dialog.timer = setInterval("fadeDialog(1)", TIMER);
  dialogclose.style.visibility = "visible";
  showList();
}

function newdept() {
var title = 'Add New Department';
var type = 'prompt';
var dialog;
var dialogheader;
var dialogclose;
var dialogtitle;
var dialogcontent;
var dialogmask;
if(!document.getElementById('dialog')) {
dialog = document.createElement('div');
dialog.id = 'dialog';
dialogheader = document.createElement('div');
dialogheader.id = 'dialog-header';
dialogtitle = document.createElement('div');
dialogtitle.id = 'dialog-title';
dialogclose = document.createElement('div');
dialogclose.id = 'dialog-close'
dialogcontent = document.createElement('div');
dialogcontent.id = 'dialog-content';
dialogmask = document.createElement('div');
dialogmask.id = 'dialog-mask';
document.body.appendChild(dialogmask);
document.body.appendChild(dialog);
dialog.appendChild(dialogheader);
dialogheader.appendChild(dialogtitle);
dialogheader.appendChild(dialogclose);
dialog.appendChild(dialogcontent);;
dialogclose.setAttribute('onclick','hideDialog()');
dialogclose.onclick = hideDialog;
} else {
dialog = document.getElementById('dialog');
dialogheader = document.getElementById('dialog-header');
dialogtitle = document.getElementById('dialog-title');
dialogclose = document.getElementById('dialog-close');
dialogcontent = document.getElementById('dialog-content');
dialogmask = document.getElementById('dialog-mask');
dialogmask.style.visibility = "visible";
dialog.style.visibility = "visible";
}
dialog.style.opacity = .00;
dialog.style.filter = 'alpha(opacity=0)';
dialog.alpha = 0;
var width = pageWidth();
var height = pageHeight();
var left = leftPosition();
var top = topPosition();
var dialogwidth = dialog.offsetWidth;
var dialogheight = dialog.offsetHeight;
var topposition = top + (height / 3) - (dialogheight / 2);
var leftposition = left + (width / 2) - (dialogwidth / 2);
dialog.style.top = topposition + "px";
dialog.style.left = leftposition + "px";
dialogheader.className = type + "header";
dialogtitle.innerHTML = title;
dialogcontent.className = type;
dialogcontent.innerHTML = "Department Name<br />\
<form method='GET' onSubmit='return checkfields()'>\
<input type='text' name='newdeptname' id='newdeptname' style='width:150px'> Unofficial <input type='checkbox' name='unofficial' value='1'><br />Description<br />\
<textarea name='description' id='description' style='width:150px'></textarea><br />\
<input type='submit' name='newdept' value='Submit'>\
</form>"; // NORMAL MESSAGE
var content = document.getElementById(WRAPPER);
dialogmask.style.height = content.offsetHeight + 'px';
dialog.timer = setInterval("fadeDialog(1)", TIMER);
dialogclose.style.visibility = "visible";
}

function addnewsubject(dept) {
if(dept != 'none') {
var title = "Add New Subject for " + dept + " DPT";
var type = 'prompt';
var dialog;
var dialogheader;
var dialogclose;
var dialogtitle;
var dialogcontent;
var dialogmask;
if(!document.getElementById('dialog')) {
dialog = document.createElement('div');
dialog.id = 'dialog';
dialogheader = document.createElement('div');
dialogheader.id = 'dialog-header';
dialogtitle = document.createElement('div');
dialogtitle.id = 'dialog-title';
dialogclose = document.createElement('div');
dialogclose.id = 'dialog-close'
dialogcontent = document.createElement('div');
dialogcontent.id = 'dialog-content';
dialogmask = document.createElement('div');
dialogmask.id = 'dialog-mask';
document.body.appendChild(dialogmask);
document.body.appendChild(dialog);
dialog.appendChild(dialogheader);
dialogheader.appendChild(dialogtitle);
dialogheader.appendChild(dialogclose);
dialog.appendChild(dialogcontent);;
dialogclose.setAttribute('onclick','hideDialog()');
dialogclose.onclick = hideDialog;
} else {
dialog = document.getElementById('dialog');
dialogheader = document.getElementById('dialog-header');
dialogtitle = document.getElementById('dialog-title');
dialogclose = document.getElementById('dialog-close');
dialogcontent = document.getElementById('dialog-content');
dialogmask = document.getElementById('dialog-mask');
dialogmask.style.visibility = "visible";
dialog.style.visibility = "visible";
}
dialog.style.opacity = .00;
dialog.style.filter = 'alpha(opacity=0)';
dialog.alpha = 0;
var width = pageWidth();
var height = pageHeight();
var left = leftPosition();
var top = topPosition();
var dialogwidth = dialog.offsetWidth;
var dialogheight = dialog.offsetHeight;
var topposition = top + (height / 3) - (dialogheight / 2);
var leftposition = left + (width / 2) - (dialogwidth / 2);
dialog.style.top = topposition + "px";
dialog.style.left = leftposition + "px";
dialogheader.className = type + "header";
dialogtitle.innerHTML = title;
dialogcontent.className = type;
dialogcontent.innerHTML = "New Subject Name\
<form method='GET'>\
<input type='hidden' name='dpt' value='"+dept+"'>\
<input type='text' name='subjectcode' style='width:150px'> <br />Description<br />\
<textarea name='sdescription' style='width:150px'></textarea><br />\
<input type='submit' name='submitnewsub' value='Submit'>\
</form>"; // NORMAL MESSAGE
var content = document.getElementById(WRAPPER);
dialogmask.style.height = content.offsetHeight + 'px';
dialog.timer = setInterval("fadeDialog(1)", TIMER);
dialogclose.style.visibility = "visible";
}
else {
	showDialog('Error','No Department Selected','error','2');
}
}

function assignteacher(dept, subj) {
  var title = 'Assign Teacher To ' + subj;
  var message = 'Search Employee (ID or Name)';
  var type = 'prompt';
  var dialog;
  var dialogheader;
  var dialogclose;
  var dialogtitle;
  var dialogcontent;
  var dialogmask;
  if(!document.getElementById('dialog')) {
    dialog = document.createElement('div');
    dialog.id = 'dialog';
    dialogheader = document.createElement('div');
    dialogheader.id = 'dialog-header';
    dialogtitle = document.createElement('div');
    dialogtitle.id = 'dialog-title';
    dialogclose = document.createElement('div');
    dialogclose.id = 'dialog-close'
    dialogcontent = document.createElement('div');
    dialogcontent.id = 'dialog-content';
    dialogmask = document.createElement('div');
    dialogmask.id = 'dialog-mask';
    document.body.appendChild(dialogmask);
    document.body.appendChild(dialog);
    dialog.appendChild(dialogheader);
    dialogheader.appendChild(dialogtitle);
    dialogheader.appendChild(dialogclose);
    dialog.appendChild(dialogcontent);;
    dialogclose.setAttribute('onclick','hideDialog()');
    dialogclose.onclick = hideDialog;
  } else {
    dialog = document.getElementById('dialog');
    dialogheader = document.getElementById('dialog-header');
    dialogtitle = document.getElementById('dialog-title');
    dialogclose = document.getElementById('dialog-close');
    dialogcontent = document.getElementById('dialog-content');
    dialogmask = document.getElementById('dialog-mask');
    dialogmask.style.visibility = "visible";
    dialog.style.visibility = "visible";
  }
  dialog.style.opacity = .00;
  dialog.style.filter = 'alpha(opacity=0)';
  dialog.alpha = 0;
  var width = pageWidth();
  var height = pageHeight();
  var left = leftPosition();
  var top = topPosition();
  var dialogwidth = dialog.offsetWidth;
  var dialogheight = dialog.offsetHeight;
  var topposition = top + (height / 3) - (dialogheight / 2);
  var leftposition = left + (width / 2) - (dialogwidth / 2);
  dialog.style.top = topposition + "px";
  dialog.style.left = leftposition + "px";
  dialogheader.className = type + "header";
  dialogtitle.innerHTML = title;
  dialogcontent.className = type;
  dialogcontent.innerHTML = message + "<br /><br />\
	<form action='' method='GET'>\
	<input type='hidden' name='dept' value='"+dept+"' />\
	<input type='hidden' name='subj' value='"+subj+"' />\
	<input type=\"text\" size=\"30\" value=\"\" id=\"empid\" name=\"empid\" onkeyup=\"lookup(this.value);\" onblur=\"fill();\" />\
	<div class=\"suggestionsBox\" id=\"suggestions\" style=\"display: none;position:absolute\">\
				<img src=\"../images/upArrow.png\" style=\"position: absolute; top: -12px; left: 30px;\" alt=\"upArrow\" />\
				<div class=\"suggestionList\" id=\"autoSuggestionsList\">\
				</div>\
			</div>\
	<br /><br />\
	<input type='submit' name='assignteacher' value='Submit'>\
	</form>"; // NORMAL MESSAGE
  var content = document.getElementById(WRAPPER);
  dialogmask.style.height = content.offsetHeight + 'px';
  dialog.timer = setInterval("fadeDialog(1)", TIMER);
  dialogclose.style.visibility = "visible";
  showList();
}

function modifysubjects(dept, subj, desc) {
var title = "Modify Subject";
var type = 'prompt';
var dialog;
var dialogheader;
var dialogclose;
var dialogtitle;
var dialogcontent;
var dialogmask;
if(!document.getElementById('dialog')) {
dialog = document.createElement('div');
dialog.id = 'dialog';
dialogheader = document.createElement('div');
dialogheader.id = 'dialog-header';
dialogtitle = document.createElement('div');
dialogtitle.id = 'dialog-title';
dialogclose = document.createElement('div');
dialogclose.id = 'dialog-close'
dialogcontent = document.createElement('div');
dialogcontent.id = 'dialog-content';
dialogmask = document.createElement('div');
dialogmask.id = 'dialog-mask';
document.body.appendChild(dialogmask);
document.body.appendChild(dialog);
dialog.appendChild(dialogheader);
dialogheader.appendChild(dialogtitle);
dialogheader.appendChild(dialogclose);
dialog.appendChild(dialogcontent);;
dialogclose.setAttribute('onclick','hideDialog()');
dialogclose.onclick = hideDialog;
} else {
dialog = document.getElementById('dialog');
dialogheader = document.getElementById('dialog-header');
dialogtitle = document.getElementById('dialog-title');
dialogclose = document.getElementById('dialog-close');
dialogcontent = document.getElementById('dialog-content');
dialogmask = document.getElementById('dialog-mask');
dialogmask.style.visibility = "visible";
dialog.style.visibility = "visible";
}
dialog.style.opacity = .00;
dialog.style.filter = 'alpha(opacity=0)';
dialog.alpha = 0;
var width = pageWidth();
var height = pageHeight();
var left = leftPosition();
var top = topPosition();
var dialogwidth = dialog.offsetWidth;
var dialogheight = dialog.offsetHeight;
var topposition = top + (height / 3) - (dialogheight / 2);
var leftposition = left + (width / 2) - (dialogwidth / 2);
dialog.style.top = topposition + "px";
dialog.style.left = leftposition + "px";
dialogheader.className = type + "header";
dialogtitle.innerHTML = title;
dialogcontent.className = type;
dialogcontent.innerHTML = "\
<form method='GET'>\
<table border='0' cellspacing='0' cellpadding='0'><tr><td>Subject Name</td><td>Department</td></tr><tr><td>&nbsp;<input type='text' name='subjectcode' style='width:150px' value='"+subj+"'>&nbsp;</td><td id='listdept'>\ </td></tr></table>Description<br /><input type='hidden' name='currentname' value='"+subj+"' />\
<textarea name='sdescription' style='width:150px'>"+desc+"</textarea><br />\
<input type='submit' name='submitmodify' value='Submit'>\
</form>"; // NORMAL MESSAGE
var content = document.getElementById(WRAPPER);
dialogmask.style.height = content.offsetHeight + 'px';
dialog.timer = setInterval("fadeDialog(1)", TIMER);
dialogclose.style.visibility = "visible";
displaydept(dept);
}


function checkfields() {
		if(document.getElementById('newdeptname').value == "") {
			showDialog('Error','No Department Selected','error','2');
			return false;
		}
		else {
			return true;
		}
}

// hide the dialog box //
function hideDialog() {
  var dialog = document.getElementById('dialog');
  clearInterval(dialog.timer);
  dialog.timer = setInterval("fadeDialog(0)", TIMER);
}

// fade-in the dialog box //
function fadeDialog(flag) {
  if(flag == null) {
    flag = 1;
  }
  var dialog = document.getElementById('dialog');
  var value;
  if(flag == 1) {
    value = dialog.alpha + SPEED;
  } else {
    value = dialog.alpha - SPEED;
  }
  dialog.alpha = value;
  dialog.style.opacity = (value / 100);
  dialog.style.filter = 'alpha(opacity=' + value + ')';
  if(value >= 99) {
    clearInterval(dialog.timer);
    dialog.timer = null;
  } else if(value <= 1) {
    dialog.style.visibility = "hidden";
    document.getElementById('dialog-mask').style.visibility = "hidden";
    clearInterval(dialog.timer);
  }
}