-- phpMyAdmin SQL Dump
-- version 3.4.3.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 12, 2011 at 02:34 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `smgts_db`
--
CREATE DATABASE `smgts_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `smgts_db`;

-- --------------------------------------------------------

--
-- Table structure for table `bulletin`
--

CREATE TABLE IF NOT EXISTS `bulletin` (
  `bulletin_num` int(11) NOT NULL,
  `posted_date` date NOT NULL,
  `posted_time` time NOT NULL,
  `employee_id` varchar(9) NOT NULL,
  `title` varchar(200) NOT NULL,
  `bulletin_message` text NOT NULL,
  PRIMARY KEY (`bulletin_num`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bulletin`
--

INSERT INTO `bulletin` (`bulletin_num`, `posted_date`, `posted_time`, `employee_id`, `title`, `bulletin_message`) VALUES
(1, '2011-09-03', '00:01:51', '2011-0001', 'To: Teachers', 'You may view your respecting class loading by the Subject and Advisory Class Page by clicking the link in the Loading Section at the Navigation Bar.'),
(3, '2011-09-03', '00:59:11', '2011-0001', 'Tutorial: Adding Subjects and Teacher Loading', 'The amateur video is located here:<br />\r\n<br />\r\nClick Here: <a href="../Tutorial/Tutorial_1.html">LINK</a>'),
(4, '2011-09-03', '01:29:14', '2011-0001', 'To: Parents and Guardians', 'You may view your sons and daughter''s grades by click their names in the navigation bar at the right.'),
(5, '2011-09-05', '12:27:42', '2011-0001', 'Welcome to all.', 'This is the Spurgeon School Management System created by Project Novo.<br />\r\n<br />\r\nIt avails the following:<br />\r\n- Assessment and Enrollment of Students<br />\r\n- Scheduling of Classes<br />\r\n- Detailed Progress Reports regarding subjects');

-- --------------------------------------------------------

--
-- Table structure for table `bulletin_allow`
--

CREATE TABLE IF NOT EXISTS `bulletin_allow` (
  `bulletin_num` int(11) NOT NULL,
  `group_name` varchar(45) NOT NULL,
  PRIMARY KEY (`bulletin_num`,`group_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bulletin_allow`
--

INSERT INTO `bulletin_allow` (`bulletin_num`, `group_name`) VALUES
(1, 'System Admin'),
(1, 'Teacher'),
(3, 'Registrar'),
(3, 'System Admin'),
(3, 'Teacher'),
(4, 'Guardian'),
(4, 'System Admin'),
(5, 'Guardian'),
(5, 'Registrar'),
(5, 'System Admin'),
(5, 'Teacher');

-- --------------------------------------------------------

--
-- Table structure for table `category_percentage`
--

CREATE TABLE IF NOT EXISTS `category_percentage` (
  `subject_code` varchar(45) NOT NULL,
  `dep_code` varchar(45) NOT NULL,
  `category` varchar(45) NOT NULL,
  `percentage` decimal(5,2) NOT NULL,
  PRIMARY KEY (`subject_code`,`dep_code`,`category`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE IF NOT EXISTS `class` (
  `class_name` varchar(45) NOT NULL,
  `level_id` varchar(45) NOT NULL,
  `class_adviser` varchar(9) DEFAULT NULL,
  `co_adviser` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`class_name`,`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `class_level`
--

CREATE TABLE IF NOT EXISTS `class_level` (
  `level_id` varchar(45) NOT NULL,
  `category` int(1) DEFAULT NULL,
  `indexnum` int(1) NOT NULL,
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_level`
--

INSERT INTO `class_level` (`level_id`, `category`, `indexnum`) VALUES
('Beginner Prep', 0, 1),
('Grade 1', 1, 5),
('Grade 2', 1, 6),
('Grade 3', 1, 7),
('Grade 4', 1, 8),
('Grade 5', 1, 9),
('Grade 6', 1, 10),
('Junior Prep', 0, 3),
('Kinder Prep', 0, 11),
('Middle Prep', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `connection`
--

CREATE TABLE IF NOT EXISTS `connection` (
  `student_id` varchar(9) NOT NULL,
  `guardian_id` int(11) NOT NULL,
  `relationship` varchar(45) NOT NULL,
  `user_name` varchar(45) NOT NULL,
  PRIMARY KEY (`student_id`,`guardian_id`,`relationship`),
  KEY `guardian_id` (`guardian_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `dep_code` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `unofficial` varchar(3) NOT NULL,
  PRIMARY KEY (`dep_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `employee_id` varchar(9) NOT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `mname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `address_home` varchar(45) DEFAULT NULL,
  `address_mail` varchar(45) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `citizenship` varchar(45) DEFAULT NULL,
  `civil_status` varchar(45) DEFAULT NULL,
  `spouse` varchar(45) DEFAULT NULL,
  `sss_number` varchar(12) DEFAULT NULL,
  `pagibig_number` varchar(45) DEFAULT NULL,
  `philhealth_number` varchar(45) DEFAULT NULL,
  `TIN` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `user_name` varchar(45) NOT NULL,
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`employee_id`, `fname`, `mname`, `lname`, `birthdate`, `gender`, `address_home`, `address_mail`, `email`, `citizenship`, `civil_status`, `spouse`, `sss_number`, `pagibig_number`, `philhealth_number`, `TIN`, `image`, `user_name`) VALUES
('2011-0001', 'Gerard', 'Sorrera', 'Cruz', '1992-03-25', 'm', 'Pasay City', 'VAB, Pasay City', 'guix21@gmail.com', 'Filipino', 'single', NULL, NULL, NULL, NULL, NULL, 'emp_img/gerard.jpg', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `enroll`
--

CREATE TABLE IF NOT EXISTS `enroll` (
  `student_id` varchar(9) NOT NULL,
  `school_year` varchar(45) NOT NULL,
  `class_name` varchar(45) NOT NULL,
  `level_id` varchar(45) NOT NULL,
  `estatus` varchar(45) DEFAULT NULL,
  `payment_option` varchar(45) DEFAULT NULL,
  `date_enrolled` date NOT NULL,
  PRIMARY KEY (`student_id`,`school_year`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fees`
--

CREATE TABLE IF NOT EXISTS `fees` (
  `fee_id` int(11) NOT NULL,
  `level_id` varchar(45) DEFAULT NULL,
  `fee_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`fee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gradeitems`
--

CREATE TABLE IF NOT EXISTS `gradeitems` (
  `level_id` varchar(45) NOT NULL,
  `class_name` varchar(45) NOT NULL,
  `subject_code` varchar(45) NOT NULL,
  `gi_code` varchar(45) NOT NULL,
  `quarter` int(11) NOT NULL,
  `school_year` varchar(9) NOT NULL,
  `category` varchar(45) NOT NULL,
  PRIMARY KEY (`level_id`,`class_name`,`subject_code`,`gi_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE IF NOT EXISTS `grades` (
  `subject_code` varchar(45) NOT NULL,
  `student_id` varchar(45) NOT NULL,
  `school_year` varchar(45) NOT NULL,
  `quarter` int(1) NOT NULL,
  `gi_code` varchar(45) NOT NULL,
  `student_grade` decimal(5,2) NOT NULL,
  PRIMARY KEY (`subject_code`,`student_id`,`school_year`,`quarter`,`gi_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `group_permissions`
--

CREATE TABLE IF NOT EXISTS `group_permissions` (
  `group_name` varchar(45) NOT NULL,
  `is_VIP` int(11) NOT NULL,
  `control_add` int(11) DEFAULT NULL,
  `control_edit` int(11) DEFAULT NULL,
  `control_delete` int(11) DEFAULT NULL,
  `ref_students` int(11) DEFAULT NULL,
  `ref_guardian` int(11) DEFAULT NULL,
  `ref_employee` int(11) DEFAULT NULL,
  `ref_update_img` int(11) NOT NULL,
  `ref_assess_enroll` int(11) NOT NULL,
  `ref_department` int(11) DEFAULT NULL,
  `ref_class` int(11) DEFAULT NULL,
  `ref_class_level` int(11) DEFAULT NULL,
  `ref_fees` int(11) DEFAULT NULL,
  `ref_payment_dues` int(11) DEFAULT NULL,
  `ref_subject_list` int(11) DEFAULT NULL,
  `ref_schedule` int(11) DEFAULT NULL,
  `ref_grade_item` int(11) DEFAULT NULL,
  `ref_grade_item_type` int(11) DEFAULT NULL,
  `ref_grade` int(11) DEFAULT NULL,
  `ref_connection` int(11) DEFAULT NULL,
  `ref_timeslot` int(11) NOT NULL,
  `ref_update_adviser` int(11) NOT NULL,
  `user_access` int(11) DEFAULT NULL,
  `user_groups` int(11) DEFAULT NULL,
  `sys_system_default` int(11) DEFAULT NULL,
  `sys_bulletin` int(11) NOT NULL,
  `sys_backup` int(11) NOT NULL,
  `sys_log` int(11) NOT NULL,
  `ref_all_load` int(11) NOT NULL,
  `delete_logs` int(11) NOT NULL,
  PRIMARY KEY (`group_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_permissions`
--

INSERT INTO `group_permissions` (`group_name`, `is_VIP`, `control_add`, `control_edit`, `control_delete`, `ref_students`, `ref_guardian`, `ref_employee`, `ref_update_img`, `ref_assess_enroll`, `ref_department`, `ref_class`, `ref_class_level`, `ref_fees`, `ref_payment_dues`, `ref_subject_list`, `ref_schedule`, `ref_grade_item`, `ref_grade_item_type`, `ref_grade`, `ref_connection`, `ref_timeslot`, `ref_update_adviser`, `user_access`, `user_groups`, `sys_system_default`, `sys_bulletin`, `sys_backup`, `sys_log`, `ref_all_load`, `delete_logs`) VALUES
('Guardian', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('No Access / Disabled', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0),
('Registrar', 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, NULL, NULL, 1, 0, 1, 1, 1),
('System Admin', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
('Teacher', 0, 0, 1, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `guardian`
--

CREATE TABLE IF NOT EXISTS `guardian` (
  `guardian_id` int(11) NOT NULL,
  `relationship` varchar(45) NOT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `mname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `occupation` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone_landline` varchar(45) DEFAULT NULL,
  `phone_mobile` varchar(45) DEFAULT NULL,
  `emergency_contact` int(11) DEFAULT NULL,
  `living_with` int(11) DEFAULT NULL,
  PRIMARY KEY (`guardian_id`,`relationship`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `optional`
--

CREATE TABLE IF NOT EXISTS `optional` (
  `fee_id` int(11) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `cost` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`fee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `other`
--

CREATE TABLE IF NOT EXISTS `other` (
  `fee_id` int(11) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `cost` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`fee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_dues`
--

CREATE TABLE IF NOT EXISTS `payment_dues` (
  `payment_type` varchar(45) NOT NULL,
  `installment` int(11) NOT NULL,
  `due_date` date NOT NULL,
  `school_year` varchar(9) NOT NULL,
  PRIMARY KEY (`payment_type`,`installment`,`school_year`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE IF NOT EXISTS `schedule` (
  `schedule_day` varchar(15) NOT NULL,
  `level_id` varchar(45) NOT NULL,
  `class_name` varchar(45) NOT NULL,
  `timeslot_num` int(11) NOT NULL,
  `subject_code` varchar(45) NOT NULL,
  `dep_code` varchar(45) NOT NULL,
  `room_code` varchar(45) NOT NULL,
  PRIMARY KEY (`schedule_day`,`level_id`,`class_name`,`timeslot_num`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `student_id` varchar(9) NOT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `mname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `address_home` text,
  `address_mail` text,
  `citizenship` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_has_fees`
--

CREATE TABLE IF NOT EXISTS `student_has_fees` (
  `student_id` varchar(9) NOT NULL,
  `fee_id` int(11) NOT NULL,
  `adjustments` decimal(8,2) DEFAULT NULL,
  `school_year` varchar(45) NOT NULL,
  PRIMARY KEY (`student_id`,`fee_id`,`school_year`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE IF NOT EXISTS `subjects` (
  `subject_code` varchar(45) NOT NULL,
  `dep_code` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `employee_id` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`subject_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `system_default`
--

CREATE TABLE IF NOT EXISTS `system_default` (
  `system_default` int(11) NOT NULL,
  `school_year` varchar(45) NOT NULL,
  `quarter` int(11) NOT NULL,
  PRIMARY KEY (`system_default`,`school_year`,`quarter`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_default`
--

INSERT INTO `system_default` (`system_default`, `school_year`, `quarter`) VALUES
(0, '2011-2012', 1);

-- --------------------------------------------------------

--
-- Table structure for table `system_logs`
--

CREATE TABLE IF NOT EXISTS `system_logs` (
  `log_num` int(11) NOT NULL,
  `module` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `employee_id` varchar(9) NOT NULL,
  `date_processed` date NOT NULL,
  `time_processed` time NOT NULL,
  PRIMARY KEY (`log_num`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `timeslot`
--

CREATE TABLE IF NOT EXISTS `timeslot` (
  `timeslot_num` int(11) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  PRIMARY KEY (`timeslot_num`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tuition`
--

CREATE TABLE IF NOT EXISTS `tuition` (
  `fee_id` int(11) NOT NULL,
  `payment_option` varchar(45) DEFAULT NULL,
  `upon_enrollment` decimal(8,2) DEFAULT NULL,
  `installment` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`fee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_access`
--

CREATE TABLE IF NOT EXISTS `user_access` (
  `user_name` varchar(45) NOT NULL,
  `user_pass` varchar(45) DEFAULT NULL,
  `group_name` varchar(45) NOT NULL,
  `is_emp` int(11) NOT NULL,
  PRIMARY KEY (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_access`
--

INSERT INTO `user_access` (`user_name`, `user_pass`, `group_name`, `is_emp`) VALUES
('admin', '12345', 'System Admin', 1),
('mmfrigillana', '123456', 'Teacher', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
