DROP TABLE bulletin;

CREATE TABLE `bulletin` (
  `bulletin_num` int(11) NOT NULL,
  `posted_date` date NOT NULL,
  `posted_time` time NOT NULL,
  `employee_id` varchar(9) NOT NULL,
  `title` varchar(200) NOT NULL,
  `bulletin_message` text NOT NULL,
  PRIMARY KEY (`bulletin_num`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO bulletin VALUES("1","2011-09-03","00:01:51","2011-0001","To: Teachers","You may view your respecting class loading by the Subject and Advisory Class Page by clicking the link in the Loading Section at the Navigation Bar.");
INSERT INTO bulletin VALUES("3","2011-09-03","00:59:11","2011-0001","Tutorial: Adding Subjects and Teacher Loading","The amateur video is located here:<br />\n<br />\nClick Here: <a href=\"../Tutorial/Tutorial_1.html\">LINK</a>");
INSERT INTO bulletin VALUES("5","2011-09-05","12:27:42","2011-0001","Welcome to all.","This is the Spurgeon School Management System created by Project Novo.<br />\n<br />\nIt avails the following:<br />\n- Assessment and Enrollment of Students<br />\n- Scheduling of Classes<br />\n- Detailed Progress Reports regarding subjects");



DROP TABLE bulletin_allow;

CREATE TABLE `bulletin_allow` (
  `bulletin_num` int(11) NOT NULL,
  `group_name` varchar(45) NOT NULL,
  PRIMARY KEY (`bulletin_num`,`group_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO bulletin_allow VALUES("1","System Admin");
INSERT INTO bulletin_allow VALUES("1","Teacher");
INSERT INTO bulletin_allow VALUES("3","Registrar");
INSERT INTO bulletin_allow VALUES("3","System Admin");
INSERT INTO bulletin_allow VALUES("3","Teacher");
INSERT INTO bulletin_allow VALUES("5","Guardian");
INSERT INTO bulletin_allow VALUES("5","Registrar");
INSERT INTO bulletin_allow VALUES("5","System Admin");
INSERT INTO bulletin_allow VALUES("5","Teacher");



DROP TABLE category_percentage;

CREATE TABLE `category_percentage` (
  `subject_code` varchar(45) NOT NULL,
  `dep_code` varchar(45) NOT NULL,
  `category` varchar(45) NOT NULL,
  `percentage` decimal(5,2) NOT NULL,
  PRIMARY KEY (`subject_code`,`dep_code`,`category`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO category_percentage VALUES("Algebra Grade 6","Mathematics","Seatwork","15.00");
INSERT INTO category_percentage VALUES("Algebra Grade 6","Mathematics","Homework","10.00");
INSERT INTO category_percentage VALUES("Algebra Grade 6","Mathematics","Quizzes","30.00");
INSERT INTO category_percentage VALUES("Algebra Grade 6","Mathematics","Exams","45.00");
INSERT INTO category_percentage VALUES("funda","Nursing","overall","100.00");



DROP TABLE class;

CREATE TABLE `class` (
  `class_name` varchar(45) NOT NULL,
  `level_id` varchar(45) NOT NULL,
  `class_adviser` varchar(9) DEFAULT NULL,
  `co_adviser` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`class_name`,`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO class VALUES("Section I","Grade 6","2011-0002","");
INSERT INTO class VALUES("Section II","Grade 6","","");



DROP TABLE class_level;

CREATE TABLE `class_level` (
  `level_id` varchar(45) NOT NULL,
  `category` int(1) DEFAULT NULL,
  `indexnum` int(1) NOT NULL,
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO class_level VALUES("Beginner Prep","0","1");
INSERT INTO class_level VALUES("Grade 1","1","5");
INSERT INTO class_level VALUES("Grade 2","1","6");
INSERT INTO class_level VALUES("Grade 3","1","7");
INSERT INTO class_level VALUES("Grade 4","1","8");
INSERT INTO class_level VALUES("Grade 5","1","9");
INSERT INTO class_level VALUES("Grade 6","1","10");
INSERT INTO class_level VALUES("Junior Prep","0","3");
INSERT INTO class_level VALUES("Kinder Prep","0","11");
INSERT INTO class_level VALUES("Middle Prep","0","2");



DROP TABLE connection;

CREATE TABLE `connection` (
  `student_id` varchar(9) NOT NULL,
  `guardian_id` int(11) NOT NULL,
  `relationship` varchar(45) NOT NULL,
  `user_name` varchar(45) NOT NULL,
  PRIMARY KEY (`student_id`,`guardian_id`,`relationship`),
  KEY `guardian_id` (`guardian_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO connection VALUES("1112-0001","1","father","limsiacofamily");
INSERT INTO connection VALUES("1112-0001","1","guardian","limsiacofamily");
INSERT INTO connection VALUES("1112-0001","1","mother","limsiacofamily");
INSERT INTO connection VALUES("1112-0002","2","father","testfamily");
INSERT INTO connection VALUES("1112-0002","2","guardian","testfamily");
INSERT INTO connection VALUES("1112-0002","2","mother","testfamily");
INSERT INTO connection VALUES("1112-0003","2","father","testfamily");
INSERT INTO connection VALUES("1112-0003","2","guardian","testfamily");
INSERT INTO connection VALUES("1112-0003","2","mother","testfamily");
INSERT INTO connection VALUES("1112-0004","3","father","msasuncion");
INSERT INTO connection VALUES("1112-0004","3","guardian","msasuncion");
INSERT INTO connection VALUES("1112-0004","3","mother","msasuncion");



DROP TABLE departments;

CREATE TABLE `departments` (
  `dep_code` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `unofficial` varchar(3) NOT NULL,
  PRIMARY KEY (`dep_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO departments VALUES("Mathematics","Full of Math","no");
INSERT INTO departments VALUES("Breaks","Recess, Lunch, Etc (Unofficial)","yes");
INSERT INTO departments VALUES("Nursing","","no");



DROP TABLE employee;

CREATE TABLE `employee` (
  `employee_id` varchar(9) NOT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `mname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `address_home` varchar(45) DEFAULT NULL,
  `address_mail` varchar(45) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `citizenship` varchar(45) DEFAULT NULL,
  `civil_status` varchar(45) DEFAULT NULL,
  `spouse` varchar(45) DEFAULT NULL,
  `sss_number` varchar(12) DEFAULT NULL,
  `pagibig_number` varchar(45) DEFAULT NULL,
  `philhealth_number` varchar(45) DEFAULT NULL,
  `TIN` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `user_name` varchar(45) NOT NULL,
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO employee VALUES("2011-0001","Gerard","Sorrera","Cruz","1992-03-25","m","Pasay City","VAB, Pasay City","guix21@gmail.com","Filipino","single","","","","","","emp_img/gerard.jpg","admin");
INSERT INTO employee VALUES("2011-0002","Marion","Mangaoang","Frigillana","1970-01-01","m","Taguig City","Taguig City","mmfrigillana@apc.edu.ph","Filipino","married","Mark Buzon","","","","","emp_img/2011_0002.jpg","mmfrigillana");
INSERT INTO employee VALUES("2011-0003","Johnny","Bravo","Junior","1980-03-10","m","Makati","Makati","bravo@mail.com","chinese","single","","","","","","emp_img/noimage.jpg","bravojohnny");



DROP TABLE enroll;

CREATE TABLE `enroll` (
  `student_id` varchar(9) NOT NULL,
  `school_year` varchar(45) NOT NULL,
  `class_name` varchar(45) NOT NULL,
  `level_id` varchar(45) NOT NULL,
  `estatus` varchar(45) DEFAULT NULL,
  `payment_option` varchar(45) DEFAULT NULL,
  `date_enrolled` date NOT NULL,
  PRIMARY KEY (`student_id`,`school_year`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO enroll VALUES("1112-0001","2011-2012","Section I","Grade 6","enrolled","quarterly","2011-09-03");
INSERT INTO enroll VALUES("1112-0004","2011-2012","Section II","Grade 6","enrolled","annual","2011-12-15");



DROP TABLE fees;

CREATE TABLE `fees` (
  `fee_id` int(11) NOT NULL,
  `level_id` varchar(45) DEFAULT NULL,
  `fee_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`fee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO fees VALUES("1","Grade 6","tuition");
INSERT INTO fees VALUES("2","Grade 6","tuition");
INSERT INTO fees VALUES("3","Grade 6","tuition");
INSERT INTO fees VALUES("4","Grade 6","other");
INSERT INTO fees VALUES("5","Grade 6","other");
INSERT INTO fees VALUES("6","Grade 6","other");
INSERT INTO fees VALUES("7","Grade 6","other");
INSERT INTO fees VALUES("8","Grade 6","other");
INSERT INTO fees VALUES("9","Grade 6","other");
INSERT INTO fees VALUES("10","Beginner Prep","tuition");
INSERT INTO fees VALUES("11","Beginner Prep","tuition");
INSERT INTO fees VALUES("12","Beginner Prep","tuition");
INSERT INTO fees VALUES("13","Middle Prep","tuition");
INSERT INTO fees VALUES("14","Middle Prep","tuition");
INSERT INTO fees VALUES("15","Middle Prep","tuition");
INSERT INTO fees VALUES("16","Junior Prep","tuition");
INSERT INTO fees VALUES("17","Junior Prep","tuition");
INSERT INTO fees VALUES("18","Junior Prep","tuition");
INSERT INTO fees VALUES("19","Kinder Prep","tuition");
INSERT INTO fees VALUES("20","Kinder Prep","tuition");
INSERT INTO fees VALUES("21","Kinder Prep","tuition");



DROP TABLE gradeitems;

CREATE TABLE `gradeitems` (
  `level_id` varchar(45) NOT NULL,
  `class_name` varchar(45) NOT NULL,
  `subject_code` varchar(45) NOT NULL,
  `gi_code` varchar(45) NOT NULL,
  `quarter` int(11) NOT NULL,
  `school_year` varchar(9) NOT NULL,
  `category` varchar(45) NOT NULL,
  PRIMARY KEY (`level_id`,`class_name`,`subject_code`,`gi_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO gradeitems VALUES("Grade 6","Section I","Algebra Grade 6","SW1","1","2011-2012","Seatwork");
INSERT INTO gradeitems VALUES("Grade 6","Section I","Algebra Grade 6","SW2","1","2011-2012","Seatwork");
INSERT INTO gradeitems VALUES("Grade 6","Section I","Algebra Grade 6","Q2","1","2011-2012","Quizzes");
INSERT INTO gradeitems VALUES("Grade 6","Section I","Algebra Grade 6","Q1","1","2011-2012","Quizzes");
INSERT INTO gradeitems VALUES("Grade 6","Section I","Algebra Grade 6","HW2","1","2011-2012","Homework");
INSERT INTO gradeitems VALUES("Grade 6","Section I","Algebra Grade 6","EX1","1","2011-2012","Exams");
INSERT INTO gradeitems VALUES("Grade 6","Section I","Algebra Grade 6","HW1","1","2011-2012","Homework");
INSERT INTO gradeitems VALUES("Grade 6","Section I","funda","exam1","1","2011-2012","overall");
INSERT INTO gradeitems VALUES("Grade 6","Section I","funda","exam2","1","2011-2012","overall");



DROP TABLE grades;

CREATE TABLE `grades` (
  `subject_code` varchar(45) NOT NULL,
  `student_id` varchar(45) NOT NULL,
  `school_year` varchar(45) NOT NULL,
  `quarter` int(1) NOT NULL,
  `gi_code` varchar(45) NOT NULL,
  `student_grade` decimal(5,2) NOT NULL,
  PRIMARY KEY (`subject_code`,`student_id`,`school_year`,`quarter`,`gi_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO grades VALUES("Algebra Grade 6","1112-0001","2011-2012","1","SW2","90.00");
INSERT INTO grades VALUES("Algebra Grade 6","1112-0001","2011-2012","1","SW1","78.00");
INSERT INTO grades VALUES("Algebra Grade 6","1112-0001","2011-2012","1","Q2","100.00");
INSERT INTO grades VALUES("Algebra Grade 6","1112-0001","2011-2012","1","Q1","-1.00");
INSERT INTO grades VALUES("Algebra Grade 6","1112-0001","2011-2012","1","HW2","90.00");
INSERT INTO grades VALUES("Algebra Grade 6","1112-0001","2011-2012","1","HW1","80.00");
INSERT INTO grades VALUES("Algebra Grade 6","1112-0001","2011-2012","1","EX1","90.00");
INSERT INTO grades VALUES("funda","1112-0001","2011-2012","1","exam1","76.00");
INSERT INTO grades VALUES("funda","1112-0001","2011-2012","1","exam2","80.00");



DROP TABLE group_permissions;

CREATE TABLE `group_permissions` (
  `group_name` varchar(45) NOT NULL,
  `is_VIP` int(11) NOT NULL,
  `control_add` int(11) DEFAULT NULL,
  `control_edit` int(11) DEFAULT NULL,
  `control_delete` int(11) DEFAULT NULL,
  `ref_students` int(11) DEFAULT NULL,
  `ref_guardian` int(11) DEFAULT NULL,
  `ref_employee` int(11) DEFAULT NULL,
  `ref_update_img` int(11) NOT NULL,
  `ref_assess_enroll` int(11) NOT NULL,
  `ref_department` int(11) DEFAULT NULL,
  `ref_class` int(11) DEFAULT NULL,
  `ref_class_level` int(11) DEFAULT NULL,
  `ref_fees` int(11) DEFAULT NULL,
  `ref_payment_dues` int(11) DEFAULT NULL,
  `ref_subject_list` int(11) DEFAULT NULL,
  `ref_schedule` int(11) DEFAULT NULL,
  `ref_grade_item` int(11) DEFAULT NULL,
  `ref_grade_item_type` int(11) DEFAULT NULL,
  `ref_grade` int(11) DEFAULT NULL,
  `ref_connection` int(11) DEFAULT NULL,
  `ref_timeslot` int(11) NOT NULL,
  `ref_update_adviser` int(11) NOT NULL,
  `user_access` int(11) DEFAULT NULL,
  `user_groups` int(11) DEFAULT NULL,
  `sys_system_default` int(11) DEFAULT NULL,
  `sys_bulletin` int(11) NOT NULL,
  `sys_backup` int(11) NOT NULL,
  `sys_log` int(11) NOT NULL,
  `ref_all_load` int(11) NOT NULL,
  `delete_logs` int(11) NOT NULL,
  PRIMARY KEY (`group_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO group_permissions VALUES("Guardian","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0");
INSERT INTO group_permissions VALUES("No Access / Disabled","0","","","","","","","0","0","","","","","","","","","","","","0","0","","","","0","0","0","0","0");
INSERT INTO group_permissions VALUES("Registrar","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","","","1","0","1","1","1");
INSERT INTO group_permissions VALUES("System Admin","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO group_permissions VALUES("Teacher","0","0","1","0","0","","0","0","0","0","0","0","0","0","0","","1","1","1","0","0","0","0","0","0","1","0","0","0","0");



DROP TABLE guardian;

CREATE TABLE `guardian` (
  `guardian_id` int(11) NOT NULL,
  `relationship` varchar(45) NOT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `mname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `occupation` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone_landline` varchar(45) DEFAULT NULL,
  `phone_mobile` varchar(45) DEFAULT NULL,
  `emergency_contact` int(11) DEFAULT NULL,
  `living_with` int(11) DEFAULT NULL,
  PRIMARY KEY (`guardian_id`,`relationship`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO guardian VALUES("1","father","Edgar","Allan","Limsiacoe","Seaman","limsiacoe@mail.com","9XX-XXXX","09XXXXXXXXX","1","1");
INSERT INTO guardian VALUES("1","guardian","","","","","","","","0","0");
INSERT INTO guardian VALUES("1","mother","Geraldine","Ricafort","Limsiacoe","Accountant","limsiacog@mail.com","9XX-XXXX","09XXXXXXXXX","1","1");
INSERT INTO guardian VALUES("2","father","Test","Test","Test","Test","Test","Test","Test","1","1");
INSERT INTO guardian VALUES("2","guardian","","","","","","","","0","0");
INSERT INTO guardian VALUES("2","mother","Test","Test","Test","Test","Test","Test","Test","1","1");
INSERT INTO guardian VALUES("3","father","Mark ","Asuncion","Buzon","IT Manager","asd","","","0","0");
INSERT INTO guardian VALUES("3","guardian","Mae","","","","","","","0","0");
INSERT INTO guardian VALUES("3","mother","Stephanie","","","","","","","0","0");



DROP TABLE optional;

CREATE TABLE `optional` (
  `fee_id` int(11) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `cost` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`fee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE other;

CREATE TABLE `other` (
  `fee_id` int(11) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `cost` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`fee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO other VALUES("5","Computer Fee","3025");
INSERT INTO other VALUES("6","Materials","605");
INSERT INTO other VALUES("7","Graduation Fee","1452");
INSERT INTO other VALUES("8","Developmental Fees","3000");
INSERT INTO other VALUES("9","Miscellaneous","4029");



DROP TABLE payment_dues;

CREATE TABLE `payment_dues` (
  `payment_type` varchar(45) NOT NULL,
  `installment` int(11) NOT NULL,
  `due_date` date NOT NULL,
  `school_year` varchar(9) NOT NULL,
  PRIMARY KEY (`payment_type`,`installment`,`school_year`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO payment_dues VALUES("quarterly","3","2011-11-15","2011-2012");
INSERT INTO payment_dues VALUES("quarterly","4","2012-01-15","2011-2012");
INSERT INTO payment_dues VALUES("semi-annual","2","2011-09-15","2011-2012");
INSERT INTO payment_dues VALUES("quarterly","2","2011-08-15","2011-2012");



DROP TABLE schedule;

CREATE TABLE `schedule` (
  `schedule_day` varchar(15) NOT NULL,
  `level_id` varchar(45) NOT NULL,
  `class_name` varchar(45) NOT NULL,
  `timeslot_num` int(11) NOT NULL,
  `subject_code` varchar(45) NOT NULL,
  `dep_code` varchar(45) NOT NULL,
  `room_code` varchar(45) NOT NULL,
  PRIMARY KEY (`schedule_day`,`level_id`,`class_name`,`timeslot_num`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO schedule VALUES("friday","Grade 6","Section I","4","Lunch","Breaks","n/a");
INSERT INTO schedule VALUES("wednesday","Grade 6","Section I","4","Lunch","Breaks","n/a");
INSERT INTO schedule VALUES("thursday","Grade 6","Section I","4","Lunch","Breaks","n/a");
INSERT INTO schedule VALUES("wednesday","Grade 6","Section I","1","Algebra Grade 6","Mathematics","G6 - S1 Classroom");
INSERT INTO schedule VALUES("thursday","Grade 6","Section I","1","Algebra Grade 6","Mathematics","G6 - S1 Classroom");
INSERT INTO schedule VALUES("friday","Grade 6","Section I","1","Algebra Grade 6","Mathematics","G6 - S1 Classroom");
INSERT INTO schedule VALUES("monday","Grade 6","Section I","4","Lunch","Breaks","n/a");
INSERT INTO schedule VALUES("tuesday","Grade 6","Section I","4","Lunch","Breaks","n/a");
INSERT INTO schedule VALUES("tuesday","Grade 6","Section I","1","Algebra Grade 6","Mathematics","G6 - S1 Classroom");
INSERT INTO schedule VALUES("monday","Grade 6","Section I","1","Algebra Grade 6","Mathematics","G6 - S1 Classroom");
INSERT INTO schedule VALUES("friday","Grade 6","ALpha","1","PHILMAN","Philosophy and Religion","415");
INSERT INTO schedule VALUES("wednesday","Grade 1","St Michael","1","Math BP","Mathematics","n/a");
INSERT INTO schedule VALUES("monday","Grade 6","Section I","2","funda","Nursing","n/a");



DROP TABLE student;

CREATE TABLE `student` (
  `student_id` varchar(9) NOT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `mname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `address_home` text,
  `address_mail` text,
  `citizenship` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO student VALUES("1112-0001","Keiko Ellaine","Ricafort","Limsiaco","2000-08-08","f","Para�aque City","Para�aque City","Japanese","student_img/1112_0001.jpg");
INSERT INTO student VALUES("1112-0002","Charls","Nier","Delbari","2005-03-02","m","Pasig","Pasig","Filipino","student_img/noimage.jpg");
INSERT INTO student VALUES("1112-0003","Stephanie","Villanueva","Asuncion","2007-02-01","f","Makati","Makati","Filipino","student_img/noimage.jpg");
INSERT INTO student VALUES("1112-0004","Maria","Ozawa","Buzon","2011-01-01","f","Makati","Makati","Filipina","student_img/noimage.jpg");



DROP TABLE student_has_fees;

CREATE TABLE `student_has_fees` (
  `student_id` varchar(9) NOT NULL,
  `fee_id` int(11) NOT NULL,
  `adjustments` decimal(8,2) DEFAULT NULL,
  `school_year` varchar(45) NOT NULL,
  PRIMARY KEY (`student_id`,`fee_id`,`school_year`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO student_has_fees VALUES("1112-0001","1","0.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0001","2","0.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0001","3","0.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0001","4","0.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0001","5","0.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0001","6","0.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0001","7","0.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0001","8","0.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0001","9","0.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0004","1","-2000.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0004","2","0.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0004","3","0.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0004","4","0.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0004","5","0.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0004","6","0.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0004","7","0.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0004","8","0.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0004","9","0.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0011","14","0.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0013","12","0.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0013","13","0.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0013","14","0.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0014","12","0.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0014","13","0.00","2011-2012");
INSERT INTO student_has_fees VALUES("1112-0014","14","0.00","2011-2012");



DROP TABLE subjects;

CREATE TABLE `subjects` (
  `subject_code` varchar(45) NOT NULL,
  `dep_code` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `employee_id` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`subject_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO subjects VALUES("Algebra Grade 6","Mathematics","Algebra for Grade 6","2011-0003");
INSERT INTO subjects VALUES("Lunch","Breaks","Lunch Break","");
INSERT INTO subjects VALUES("funda","Nursing","","");



DROP TABLE system_default;

CREATE TABLE `system_default` (
  `system_default` int(11) NOT NULL,
  `school_year` varchar(45) NOT NULL,
  `quarter` int(11) NOT NULL,
  PRIMARY KEY (`system_default`,`school_year`,`quarter`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO system_default VALUES("0","2011-2012","1");



DROP TABLE system_logs;

CREATE TABLE `system_logs` (
  `log_num` int(11) NOT NULL,
  `module` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `employee_id` varchar(9) NOT NULL,
  `date_processed` date NOT NULL,
  `time_processed` time NOT NULL,
  PRIMARY KEY (`log_num`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO system_logs VALUES("1","Assess & Enroll","Student ID \'1112-0001\' - Status changed to: \'enrolled\'","2011-0001","2011-09-03","00:23:09");
INSERT INTO system_logs VALUES("2","User Groups","Permission \"delete_logs\" for \"Teacher\" was SET to Yes","2011-0001","2011-09-05","12:25:46");
INSERT INTO system_logs VALUES("3","User Groups","Permission \"delete_logs\" for \"Teacher\" was SET to No","2011-0001","2011-09-05","12:25:47");
INSERT INTO system_logs VALUES("29","User Groups","Permission \"is_VIP\" for \"Teacher\" was SET to Yes","2011-0001","2011-12-15","15:19:29");
INSERT INTO system_logs VALUES("30","User Groups","Permission \"is_VIP\" for \"Teacher\" was SET to No","2011-0001","2011-12-15","15:19:30");
INSERT INTO system_logs VALUES("31","User Groups","Permission \"control_delete\" for \"Teacher\" was SET to Yes","2011-0001","2011-12-15","15:19:37");
INSERT INTO system_logs VALUES("32","User Groups","Permission \"control_delete\" for \"Teacher\" was SET to No","2011-0001","2011-12-15","15:19:38");
INSERT INTO system_logs VALUES("21","Assess & Enroll","An Adjustment value of \'5000\' for Fee ID \'3\' was given to Student ID \'1112-0001\'","2011-0001","2011-11-24","09:34:48");
INSERT INTO system_logs VALUES("22","Assess & Enroll","An Adjustment value of \'-3000\' for Fee ID \'3\' was given to Student ID \'1112-0001\'","2011-0001","2011-11-24","09:35:09");
INSERT INTO system_logs VALUES("23","Assess & Enroll","An Adjustment value of \'0\' for Fee ID \'3\' was given to Student ID \'1112-0001\'","2011-0001","2011-12-02","10:37:15");
INSERT INTO system_logs VALUES("24","Assess & Enroll","An Adjustment value of \'9000\' for Fee ID \'3\' was given to Student ID \'1112-0001\'","2011-0001","2011-12-13","18:39:46");
INSERT INTO system_logs VALUES("25","Assess & Enroll","An Adjustment value of \'0\' for Fee ID \'3\' was given to Student ID \'1112-0001\'","2011-0001","2011-12-13","18:39:51");
INSERT INTO system_logs VALUES("26","Assess & Enroll","Student ID \'1112-0004\' - Status changed to: \'assessed\'","2011-0001","2011-12-15","15:10:08");
INSERT INTO system_logs VALUES("27","Assess & Enroll","An Adjustment value of \'-2000\' for Fee ID \'1\' was given to Student ID \'1112-0004\'","2011-0001","2011-12-15","15:10:28");
INSERT INTO system_logs VALUES("28","Assess & Enroll","Student ID \'1112-0004\' - Status changed to: \'enrolled\'","2011-0001","2011-12-15","15:14:38");



DROP TABLE timeslot;

CREATE TABLE `timeslot` (
  `timeslot_num` int(11) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  PRIMARY KEY (`timeslot_num`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO timeslot VALUES("4","10:30:00","11:30:00");
INSERT INTO timeslot VALUES("3","09:30:00","10:30:00");
INSERT INTO timeslot VALUES("2","08:30:00","09:30:00");
INSERT INTO timeslot VALUES("1","07:30:00","08:30:00");



DROP TABLE tuition;

CREATE TABLE `tuition` (
  `fee_id` int(11) NOT NULL,
  `payment_option` varchar(45) DEFAULT NULL,
  `upon_enrollment` decimal(8,2) DEFAULT NULL,
  `installment` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`fee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO tuition VALUES("1","annual","72000.00","0.00");
INSERT INTO tuition VALUES("2","semi_annual","36500.00","36500.00");
INSERT INTO tuition VALUES("3","quarterly","18500.00","18500.00");
INSERT INTO tuition VALUES("10","annual","0.00","0.00");
INSERT INTO tuition VALUES("11","semi_annual","0.00","0.00");
INSERT INTO tuition VALUES("12","quarterly","0.00","0.00");
INSERT INTO tuition VALUES("13","annual","0.00","0.00");
INSERT INTO tuition VALUES("14","semi_annual","0.00","0.00");
INSERT INTO tuition VALUES("15","quarterly","0.00","0.00");
INSERT INTO tuition VALUES("16","annual","0.00","0.00");
INSERT INTO tuition VALUES("17","semi_annual","0.00","0.00");
INSERT INTO tuition VALUES("18","quarterly","0.00","0.00");
INSERT INTO tuition VALUES("19","annual","0.00","0.00");
INSERT INTO tuition VALUES("20","semi_annual","0.00","0.00");
INSERT INTO tuition VALUES("21","quarterly","0.00","0.00");



DROP TABLE user_access;

CREATE TABLE `user_access` (
  `user_name` varchar(45) NOT NULL,
  `user_pass` varchar(45) DEFAULT NULL,
  `group_name` varchar(45) NOT NULL,
  `is_emp` int(11) NOT NULL,
  PRIMARY KEY (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO user_access VALUES("admin","12345","System Admin","1");
INSERT INTO user_access VALUES("bravojohnny","123456","Teacher","1");
INSERT INTO user_access VALUES("limsiacofamily","123456","guardian","0");
INSERT INTO user_access VALUES("mmfrigillana","123456","Teacher","1");
INSERT INTO user_access VALUES("msasuncion","123456","guardian","0");
INSERT INTO user_access VALUES("testfamily","123456","guardian","0");



