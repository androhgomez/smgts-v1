-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 31, 2012 at 11:30 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `smgts_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `bulletin`
--

CREATE TABLE IF NOT EXISTS `bulletin` (
  `bulletin_num` int(11) NOT NULL,
  `posted_date` date NOT NULL,
  `posted_time` time NOT NULL,
  `employee_id` varchar(9) NOT NULL,
  `title` varchar(200) NOT NULL,
  `bulletin_message` text NOT NULL,
  PRIMARY KEY (`bulletin_num`),
  KEY `employee_id` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bulletin`
--

INSERT INTO `bulletin` (`bulletin_num`, `posted_date`, `posted_time`, `employee_id`, `title`, `bulletin_message`) VALUES
(1, '2011-09-03', '00:01:51', '2011-0001', 'To: Teachers', 'You may view your respecting class loading by the Subject and Advisory Class Page by clicking the link in the Loading Section at the Navigation Bar.'),
(3, '2011-09-03', '00:59:11', '2011-0001', 'Tutorial: Adding Subjects and Teacher Loading', 'The amateur video is located here:<br />\r\n<br />\r\nClick Here: <a href="../Tutorial/Tutorial_1.html">LINK</a>'),
(5, '2011-09-05', '12:27:42', '2011-0001', 'Welcome to all.', 'This is the Spurgeon School Management System created by Project Novo.<br />\r\n<br />\r\nIt avails the following:<br />\r\n- Assessment and Enrollment of Students<br />\r\n- Scheduling of Classes<br />\r\n- Detailed Progress Reports regarding subjects'),
(6, '2012-02-21', '19:16:07', '2011-0001', 'Testing html', '<table border="0"><tr><td>Hello WOrld</td></tr></table>');

-- --------------------------------------------------------

--
-- Table structure for table `bulletin_allow`
--

CREATE TABLE IF NOT EXISTS `bulletin_allow` (
  `bulletin_num` int(11) NOT NULL,
  `group_name` varchar(45) NOT NULL,
  PRIMARY KEY (`bulletin_num`,`group_name`),
  KEY `group_name` (`group_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bulletin_allow`
--

INSERT INTO `bulletin_allow` (`bulletin_num`, `group_name`) VALUES
(5, 'Guardian'),
(3, 'Registrar'),
(5, 'Registrar'),
(1, 'System Admin'),
(3, 'System Admin'),
(5, 'System Admin'),
(6, 'System Admin'),
(1, 'Teacher'),
(3, 'Teacher'),
(5, 'Teacher');

-- --------------------------------------------------------

--
-- Table structure for table `category_percentage`
--

CREATE TABLE IF NOT EXISTS `category_percentage` (
  `subject_code` varchar(45) NOT NULL,
  `dep_code` varchar(45) NOT NULL,
  `category` varchar(45) NOT NULL,
  `percentage` decimal(5,2) NOT NULL,
  PRIMARY KEY (`category`),
  KEY `subject_code` (`subject_code`),
  KEY `dep_code` (`dep_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_percentage`
--

INSERT INTO `category_percentage` (`subject_code`, `dep_code`, `category`, `percentage`) VALUES
('Algebra Grade 6', 'Mathematics', 'Exams', 45.00),
('Algebra Grade 6', 'Mathematics', 'Homework', 10.00),
('funda', 'Nursing', 'overall', 100.00),
('Algebra Grade 6', 'Mathematics', 'Quizzes', 30.00),
('Algebra Grade 6', 'Mathematics', 'Seatwork', 15.00);

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE IF NOT EXISTS `class` (
  `class_name` varchar(45) NOT NULL,
  `level_id` varchar(45) NOT NULL,
  `class_adviser` varchar(9) DEFAULT NULL,
  `co_adviser` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`class_name`,`level_id`),
  KEY `class_adviser` (`class_adviser`),
  KEY `co_adviser` (`co_adviser`),
  KEY `level_id` (`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`class_name`, `level_id`, `class_adviser`, `co_adviser`) VALUES
('Section I', 'Grade 6', '2011-0002', NULL),
('Section II', 'Grade 6', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `class_level`
--

CREATE TABLE IF NOT EXISTS `class_level` (
  `level_id` varchar(45) NOT NULL,
  `category` int(1) DEFAULT NULL,
  `indexnum` int(1) NOT NULL,
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_level`
--

INSERT INTO `class_level` (`level_id`, `category`, `indexnum`) VALUES
('Beginner Prep', 0, 1),
('Grade 1', 1, 5),
('Grade 2', 1, 6),
('Grade 3', 1, 7),
('Grade 4', 1, 8),
('Grade 5', 1, 9),
('Grade 6', 1, 10),
('Junior Prep', 0, 3),
('Kinder Prep', 0, 11),
('Middle Prep', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `connection`
--

CREATE TABLE IF NOT EXISTS `connection` (
  `student_id` varchar(9) NOT NULL,
  `guardian_id` int(11) NOT NULL,
  `relationship` varchar(45) NOT NULL,
  `user_name` varchar(45) NOT NULL,
  PRIMARY KEY (`student_id`,`guardian_id`,`relationship`),
  KEY `guardian_id` (`guardian_id`),
  KEY `user_name` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `connection`
--

INSERT INTO `connection` (`student_id`, `guardian_id`, `relationship`, `user_name`) VALUES
('1112-0001', 1, 'father', 'limsiacofamily'),
('1112-0001', 1, 'guardian', 'limsiacofamily'),
('1112-0001', 1, 'mother', 'limsiacofamily'),
('1112-0004', 3, 'father', 'msasuncion'),
('1112-0004', 3, 'guardian', 'msasuncion'),
('1112-0004', 3, 'mother', 'msasuncion'),
('1112-0002', 2, 'father', 'testfamily'),
('1112-0002', 2, 'guardian', 'testfamily'),
('1112-0002', 2, 'mother', 'testfamily'),
('1112-0003', 2, 'father', 'testfamily'),
('1112-0003', 2, 'guardian', 'testfamily'),
('1112-0003', 2, 'mother', 'testfamily');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `dep_code` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `unofficial` varchar(3) NOT NULL,
  PRIMARY KEY (`dep_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`dep_code`, `description`, `unofficial`) VALUES
('Breaks', 'Recess, Lunch, Etc (Unofficial)', 'yes'),
('Mathematics', 'Full of Math', 'no'),
('Nursing', '', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `employee_id` varchar(9) NOT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `mname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `address_home` varchar(45) DEFAULT NULL,
  `address_mail` varchar(45) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `citizenship` varchar(45) DEFAULT NULL,
  `civil_status` varchar(45) DEFAULT NULL,
  `spouse` varchar(45) DEFAULT NULL,
  `sss_number` varchar(12) DEFAULT NULL,
  `pagibig_number` varchar(45) DEFAULT NULL,
  `philhealth_number` varchar(45) DEFAULT NULL,
  `TIN` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `user_name` varchar(45) NOT NULL,
  PRIMARY KEY (`employee_id`),
  KEY `user_name` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`employee_id`, `fname`, `mname`, `lname`, `birthdate`, `gender`, `address_home`, `address_mail`, `email`, `citizenship`, `civil_status`, `spouse`, `sss_number`, `pagibig_number`, `philhealth_number`, `TIN`, `image`, `user_name`) VALUES
('2011-0001', 'Gerard', 'Sorrera', 'Cruz', '1992-03-25', 'm', 'Pasay City', 'VAB, Pasay City', 'guix21@gmail.com', 'Filipino', 'single', NULL, NULL, NULL, NULL, NULL, 'emp_img/gerard.jpg', 'admin'),
('2011-0002', 'Marion', 'Mangaoang', 'Frigillana', '1970-01-01', 'm', 'Taguig City', 'Taguig City', 'mmfrigillana@apc.edu.ph', 'Filipino', 'married', 'Mark Buzon', '', '', '', '', 'emp_img/2011_0002.jpg', 'mmfrigillana'),
('2011-0003', 'Johnny', 'Bravo', 'Junior', '1980-03-10', 'm', 'Makati', 'Makati', 'bravo@mail.com', 'chinese', 'single', '', '', '', '', '', 'emp_img/noimage.jpg', 'bravojohnny');

-- --------------------------------------------------------

--
-- Table structure for table `enroll`
--

CREATE TABLE IF NOT EXISTS `enroll` (
  `student_id` varchar(9) NOT NULL,
  `school_year` varchar(45) NOT NULL,
  `class_name` varchar(45) NOT NULL,
  `level_id` varchar(45) NOT NULL,
  `estatus` varchar(45) DEFAULT NULL,
  `payment_option` varchar(45) DEFAULT NULL,
  `date_enrolled` date NOT NULL,
  PRIMARY KEY (`student_id`,`school_year`),
  KEY `level_id` (`level_id`),
  KEY `class_name` (`class_name`),
  KEY `payment_option` (`payment_option`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enroll`
--

INSERT INTO `enroll` (`student_id`, `school_year`, `class_name`, `level_id`, `estatus`, `payment_option`, `date_enrolled`) VALUES
('1112-0001', '2011-2012', 'Section I', 'Grade 6', 'enrolled', 'quarterly', '2011-09-03'),
('1112-0004', '2011-2012', 'Section II', 'Grade 6', 'enrolled', 'annual', '2011-12-15');

-- --------------------------------------------------------

--
-- Table structure for table `fees`
--

CREATE TABLE IF NOT EXISTS `fees` (
  `fee_id` int(11) NOT NULL,
  `level_id` varchar(45) NOT NULL,
  `fee_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`fee_id`),
  KEY `level_id` (`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fees`
--

INSERT INTO `fees` (`fee_id`, `level_id`, `fee_type`) VALUES
(1, 'Grade 6', 'tuition'),
(2, 'Grade 6', 'tuition'),
(3, 'Grade 6', 'tuition'),
(4, 'Grade 6', 'other'),
(5, 'Grade 6', 'other'),
(6, 'Grade 6', 'other'),
(7, 'Grade 6', 'other'),
(8, 'Grade 6', 'other'),
(9, 'Grade 6', 'other'),
(10, 'Beginner Prep', 'tuition'),
(11, 'Beginner Prep', 'tuition'),
(12, 'Beginner Prep', 'tuition'),
(13, 'Middle Prep', 'tuition'),
(14, 'Middle Prep', 'tuition'),
(15, 'Middle Prep', 'tuition'),
(16, 'Junior Prep', 'tuition'),
(17, 'Junior Prep', 'tuition'),
(18, 'Junior Prep', 'tuition'),
(19, 'Kinder Prep', 'tuition'),
(20, 'Kinder Prep', 'tuition'),
(21, 'Kinder Prep', 'tuition');

-- --------------------------------------------------------

--
-- Table structure for table `gradeitems`
--

CREATE TABLE IF NOT EXISTS `gradeitems` (
  `level_id` varchar(45) NOT NULL,
  `class_name` varchar(45) NOT NULL,
  `subject_code` varchar(45) NOT NULL,
  `gi_code` varchar(45) NOT NULL,
  `quarter` int(11) NOT NULL,
  `school_year` varchar(9) NOT NULL,
  `category` varchar(45) NOT NULL,
  PRIMARY KEY (`gi_code`),
  KEY `category` (`category`),
  KEY `level_id` (`level_id`),
  KEY `class_name` (`class_name`),
  KEY `subject_code` (`subject_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gradeitems`
--

INSERT INTO `gradeitems` (`level_id`, `class_name`, `subject_code`, `gi_code`, `quarter`, `school_year`, `category`) VALUES
('Grade 6', 'Section I', 'Algebra Grade 6', 'EX1', 1, '2011-2012', 'Exams'),
('Grade 6', 'Section I', 'funda', 'exam1', 1, '2011-2012', 'overall'),
('Grade 6', 'Section I', 'funda', 'exam2', 1, '2011-2012', 'overall'),
('Grade 6', 'Section I', 'Algebra Grade 6', 'HW1', 1, '2011-2012', 'Homework'),
('Grade 6', 'Section I', 'Algebra Grade 6', 'HW2', 1, '2011-2012', 'Homework'),
('Grade 6', 'Section I', 'Algebra Grade 6', 'Q1', 1, '2011-2012', 'Quizzes'),
('Grade 6', 'Section I', 'Algebra Grade 6', 'Q2', 1, '2011-2012', 'Quizzes'),
('Grade 6', 'Section I', 'Algebra Grade 6', 'SW1', 1, '2011-2012', 'Seatwork'),
('Grade 6', 'Section I', 'Algebra Grade 6', 'SW2', 1, '2011-2012', 'Seatwork');

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE IF NOT EXISTS `grades` (
  `subject_code` varchar(45) NOT NULL,
  `student_id` varchar(45) NOT NULL,
  `school_year` varchar(45) NOT NULL,
  `quarter` int(1) NOT NULL,
  `gi_code` varchar(45) NOT NULL,
  `student_grade` decimal(5,2) NOT NULL,
  PRIMARY KEY (`subject_code`,`student_id`,`school_year`,`quarter`,`gi_code`),
  KEY `student_id` (`student_id`),
  KEY `gi_code` (`gi_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`subject_code`, `student_id`, `school_year`, `quarter`, `gi_code`, `student_grade`) VALUES
('Algebra Grade 6', '1112-0001', '2011-2012', 1, 'EX1', 90.00),
('Algebra Grade 6', '1112-0001', '2011-2012', 1, 'HW1', 80.00),
('Algebra Grade 6', '1112-0001', '2011-2012', 1, 'HW2', 90.00),
('Algebra Grade 6', '1112-0001', '2011-2012', 1, 'Q1', -1.00),
('Algebra Grade 6', '1112-0001', '2011-2012', 1, 'Q2', 100.00),
('Algebra Grade 6', '1112-0001', '2011-2012', 1, 'SW1', 78.00),
('Algebra Grade 6', '1112-0001', '2011-2012', 1, 'SW2', 90.00),
('funda', '1112-0001', '2011-2012', 1, 'exam1', 76.00),
('funda', '1112-0001', '2011-2012', 1, 'exam2', 80.00);

-- --------------------------------------------------------

--
-- Table structure for table `group_permissions`
--

CREATE TABLE IF NOT EXISTS `group_permissions` (
  `group_name` varchar(45) NOT NULL,
  `is_VIP` int(11) NOT NULL,
  `control_add` int(11) DEFAULT NULL,
  `control_edit` int(11) DEFAULT NULL,
  `control_delete` int(11) DEFAULT NULL,
  `ref_students` int(11) DEFAULT NULL,
  `ref_guardian` int(11) DEFAULT NULL,
  `ref_employee` int(11) DEFAULT NULL,
  `ref_update_img` int(11) NOT NULL,
  `ref_assess_enroll` int(11) NOT NULL,
  `ref_department` int(11) DEFAULT NULL,
  `ref_class` int(11) DEFAULT NULL,
  `ref_class_level` int(11) DEFAULT NULL,
  `ref_fees` int(11) DEFAULT NULL,
  `ref_payment_dues` int(11) DEFAULT NULL,
  `ref_subject_list` int(11) DEFAULT NULL,
  `ref_schedule` int(11) DEFAULT NULL,
  `ref_grade_item` int(11) DEFAULT NULL,
  `ref_grade_item_type` int(11) DEFAULT NULL,
  `ref_grade` int(11) DEFAULT NULL,
  `ref_connection` int(11) DEFAULT NULL,
  `ref_timeslot` int(11) NOT NULL,
  `ref_update_adviser` int(11) NOT NULL,
  `user_access` int(11) DEFAULT NULL,
  `user_groups` int(11) DEFAULT NULL,
  `sys_system_default` int(11) DEFAULT NULL,
  `sys_bulletin` int(11) NOT NULL,
  `sys_backup` int(11) NOT NULL,
  `sys_log` int(11) NOT NULL,
  `ref_all_load` int(11) NOT NULL,
  `delete_logs` int(11) NOT NULL,
  PRIMARY KEY (`group_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_permissions`
--

INSERT INTO `group_permissions` (`group_name`, `is_VIP`, `control_add`, `control_edit`, `control_delete`, `ref_students`, `ref_guardian`, `ref_employee`, `ref_update_img`, `ref_assess_enroll`, `ref_department`, `ref_class`, `ref_class_level`, `ref_fees`, `ref_payment_dues`, `ref_subject_list`, `ref_schedule`, `ref_grade_item`, `ref_grade_item_type`, `ref_grade`, `ref_connection`, `ref_timeslot`, `ref_update_adviser`, `user_access`, `user_groups`, `sys_system_default`, `sys_bulletin`, `sys_backup`, `sys_log`, `ref_all_load`, `delete_logs`) VALUES
('Guardian', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('No Access / Disabled', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0),
('Registrar', 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, NULL, NULL, 1, 0, 1, 1, 1),
('System Admin', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
('Teacher', 0, 0, 1, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `guardian`
--

CREATE TABLE IF NOT EXISTS `guardian` (
  `guardian_id` int(11) NOT NULL,
  `relationship` varchar(45) NOT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `mname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `occupation` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone_landline` varchar(45) DEFAULT NULL,
  `phone_mobile` varchar(45) DEFAULT NULL,
  `emergency_contact` int(11) DEFAULT NULL,
  `living_with` int(11) DEFAULT NULL,
  PRIMARY KEY (`guardian_id`,`relationship`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guardian`
--

INSERT INTO `guardian` (`guardian_id`, `relationship`, `fname`, `mname`, `lname`, `occupation`, `email`, `phone_landline`, `phone_mobile`, `emergency_contact`, `living_with`) VALUES
(1, 'father', 'Edgar', 'Allan', 'Limsiacoe', 'Seaman', 'limsiacoe@mail.com', '9XX-XXXX', '09XXXXXXXXX', 1, 1),
(1, 'guardian', '', '', '', '', '', '', '', 0, 0),
(1, 'mother', 'Geraldine', 'Ricafort', 'Limsiacoe', 'Accountant', 'limsiacog@mail.com', '9XX-XXXX', '09XXXXXXXXX', 1, 1),
(2, 'father', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 1, 1),
(2, 'guardian', '', '', '', '', '', '', '', 0, 0),
(2, 'mother', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 1, 1),
(3, 'father', 'Mark ', 'Asuncion', 'Buzon', 'IT Manager', 'asd', '', '', 0, 0),
(3, 'guardian', 'Mae', '', '', '', '', '', '', 0, 0),
(3, 'mother', 'Stephanie', '', '', '', '', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `optional`
--

CREATE TABLE IF NOT EXISTS `optional` (
  `fee_id` int(11) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `cost` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`fee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `other`
--

CREATE TABLE IF NOT EXISTS `other` (
  `fee_id` int(11) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `cost` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`fee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `other`
--

INSERT INTO `other` (`fee_id`, `description`, `cost`) VALUES
(5, 'Computer Fee', '3025'),
(6, 'Materials', '605'),
(7, 'Graduation Fee', '1452'),
(8, 'Developmental Fees', '3000'),
(9, 'Miscellaneous', '4029');

-- --------------------------------------------------------

--
-- Table structure for table `payment_dues`
--

CREATE TABLE IF NOT EXISTS `payment_dues` (
  `payment_type` varchar(45) NOT NULL,
  `installment` int(11) NOT NULL,
  `due_date` date NOT NULL,
  `school_year` varchar(9) NOT NULL,
  PRIMARY KEY (`payment_type`,`installment`,`school_year`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_dues`
--

INSERT INTO `payment_dues` (`payment_type`, `installment`, `due_date`, `school_year`) VALUES
('quarterly', 2, '2011-08-15', '2011-2012'),
('quarterly', 3, '2011-11-15', '2011-2012'),
('quarterly', 4, '2012-01-15', '2011-2012'),
('semi-annual', 2, '2011-09-15', '2011-2012');

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE IF NOT EXISTS `schedule` (
  `schedule_day` varchar(15) NOT NULL,
  `level_id` varchar(45) NOT NULL,
  `class_name` varchar(45) NOT NULL,
  `timeslot_num` int(11) NOT NULL,
  `subject_code` varchar(45) NOT NULL,
  `dep_code` varchar(45) NOT NULL,
  `room_code` varchar(45) NOT NULL,
  PRIMARY KEY (`schedule_day`,`level_id`,`class_name`,`timeslot_num`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`schedule_day`, `level_id`, `class_name`, `timeslot_num`, `subject_code`, `dep_code`, `room_code`) VALUES
('friday', 'Grade 6', 'ALpha', 1, 'PHILMAN', 'Philosophy and Religion', '415'),
('friday', 'Grade 6', 'Section I', 1, 'Algebra Grade 6', 'Mathematics', 'G6 - S1 Classroom'),
('friday', 'Grade 6', 'Section I', 4, 'Lunch', 'Breaks', 'n/a'),
('monday', 'Grade 6', 'Section I', 1, 'Algebra Grade 6', 'Mathematics', 'G6 - S1 Classroom'),
('monday', 'Grade 6', 'Section I', 2, 'funda', 'Nursing', 'n/a'),
('monday', 'Grade 6', 'Section I', 4, 'Lunch', 'Breaks', 'n/a'),
('thursday', 'Grade 6', 'Section I', 1, 'Algebra Grade 6', 'Mathematics', 'G6 - S1 Classroom'),
('thursday', 'Grade 6', 'Section I', 4, 'Lunch', 'Breaks', 'n/a'),
('tuesday', 'Grade 6', 'Section I', 1, 'Algebra Grade 6', 'Mathematics', 'G6 - S1 Classroom'),
('tuesday', 'Grade 6', 'Section I', 4, 'Lunch', 'Breaks', 'n/a'),
('wednesday', 'Grade 1', 'St Michael', 1, 'Math BP', 'Mathematics', 'n/a'),
('wednesday', 'Grade 6', 'Section I', 1, 'Algebra Grade 6', 'Mathematics', 'G6 - S1 Classroom'),
('wednesday', 'Grade 6', 'Section I', 4, 'Lunch', 'Breaks', 'n/a');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `student_id` varchar(9) NOT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `mname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `address_home` text,
  `address_mail` text,
  `citizenship` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`student_id`, `fname`, `mname`, `lname`, `birthdate`, `gender`, `address_home`, `address_mail`, `citizenship`, `image`) VALUES
('1112-0001', 'Keiko Ellaine', 'Ricafort', 'Limsiaco', '2000-08-08', 'f', 'Parañaque City', 'Parañaque City', 'Japanese', 'student_img/1112_0001.jpg'),
('1112-0002', 'Charls', 'Nier', 'Delbari', '2005-03-02', 'm', 'Pasig', 'Pasig', 'Filipino', 'student_img/noimage.jpg'),
('1112-0003', 'Stephanie', 'Villanueva', 'Asuncion', '2007-02-01', 'f', 'Makati', 'Makati', 'Filipino', 'student_img/noimage.jpg'),
('1112-0004', 'Maria', 'Ozawa', 'Buzon', '2011-01-01', 'f', 'Makati', 'Makati', 'Filipina', 'student_img/noimage.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `student_has_fees`
--

CREATE TABLE IF NOT EXISTS `student_has_fees` (
  `student_id` varchar(9) NOT NULL,
  `fee_id` int(11) NOT NULL,
  `adjustments` decimal(8,2) DEFAULT NULL,
  `school_year` varchar(45) NOT NULL,
  PRIMARY KEY (`student_id`,`fee_id`,`school_year`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_has_fees`
--

INSERT INTO `student_has_fees` (`student_id`, `fee_id`, `adjustments`, `school_year`) VALUES
('1112-0001', 1, 0.00, '2011-2012'),
('1112-0001', 2, 0.00, '2011-2012'),
('1112-0001', 3, 0.00, '2011-2012'),
('1112-0001', 4, 0.00, '2011-2012'),
('1112-0001', 5, 0.00, '2011-2012'),
('1112-0001', 6, 0.00, '2011-2012'),
('1112-0001', 7, 0.00, '2011-2012'),
('1112-0001', 8, 0.00, '2011-2012'),
('1112-0001', 9, 0.00, '2011-2012'),
('1112-0004', 1, -2000.00, '2011-2012'),
('1112-0004', 2, 0.00, '2011-2012'),
('1112-0004', 3, 0.00, '2011-2012'),
('1112-0004', 4, 0.00, '2011-2012'),
('1112-0004', 5, 0.00, '2011-2012'),
('1112-0004', 6, 0.00, '2011-2012'),
('1112-0004', 7, 0.00, '2011-2012'),
('1112-0004', 8, 0.00, '2011-2012'),
('1112-0004', 9, 0.00, '2011-2012'),
('1112-0011', 14, 0.00, '2011-2012'),
('1112-0013', 12, 0.00, '2011-2012'),
('1112-0013', 13, 0.00, '2011-2012'),
('1112-0013', 14, 0.00, '2011-2012'),
('1112-0014', 12, 0.00, '2011-2012'),
('1112-0014', 13, 0.00, '2011-2012'),
('1112-0014', 14, 0.00, '2011-2012');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE IF NOT EXISTS `subjects` (
  `subject_code` varchar(45) NOT NULL,
  `dep_code` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `employee_id` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`subject_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`subject_code`, `dep_code`, `description`, `employee_id`) VALUES
('Algebra Grade 6', 'Mathematics', 'Algebra for Grade 6', '2011-0003'),
('funda', 'Nursing', '', NULL),
('Lunch', 'Breaks', 'Lunch Break', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `system_default`
--

CREATE TABLE IF NOT EXISTS `system_default` (
  `system_default` int(11) NOT NULL,
  `school_year` varchar(45) NOT NULL,
  `quarter` int(11) NOT NULL,
  PRIMARY KEY (`system_default`,`school_year`,`quarter`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_default`
--

INSERT INTO `system_default` (`system_default`, `school_year`, `quarter`) VALUES
(0, '2011-2012', 1);

-- --------------------------------------------------------

--
-- Table structure for table `system_logs`
--

CREATE TABLE IF NOT EXISTS `system_logs` (
  `log_num` int(11) NOT NULL,
  `module` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `employee_id` varchar(9) NOT NULL,
  `date_processed` date NOT NULL,
  `time_processed` time NOT NULL,
  PRIMARY KEY (`log_num`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_logs`
--

INSERT INTO `system_logs` (`log_num`, `module`, `description`, `employee_id`, `date_processed`, `time_processed`) VALUES
(1, 'Assess & Enroll', 'Student ID ''1112-0001'' - Status changed to: ''enrolled''', '2011-0001', '2011-09-03', '00:23:09'),
(2, 'User Groups', 'Permission "delete_logs" for "Teacher" was SET to Yes', '2011-0001', '2011-09-05', '12:25:46'),
(3, 'User Groups', 'Permission "delete_logs" for "Teacher" was SET to No', '2011-0001', '2011-09-05', '12:25:47'),
(21, 'Assess & Enroll', 'An Adjustment value of ''5000'' for Fee ID ''3'' was given to Student ID ''1112-0001''', '2011-0001', '2011-11-24', '09:34:48'),
(22, 'Assess & Enroll', 'An Adjustment value of ''-3000'' for Fee ID ''3'' was given to Student ID ''1112-0001''', '2011-0001', '2011-11-24', '09:35:09'),
(23, 'Assess & Enroll', 'An Adjustment value of ''0'' for Fee ID ''3'' was given to Student ID ''1112-0001''', '2011-0001', '2011-12-02', '10:37:15'),
(24, 'Assess & Enroll', 'An Adjustment value of ''9000'' for Fee ID ''3'' was given to Student ID ''1112-0001''', '2011-0001', '2011-12-13', '18:39:46'),
(25, 'Assess & Enroll', 'An Adjustment value of ''0'' for Fee ID ''3'' was given to Student ID ''1112-0001''', '2011-0001', '2011-12-13', '18:39:51'),
(26, 'Assess & Enroll', 'Student ID ''1112-0004'' - Status changed to: ''assessed''', '2011-0001', '2011-12-15', '15:10:08'),
(27, 'Assess & Enroll', 'An Adjustment value of ''-2000'' for Fee ID ''1'' was given to Student ID ''1112-0004''', '2011-0001', '2011-12-15', '15:10:28'),
(28, 'Assess & Enroll', 'Student ID ''1112-0004'' - Status changed to: ''enrolled''', '2011-0001', '2011-12-15', '15:14:38'),
(29, 'User Groups', 'Permission "is_VIP" for "Teacher" was SET to Yes', '2011-0001', '2011-12-15', '15:19:29'),
(30, 'User Groups', 'Permission "is_VIP" for "Teacher" was SET to No', '2011-0001', '2011-12-15', '15:19:30'),
(31, 'User Groups', 'Permission "control_delete" for "Teacher" was SET to Yes', '2011-0001', '2011-12-15', '15:19:37'),
(32, 'User Groups', 'Permission "control_delete" for "Teacher" was SET to No', '2011-0001', '2011-12-15', '15:19:38');

-- --------------------------------------------------------

--
-- Table structure for table `timeslot`
--

CREATE TABLE IF NOT EXISTS `timeslot` (
  `timeslot_num` int(11) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  PRIMARY KEY (`timeslot_num`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timeslot`
--

INSERT INTO `timeslot` (`timeslot_num`, `start_time`, `end_time`) VALUES
(1, '07:30:00', '08:30:00'),
(2, '08:30:00', '09:30:00'),
(3, '09:30:00', '10:30:00'),
(4, '10:30:00', '11:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `tuition`
--

CREATE TABLE IF NOT EXISTS `tuition` (
  `fee_id` int(11) NOT NULL,
  `payment_option` varchar(45) DEFAULT NULL,
  `upon_enrollment` decimal(8,2) DEFAULT NULL,
  `installment` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`fee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tuition`
--

INSERT INTO `tuition` (`fee_id`, `payment_option`, `upon_enrollment`, `installment`) VALUES
(1, 'annual', 72000.00, 0.00),
(2, 'semi_annual', 36500.00, 36500.00),
(3, 'quarterly', 18500.00, 18500.00),
(10, 'annual', 0.00, 0.00),
(11, 'semi_annual', 0.00, 0.00),
(12, 'quarterly', 0.00, 0.00),
(13, 'annual', 0.00, 0.00),
(14, 'semi_annual', 0.00, 0.00),
(15, 'quarterly', 0.00, 0.00),
(16, 'annual', 0.00, 0.00),
(17, 'semi_annual', 0.00, 0.00),
(18, 'quarterly', 0.00, 0.00),
(19, 'annual', 0.00, 0.00),
(20, 'semi_annual', 0.00, 0.00),
(21, 'quarterly', 0.00, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `user_access`
--

CREATE TABLE IF NOT EXISTS `user_access` (
  `user_name` varchar(45) NOT NULL,
  `user_pass` varchar(45) DEFAULT NULL,
  `group_name` varchar(45) NOT NULL,
  `is_emp` int(11) NOT NULL,
  PRIMARY KEY (`user_name`),
  KEY `group_name` (`group_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_access`
--

INSERT INTO `user_access` (`user_name`, `user_pass`, `group_name`, `is_emp`) VALUES
('admin', '12345', 'System Admin', 1),
('bravojohnny', '123456', 'Teacher', 1),
('limsiacofamily', '123456', 'guardian', 0),
('mmfrigillana', '123456', 'Teacher', 1),
('msasuncion', '123456', 'guardian', 0),
('testfamily', '123456', 'guardian', 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bulletin`
--
ALTER TABLE `bulletin`
  ADD CONSTRAINT `bulletin_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bulletin_allow`
--
ALTER TABLE `bulletin_allow`
  ADD CONSTRAINT `bulletin_allow_ibfk_2` FOREIGN KEY (`group_name`) REFERENCES `group_permissions` (`group_name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bulletin_allow_ibfk_1` FOREIGN KEY (`bulletin_num`) REFERENCES `bulletin` (`bulletin_num`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `category_percentage`
--
ALTER TABLE `category_percentage`
  ADD CONSTRAINT `category_percentage_ibfk_2` FOREIGN KEY (`dep_code`) REFERENCES `departments` (`dep_code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `category_percentage_ibfk_1` FOREIGN KEY (`subject_code`) REFERENCES `subjects` (`subject_code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `class`
--
ALTER TABLE `class`
  ADD CONSTRAINT `class_ibfk_3` FOREIGN KEY (`co_adviser`) REFERENCES `employee` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `class_ibfk_1` FOREIGN KEY (`level_id`) REFERENCES `class_level` (`level_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `class_ibfk_2` FOREIGN KEY (`class_adviser`) REFERENCES `employee` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `connection`
--
ALTER TABLE `connection`
  ADD CONSTRAINT `connection_ibfk_3` FOREIGN KEY (`user_name`) REFERENCES `user_access` (`user_name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `connection_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `connection_ibfk_2` FOREIGN KEY (`guardian_id`) REFERENCES `guardian` (`guardian_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`user_name`) REFERENCES `user_access` (`user_name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `enroll`
--
ALTER TABLE `enroll`
  ADD CONSTRAINT `enroll_ibfk_3` FOREIGN KEY (`level_id`) REFERENCES `class` (`level_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `enroll_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `enroll_ibfk_2` FOREIGN KEY (`class_name`) REFERENCES `class` (`class_name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fees`
--
ALTER TABLE `fees`
  ADD CONSTRAINT `fees_ibfk_1` FOREIGN KEY (`level_id`) REFERENCES `class_level` (`level_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `gradeitems`
--
ALTER TABLE `gradeitems`
  ADD CONSTRAINT `gradeitems_ibfk_7` FOREIGN KEY (`subject_code`) REFERENCES `subjects` (`subject_code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `gradeitems_ibfk_4` FOREIGN KEY (`category`) REFERENCES `category_percentage` (`category`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `gradeitems_ibfk_5` FOREIGN KEY (`level_id`) REFERENCES `class` (`level_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `gradeitems_ibfk_6` FOREIGN KEY (`class_name`) REFERENCES `class` (`class_name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `grades`
--
ALTER TABLE `grades`
  ADD CONSTRAINT `grades_ibfk_3` FOREIGN KEY (`gi_code`) REFERENCES `gradeitems` (`gi_code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `grades_ibfk_1` FOREIGN KEY (`subject_code`) REFERENCES `subjects` (`subject_code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `grades_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_access`
--
ALTER TABLE `user_access`
  ADD CONSTRAINT `user_access_ibfk_1` FOREIGN KEY (`group_name`) REFERENCES `group_permissions` (`group_name`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
