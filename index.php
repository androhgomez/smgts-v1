<?php
include("password_protect.php");
?>
<html>
    <head>
        <title>Project Novo School Management System</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="main_style.css" rel="stylesheet" type="text/css" media="screen">
        <link rel="shortcut icon" href="favicon.ico" />
        <link rel="stylesheet" type="text/css" href="dialog_box.css" />
        <script type="text/javascript" src="dialog_box.js"></script>
        <script src="js/jquery.js" type="text/javascript"></script>
        <script type="text/javascript" src="accordian.pack.js"></script>


        <SCRIPT LANGUAGE="JavaScript">

            function popUp(URL) {
                day = new Date();
                id = day.getTime();
                eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=700,height=600,left = 333,top = 84');");
            }

        </script>



    </head>
    <body bgcolor="#f2f2f2" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="new Accordian('basic-accordian',5,'header_highlight');">


        <!-- Save for Web Slices (smgtspsd.psd) -->
        <table id="Table_01" width="1100" height="501" border="0" cellpadding="0" cellspacing="0" align="center">
            <tr>
                <td colspan="3" background="images/logobg.png">
                    <a href="index.php"><img src="images/smgts_01_sub3.png" width="742" height="36" alt=""></a></td>
                <td background="images/smgts_02.gif" width="358" height="36" style="text-align:right; padding-right:15px">
                    <?php if ($_SESSION['is_emp'] == 1) { ?><a href="./references/view_emp.php?id=<?php echo $_SESSION['userid']; ?>" target="content_area"><?php echo $_SESSION['fullname']; ?></a><?php } else { ?>Logged as <a href="references/view_guardian.php" target="content_area"><?php echo $_SESSION['username']; ?></a><?php } ?> <img src="images/separator.png" /> <?php echo ucfirst($_SESSION['group']) . " Group"; ?> <img src="images/separator.png" /> <a href="index.php?logout=1">Logout</a></td><?php echo $_SESSION['ifmu']; ?>
            </tr>
            <tr>
                <td>
                    <img src="images/smgts_03.gif" width="268" height="23" alt=""></td>
                <td width="8" height="23">
                </td>
                <td colspan="2">
                    <img src="images/smgts_05.gif" width="824" height="23" alt=""></td>
            </tr>
            <tr>
                <td width="268" height="550" style="vertical-align:top">
                    <?php include('menus.php'); ?></td>
                <td width="8" height="550">
                </td>
                <td colspan="2" width="824" height="550" style="border-bottom:1px solid #cecece;border-left:1px solid #cecece;border-right:1px solid #cecece; background-color:#FFFFFF;">
                    <iframe name="content_area" id="content_area" src="home.php" height="100%" width="100%" allowtransparency="false" frameborder="0" scrolling="auto"></iframe>
                </td>
            </tr>
            <tr>
                <td>
                    <img src="images/spacer.gif" width="268" height="1" alt=""></td>
                <td>
                    <img src="images/spacer.gif" width="8" height="1" alt=""></td>
                <td>
                    <img src="images/spacer.gif" width="466" height="1" alt=""></td>
                <td>
                    <img src="images/spacer.gif" width="358" height="1" alt=""></td>
            </tr>
        </table>
    </body>

</html>