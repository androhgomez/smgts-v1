<?php
include('password_protect.php');
if ($gp['is_VIP'] == 1 || $gp['sys_bulletin'] == 1) {
	$grouplist;
	
	
	if(isset($_POST["submitbulletin"])) {
		$grouparr = array();
		$gpin = 0;
		$allowedtoview = $_POST["allowedtoview"];
		$title = $_POST["messagetitle"];
		$message = nl2br(str_replace("'","\'",$_POST["message"]));
		$postdate = date('Y') . "-" . date('m') . "-" . date('d');
		$posttime = date('G') . ":" . date('i') . ":" . date('s');
		$newbulnum = 0;
		
		for($i = 0; $i <= strlen($allowedtoview); $i++) {
			if(substr($allowedtoview, $i, 1) != ",") {
				$grouparr[$gpin] .= substr($allowedtoview, $i, 1);
			}
			else {
				$gpin++;
			}
		}
		
		$query = mysql_query("SELECT MAX(bulletin_num) 'bulletin_num' FROM bulletin");
		while($getmax = mysql_fetch_array($query)) {
			$newbulnum = $getmax[0] + 1;
		}
		
		$query = "INSERT INTO bulletin VALUES($newbulnum, '$postdate', '$posttime', '".$_SESSION['userid']."', '$title', '$message')";
		mysql_query($query);
		
		for($i = 0; $i <= $gpin;$i++) {
			mysql_query("INSERT INTO bulletin_allow VALUES($newbulnum, '".$grouparr[$i]."')");
		}
		?>
			<script type="text/javascript">
				alert("Message Posted. Check home page.");
			</script>
		<?php
	}

?>
<html>
<head>
<title>Bulletin</title>
<link href="main_style.css" rel="stylesheet" type="text/css">
<script type="text/javascript">

	var viparr = new Array();

	<?php
			$vipindex = 0;
			$query = mysql_query("SELECT group_name FROM group_permissions WHERE is_VIP = 1");
			while($getvips = mysql_fetch_array($query)) {
				?>
				viparr[<?php echo $vipindex; ?>] = "<?php echo $getvips[0]; ?>";
				<?php
				$vipindex++;
			}
	?>
		

	function checkarray(findstr) {
		var x = 0;
		var existinarr = false;
		for(x = 0; x <= viparr.length; x++) {
			if(viparr[x] == findstr) {
				existinarr = true;
			}
		}
		return existinarr;
	}

		
	function allowdisallow(str) {
		var viewyes = document.getElementById("view_yes");
		var viewno = document.getElementById("view_no");
		var i = 0;
		if(str=="Allow") {
			for(i=0; i<=(viewno.options.length - 1); i++) {
				if(viewno.options[i].selected) {
					viewyes.options[viewyes.options.length] = new Option(viewno.options[i].text, viewno.options[i].value);
					viewno.options[i] = null;
					i--;
				}
			}
		}
		else if(str=="Disallow") {
			for(i=0; i<=(viewyes.options.length - 1); i++) {
				if(viewyes.options[i].selected && !(checkarray(viewyes.options[i].value))) {
					viewno.options[viewno.options.length] = new Option(viewyes.options[i].text, viewyes.options[i].value);
					viewyes.options[i] = null;
					i--;
				}
			}
		}
	}
	
	function getallallowed() {
		var viewyes = document.getElementById("view_yes");
		var title = document.getElementById("messagetitle");
		var message = document.getElementById("message");
		var decision = false;
		
		var i = 0;
		var allowedusers = "";
		for(i = 0; i <= (viewyes.options.length - 1); i++) {
			if(i != 0) {
				allowedusers += ",";
			}
			allowedusers += viewyes.options[i].value;
		}
		
		document.getElementById("allowedtoview").value = allowedusers;
		
		if(title.value == "" || message.value == "") {
			alert("Title or message is empty");
			return false;
		}
		else {
			decision = confirm("Post Message? Check the fields if they're correct.");
			
			if (decision == true) {
				return true;
			}
			else {
				return false;
			}
		}
		
	}

</script>

<!--FOR SITE INDICATOR---->

<script src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
//  Developed by Roshan Bhattarai 
//  Visit http://roshanbh.com.np for this script and more.
//  This notice MUST stay intact for legal use
$(document).ready(function()
{
	//scroll the message box to the top offset of browser's scrool bar
	$(window).scroll(function()
	{
  		$('#message_box2').animate({top:$(window).scrollTop()+"px" },{queue: false, duration: 350});  
	});
    //when the close button at right corner of the message box is clicked 
	$('#close_message').click(function()
	{
  		//the messagebox gets scrool down with top property and gets hidden with zero opacity 
		$('#message_box2').animate({ top:"+=15px",opacity:0 }, "slow");
		$('#bulletintable').animate({ top:"-=43px" }, "slow")
	});
});
</script>


<!------>

</head>
<body topmargin="0">
<div id="message_box2"><img id="close_message" style="float:right;cursor:pointer"  src="images/cross.png" />
<img src="images/arrow.png"> <font color="green"><b>Bulletin</b></font>
</div>
<div id="bulletintable" style="position:absolute; top:45px">
<form method="post" onSubmit="return getallallowed()">
		<table border="0" cellpadding="3" cellspacing="0">
		<tr>
			<td width="100px"></td>
			<td colspan="3" style="padding-left:30px">Post New Message [ HTML codes are allowed ]</td>
		</tr>
		<tr>
			<td style="text-align:right;">Title</td>
			<td colspan="3" style="padding-left:30px"><input type="text" name="messagetitle" id="messagetitle" style="width:500px"></td>
		</tr>
		<tr>
			<td style="vertical-align:top; text-align:right;">Message</td>
			<td colspan="3" style="padding-left:30px"><textarea id="message" name="message" id="message" style="width:500px;height:250px"></textarea></td>
		</tr>
		<tr>
			<td style="vertical-align:top; text-align:right;">Allow user groups to view</td>
			<td style="padding-left:30px; text-align:center;">
			<font color="red">Not Allowed</font><br /><br />
			<select name="view_no" id="view_no" style="width:150px; height:100px" multiple>
			<?php
				$query = mysql_query("SELECT group_name, is_VIP FROM group_permissions");
				while($getgroups = mysql_fetch_array($query)) {
					if($getgroups[1] != 1) {
						echo "<option value='$getgroups[0]'>$getgroups[0]</option>";
					}
				}
			?>
			</select>
			</td>
			<td style="text-align:center; width:150px">
			<input type="button" id="add" value="Disallow" onClick="allowdisallow(this.value)" />
			<input type="button" id="remove" value="Allow"  onClick="allowdisallow(this.value)" />
			</td>
			<td style="vertical-align:top; text-align:center;">
			<font color="green">Allowed</font><br /><br />
			<select name="view_yes" id="view_yes" style="width:150px; height:100px" multiple>
			<?php
				$query = mysql_query("SELECT group_name, is_VIP FROM group_permissions");
				while($getgroups = mysql_fetch_array($query)) {
					if($getgroups[1] == 1) {
						echo "<option value='$getgroups[0]'>$getgroups[0]</option>";
					}
				}
			?>
			</select>
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td colspan="3" style="padding-left:30px; text-align:center;">System Policy: All users within a VIP group <b>can view all</b> bulletin messages</td>
		</tr>
		<tr>
			<td>
			</td>
			<td colspan="3" style="padding-left:30px; text-align:center;">
				<input type="hidden" name="allowedtoview" id="allowedtoview" values="" runat="server" />
				<input type="submit" name="submitbulletin" value="Post New Message" />
			</td>
		</tr>
		</table>
		</form>
		</div>
</body>
</html>
<?php
}
else {
	header("Location: home.php?erroraccess=1");
}
?>