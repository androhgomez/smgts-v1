<?php
	include("password_protect.php");
	if ($gp['is_VIP'] == 1 || $gp['sys_system_default'] == 1) {

	
	$error = 0;
	
	$stsy = 1994;
	$endsy = 1995;
	// Add new group
	if(isset($_POST['submitnewgroup']) && ($gp['is_VIP'] == 1 ||  $gp['control_add'] == 1)) {
		$query = "INSERT INTO group_permissions(group_name) VALUES('".ucfirst($_POST['newgroupname'])."')";
		mysql_query($query);
		
		$modulename = "User Groups";
		$description = "New Group added : ".ucfirst($_POST['newgroupname']);
	
		include("submitlog.php");
	}
	elseif($_POST['submitnewgroup'] && $gp['control_add'] == 1) {
		header("Location: index.php?erroraccess=1");
	}
	// Change Permission
	if($_GET['change'] == 1 && ($gp['is_VIP'] == 1 || $gp['control_edit'] == 1)) {
	$newval = 0;
		if($_GET['val'] == 1) {
			$newval = 0;
		}
		else {
			$newval = 1;
		}
		
	$query = "UPDATE group_permissions SET " .$_GET['fieldname']. " = '" .$newval. "' WHERE group_name = '" .$_GET['grp']."'";
	mysql_query($query);
	
	$permval = "";

	
	switch($newval) {
		case 0:
			$permval = "No";
			break;
		case 1:
			$permval = "Yes";
			break;
	}
	
	#Record to System Logs
	$modulename = "User Groups";
	$description = "Permission \"".$_GET['fieldname']."\" for \"".$_GET['grp']."\" was SET to $permval";
	include("submitlog.php");
	#End of System Logs Script

	}
	elseif($_GET['change'] == 1 && $gp['control_edit'] != 1) {
		header("Location: home.php?erroraccess=1");
	}
	
	//DELETE group
	if(isset($_POST['delete']) && ($gp['is_VIP'] == 1 || $_GET['control_delete'] == 1)) {
		$checkaccs = mysql_query("SELECT * FROM user_access WHERE group_name = '".$_POST['groups']."'");
		if(mysql_num_rows($checkaccs) > 0) {
			$error = 1;
		}
		else {
			mysql_query("DELETE FROM group_permissions WHERE group_name = '".$_POST['groups']."'");
			
			#Record to System Logs
			$modulename = "User Groups";
			$description = "Group : ".$_POST['groups']." was deleted";
			include("submitlog.php");
			#End of System Logs Script

		}
	}
	elseif(isset($_POST['delete']) && ($gp['is_VIP'] == 1 || $_GET['control_delete'] != 1)) {
		header("Location: home.php?erroraccess=1");
	}
	
	// Change Defaults
	if(isset($_POST["syssubmit"]) && ($gp['is_VIP'] == 1 || $gp['control_edit'] == 1)) {
		$query = "UPDATE system_default SET school_year = '" . $_POST["startsy"]."-".$_POST["endsy"] . "', quarter = " .$_POST["sysquarter"]. " WHERE system_default = 0";
		mysql_query($query);
	}
	elseif($_POST["syssubmit"] && $gp['control_edit'] == 1) {
		header("Location: home.php?erroraccess=1");
	}
	
	$query = mysql_query("SELECT * FROM system_default WHERE system_default = 0");
	$sysdefault = array();
	$sys_permissions;
	$index = 0;
	while($getdefaults = mysql_fetch_array($query)) {
		$sysdefault[0] = substr($getdefaults[1],0,4); // Start SY
		$sysdefault[1] = substr($getdefaults[1],5,9); // End SY
		$sysdefault[2] = $getdefaults[2]; // Quarter
	}
	
	$query = mysql_query("
	SELECT group_name 
	FROM group_permissions 
	WHERE 
		group_name <> 'guardian' AND 
		group_name <> 'system admin' AND
		group_name <> 'No Access / Disabled' 
	ORDER BY 1 DESC");
	while($getgroups = mysql_fetch_array($query)) {
		$sys_permissions[$index] = $getgroups;
		$index++;
	}
	
?>
<html>

<head>
<title>System Settings</title>
<link href="main_style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="dialog_box.css" />
<script type="text/javascript" src="dialog_box.js"></script>

<!--FOR SITE INDICATOR---->

<script src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
//  Developed by Roshan Bhattarai 
//  Visit http://roshanbh.com.np for this script and more.
//  This notice MUST stay intact for legal use
$(document).ready(function()
{
	//scroll the message box to the top offset of browser's scrool bar
	$(window).scroll(function()
	{
  		$('#message_box2').animate({top:$(window).scrollTop()+"px" },{queue: false, duration: 350});  
	});
    //when the close button at right corner of the message box is clicked 
	$('#close_message').click(function()
	{
  		//the messagebox gets scrool down with top property and gets hidden with zero opacity 
		$('#message_box2').animate({ top:"+=15px",opacity:0 }, "slow");
		$('#bulletintable').animate({ top:"-=40px" }, "slow")
	});
});
</script>


<!------>


<script type="text/javascript">

function sys()
{
	if(document.getElementById("sysedit").checked == true)
	{
		document.getElementById("startsy").disabled = false;
		document.getElementById("endsy").disabled = false;
		document.getElementById("sysquarter").disabled = false;
		document.getElementById("syssubmit").disabled = false;
	}
	else
	{
		document.getElementById("startsy").disabled = true;
		document.getElementById("endsy").disabled = true;
		document.getElementById("sysquarter").disabled = true;
		document.getElementById("syssubmit").disabled = true;
	}
}
function showpermissions(str)
{
if (str=="")
  {
  document.getElementById("txtHint").innerHTML="";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("divpermissions").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","getpermissions.php?q="+str,true);
xmlhttp.send();
}

</script>
</head>

<body TOPMARGIN="0">

<div id="message_box2"><img id="close_message" style="float:right;cursor:pointer"  src="images/cross.png" />
<img src="images/arrow.png"> <font color="green"><b>System Settings and User Groups</b></font>
</div>

<div id="content">
<div id="bulletintable" style="position:absolute; top:45px; width:98%;">
		<form action="" method="post">
        <table border="0" cellpadding="3" cellspacing="0">
        <tr><td width="100px" style="text-align:right;">System Defaults</td><td style="padding-left:30px"><input name="sysedit" type="checkbox" id="sysedit" value="1" onClick="sys()"></td></tr>
        <tr><td style="text-align:right">School Year</td><td style="padding-left:30px">
		<select name="startsy" id="startsy" DISABLED>
		<?php 
			for($x = $stsy ; $x <= 2030; $x++) {
				if($x == $sysdefault[0]) {
					echo "<option value='$x' selected>$x</option>";
				}
				else {
					echo "<option value='$x'>$x</option>";
				}
			}
		?>
		</select> - 
		<select name="endsy" id="endsy" DISABLED>
		<?php 
			for($x = $endsy ; $x <= 2030; $x++) {
				if($x == $sysdefault[1]) {
					echo "<option value='$x' selected>$x</option>";
				}
				else {
					echo "<option value='$x'>$x</option>";
				}
			}
		?>
		</select>
		
		</td></tr>
		
		<tr><td style="text-align:right">Quarter</td><td style="padding-left:30px"><select name="sysquarter" id="sysquarter" style="width:113px" disabled>
		  <option value="1" <?php if($sysdefault[2] == 1) { ?>selected<?php } ?> >First</option>
		  <option value="2" <?php if($sysdefault[2] == 2) { ?>selected<?php } ?> >Second</option>
		  <option value="3" <?php if($sysdefault[2] == 3) { ?>selected<?php } ?> >Third</option>
		  <option value="4" <?php if($sysdefault[2] == 4) { ?>selected<?php } ?> >Fourth</option>
		</select></td></tr>
		<tr>
		  <td style="text-align:right">&nbsp;</td>
		  <td style="padding-left:30px"><input name="syssubmit" id="syssubmit" type="submit" value="Edit Defaults" disabled /></td>
		  </tr>
        </table>
   	  </form>
		<center><hr style="width: 90%; margin:15px;" /></center>
        <form action="" method="post">
        <table border="0" cellpadding="3" cellspacing="0">
        <tr>
        
        <td width="100px" style="text-align:right; vertical-align:top;">User Groups</td>
        <td colspan="2" style="padding-left:30px; vertical-align:top">
        <select name="groups" id="groups" onChange="showpermissions(this.value)" style="width:65%">
		<?php
			for($i = 0; $i <= ($index - 1); $i++) {
		?>
			<option value="<?php echo $sys_permissions[$i][0] ?>" <?php if($_GET['grp'] == $sys_permissions[$i][0]) { echo "selected"; } ?>><?php echo $sys_permissions[$i][0] ?></option>
		<?php
			}
		?>
		</select>
		<input type="submit" name="delete" value="Delete" />
        </td></tr>
		<tr>
        <td style="padding-left:30px; vertical-align:top; text-align:right;">
        Permissions<br /><br />
		(Click values to on/off permission)
        </td>
        <td style="vertical-align:top">
		<script type="text/javascript">
			showpermissions(document.getElementById("groups").value);
		</script>
        <div id="divpermissions">
        
        </div>
        </td>
		</tr>
		<tr>
		<form method="POST">
		<td style="text-align:right">
			New Group Name
		</td>
		<td style="padding-left:30px; text-align:right">
			<input type="text" name="newgroupname" /> 
			<input type="submit" name="submitnewgroup">
		</td>
		</form>
        </tr>
        </table>
        </form>

<!-----Body-------->
</div>
</div>
<?php

if(isset($_POST['delete']) && $error == 1) {
	?>
	<script type="text/javascript">showDialog("Delete Error", "There are still accounts belonging to that group. <br /><br /><u>You may not delete the user group until such accounts are either moved or deleted</u><br /><br />There are still <font color=red><?php echo mysql_num_rows($checkaccs); ?></font> account(s) under <font color=red>\'<?php echo $_POST['groups']; ?>\' </font>group", "error", 5)</script>
	<?php
}
elseif(isset($_POST['delete']) && $error == 0) {
	?>
	<script type="text/javascript">showDialog("Delete Successful", "Group \'<?php echo $_POST['groups']; ?>\' was deleted", "success", 2)</script>
	<?php
}

?>
</body>

</html>
<?php

}
else {
	header("Location: home.php?erroraccess=1");
}
?>