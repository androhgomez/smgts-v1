<?php
$browser = $_SERVER['HTTP_USER_AGENT'];
if (strstr($browser, "MSIE")) {
    header("Location: ie-incompatible.html");
} elseif (strstr($browser, "Opera")) {
    header("Location: ie-incompatible.html");
} else {
    
}

session_start();

include('connection.php');

# Usage:
# Set usernames / passwords below between SETTINGS START and SETTINGS END.
# Open it in browser with "help" parameter to get the code
# to add to all files being protected. 
#    Example: password_protect.php?help
# Include protection string which it gave you into every file that needs to be protected
#
# Add following HTML code to your page where you want to have logout link
# <a href="http://www.example.com/path/to/protected/page.php?logout=1">Logout</a>
#
###############################################################

/*
  -------------------------------------------------------------------
  SAMPLE if you only want to request login and password on login form.
  Each row represents different user.

  $LOGIN_INFORMATION = array(
  'test' => 'testpass',
  'admin' => 'passwd'
  );

  --------------------------------------------------------------------
  SAMPLE if you only want to request only password on login form.
  Note: only passwords are listed

  $LOGIN_INFORMATION = array(
  'testpass',
  'passwd'
  );

  --------------------------------------------------------------------
 */

##################################################################
#  SETTINGS START
##################################################################
// Add login/password pairs below, like described above
// NOTE: all rows except last must have comma "," at the end of line
$LOGIN_INFORMATION = $_SESSION['LOGIN_INFORMATION'];
if (!$LOGIN_INFORMATION) {
    $LOGIN_INFORMATION = array();
}
$is_emp;
$gp = $_SESSION['group_permissions'];
$userid = $_SESSION['userid'];
$login = '';
//if(!$gp) {
//	$gp;
//}
// request login? true - show login and password boxes, false - password box only
define('USE_USERNAME', true);

// User will be redirected to this page after logout
define('LOGOUT_URL', 'index.php');

// time out after NN minutes of inactivity. Set to 0 to not timeout
define('TIMEOUT_MINUTES', 25);

// This parameter is only useful when TIMEOUT_MINUTES is not zero
// true - timeout time from last activity, false - timeout time from login
define('TIMEOUT_CHECK_ACTIVITY', true);

##################################################################
#  SETTINGS END
##################################################################
///////////////////////////////////////////////////////
// do not change code below
///////////////////////////////////////////////////////
// show usage example
if (isset($_GET['help'])) {
    die('Include following code into every page you would like to protect, at the very beginning (first line):<br>&lt;?php include("' . str_replace('\\', '\\\\', __FILE__) . '"); ?&gt;');
}

// timeout in seconds
$timeout = (TIMEOUT_MINUTES == 0 ? 0 : time() + TIMEOUT_MINUTES * 60);

// logout?
if (isset($_GET['logout'])) {
    setcookie("verify", '', $timeout, '/'); // clear password;
    header('Location: ' . LOGOUT_URL);
    session_destroy();
    exit();
}

if (!function_exists('showLoginPasswordProtect')) {

// show login form
    function showLoginPasswordProtect($error_msg) {
        $_SESSION['isuserlogged'] = false;
        ?>
        <html>
            <head>
                <title>School Management System</title>
                <link rel="shortcut icon" href="favicon.ico" />
                <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
                <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
                <link href="main_style.css" rel="stylesheet" type="text/css">
                <script src="js/jquery.js" type="text/javascript"></script>
                <script src="js/jqFancyTransitions.js" type="text/javascript"></script>
                <link rel="stylesheet" type="text/css" href="dialog_box.css" />
                <script type="text/javascript" src="dialog_box.js"></script>
                <script type="text/javascript">
                    if(self != top) {
                        top.location="../index.php";
                    }
                </script>
            </head>
            <body bgcolor="#319e3d" topmargin="0" rightmargin="0" leftmargin="0"><center>

                <div id="content" style="background-image:url('./images/bglogin.png'); width:800px; height:600px; margin-top:12px">
                    <center><img src="./images/loginpagelogo.png" /></center>
                    <table border="0" cellpadding="0" cellspacing="0" align="center" style="margin-top: 0px; border:1px solid #000; ">
                        <tr>
                            <td>
                                <div id="container">

                                    <div id='slideshowHolder' style="position:relative">
                                        <img src='ss/img1.JPG'  />
                                        <img src='ss/img2.JPG'  />
                                        <img src='ss/img3.JPG'  />
                                        <img src='ss/img4.JPG'  />
                                        <img src='ss/img5.JPG'  />
                                        <div id="col1" style="position:relative; float:left; z-index: 10000; background:#000; color:#FFF; width:800px; height:45px; opacity:0.8; vertical-align:middle;">
                                            <form method="post">
                                                <table border="0" cellspacing="0" cellpadding="3" align="center">
                                                    <tr>
                                                        <td style="text-align:right;"><font color="white">Username</font></td><td><input type="input" name="access_login" /></td>
                                                        <td style="text-align:right;"><font color="white">Password</font></td><td><input type="password" name="access_password" /></td>
                                                        <td colspan="2" style="text-align:center;"><input type="image" name="submit" src="images/sibutton.png"></td>
                                                    </tr>
                                                </table>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" align="center">
                        <tr>
                            <td width="800" style="text-align:right; color:#FFF; font-size:10px">School Management System | Version 1.1 | &COPY; Project Novo</td>
                        </tr>
                    </table>
                    <script>
                        $('#slideshowHolder').jqFancyTransitions({ width: 800, height: 531 });
                    </script>
                </div>
            </center>
        </body>
        </html>

        <?php if ($error_msg) { ?>
            <script type="text/javascript">showDialog("Login Error", "<?php echo $error_msg; ?>", "error", 2)</script>
        <?php } ?>

        <?php
        // stop at this point
        die();
    }

}

// user provided password
if (isset($_POST['access_login']) && isset($_POST['access_password'])) {
    $login = $_POST['access_login'];
    $pass = $_POST['access_password'];
    $query = mysql_query("SELECT * FROM user_access WHERE user_name = '$login' AND user_pass = '".crypt($pass, "chippermunky")."'");


    while ($getaccounts = mysql_fetch_array($query)) {
        $LOGIN_INFORMATION[$getaccounts[0]] = $getaccounts[1];
        $group = $getaccounts[2];
        $is_emp = $getaccounts[3];
    }

    $_SESSION['LOGIN_INFORMATION'] = $LOGIN_INFORMATION;

    if (!USE_USERNAME && !in_array(crypt($pass, "chippermunky"), $LOGIN_INFORMATION)
            || (USE_USERNAME && (!array_key_exists($login, $LOGIN_INFORMATION) || $LOGIN_INFORMATION[$login] != crypt($pass, "chippermunky") ) )
    ) {
        showLoginPasswordProtect("Incorrect username or password.");
    } else {

        $userlogged = $_POST['access_login'];
        $fullname;
        $userid;
        $group_permissions = array();


        if ($is_emp == 1) {
            $query = mysql_query("SELECT employee_id, CONCAT(fname, ' ', SUBSTR(mname, 1, 1), '. ', lname) 'fullname' FROM employee WHERE user_name = '$userlogged'");
        } else {
            $query = mysql_query("SELECT g.guardian_id, CONCAT(g.fname, ' ', SUBSTR(g.mname, 1, 1), '. ', g.lname) 'fullname' FROM guardian g LEFT JOIN connection c ON g.guardian_id = c.guardian_id WHERE c.user_name = '$userlogged'");
        }

        while ($getrecords = mysql_fetch_array($query)) {
            $userid = $getrecords[0];
            $fullname = $getrecords[1];
        }

        $query = mysql_query("SELECT gp.* FROM user_access ua LEFT JOIN group_permissions gp ON ua.group_name = gp.group_name WHERE ua.group_name = '$group'");
        while ($getrecords = mysql_fetch_array($query)) {
            $group_permissions = $getrecords;
        }

        $_SESSION['username'] = $userlogged;
        $_SESSION['fullname'] = $fullname;
        $_SESSION['userid'] = $userid;
        $_SESSION['is_emp'] = $is_emp;
        $_SESSION['group'] = $group;
        $_SESSION['group_permissions'] = $group_permissions;
        $gp = $group_permissions;

        // set cookie if password was validated
        setcookie("verify", md5($login . '%' . crypt($pass, "chippermunky")), $timeout, '/');

        // Some programs (like Form1 Bilder) check $_POST array to see if parameters passed
        // So need to clear password protector variables
        unset($_POST['access_login']);
        unset($_POST['access_password']);
        unset($_POST['Submit']);
    }
} else {

    $curdate = date('Y') . "-" . date('m') . "-" . date('d');
    $curtime = date('G') . ":" . date('i') . ":" . date('s');

    $group_permissions;

    $query = mysql_query("SELECT gp.* FROM user_access ua LEFT JOIN group_permissions gp ON ua.group_name = gp.group_name WHERE ua.group_name = '" . $_SESSION['group'] . "'");
    while ($getrecords = mysql_fetch_array($query)) {
        $group_permissions = $getrecords;
    }

    $sysde = array();

    $query = mysql_query("SELECT * FROM system_default WHERE system_default = 0");
    while ($getdefault = mysql_fetch_array($query)) {
        $sysde['ssy'] = substr($getdefault[1], 0, 4);
        $sysde['esy'] = substr($getdefault[1], 5, 8);
        $sysde['qtr'] = $getdefault[2];
    }

    $_SESSION['userid'] = $userid;
    $_SESSION['group_permissions'] = $group_permissions;
    $_SESSION['sysde'] = $sysde;
    $_SESSION['isuserlogged'] = true;
    $gp = $group_permissions;

    // check if password cookie is set
    if (!isset($_COOKIE['verify'])) {
        showLoginPasswordProtect("");
    }

    // check if cookie is good
    $found = false;
    foreach ($LOGIN_INFORMATION as $key => $val) {
        $lp = (USE_USERNAME ? $key : '') . '%' . $val;
        if ($_COOKIE['verify'] == md5($lp)) {
            $found = true;
            // prolong timeout
            if (TIMEOUT_CHECK_ACTIVITY) {
                setcookie("verify", md5($lp), $timeout, '/');
            }
            break;
        }
    }
    if (!$found) {
        showLoginPasswordProtect("");
    }
}
?>
