<?php
	include('pw_protect.php');
	$sid = $_GET['id'];
	$subj = $_GET['subject'];
	$quarter = 1;
	
	$getsystemsettings = mysql_query("SELECT * FROM system_default WHERE system_default = 0");
	

	while($settingsrecord = mysql_fetch_array($getsystemsettings)) {
		$schoolyear = $settingsrecord[1];
		$quarter = $settingsrecord[2];
	break;
	}
	
	if($_GET['qtr']) {
		$quarter = $_GET['qtr'];
	}
	
	$studentname = "";
	
	$querystudent = mysql_query("SELECT * FROM student WHERE student_id = '$sid'");
	
	while($getstudent = mysql_fetch_array($querystudent)) {
		$studentname = $getstudent[1] . " " . substr($getstudent[2], 0, 1) . ". " . $getstudent[3];
	}
	
?>
<html>
<head>
<title>Grades</title>
<link href="main_style.css" rel="stylesheet" type="text/css">
</head>

<body style="margin-left:0; margin-right:0;"><center>
<?php echo $studentname; ?><br />
<?php echo $subj ?> Grades
</center>
<div style="background-color:#569f00; margin-top:10px;margin-bottom:10px"><center>
	<font face="arial" size ="-1" color="#ffffff"><b>Select Quarter</b></font><br></center></div>
	<center>
<a href="viewgrades.php?id=<?php echo $sid ?>&subject=<?php echo $subj ?>&qtr=1">1st</a> <img src="images/button.png"> 
<a href="viewgrades.php?id=<?php echo $sid ?>&subject=<?php echo $subj ?>&qtr=2">2nd</a> <img src="images/button.png"> 
<a href="viewgrades.php?id=<?php echo $sid ?>&subject=<?php echo $subj ?>&qtr=3">3rd</a> <img src="images/button.png"> 
<a href="viewgrades.php?id=<?php echo $sid ?>&subject=<?php echo $subj ?>&qtr=4">4th</a>
</center>
<div style="background-color:#569f00; margin-top:10px; margin-bottom:10px"><center>
	<font face="arial" size ="-1" color="#ffffff"><b>
	<?php
	switch($quarter) {
				case 1:
					echo "First Quarter";
				break;	
				case 2:
					echo "Second Quarter";
				break;	
				case 3:
					echo "Third Quarter";
				break;	
				case 4:
					echo "Fourth Quarter";
				break;	
			}
	?>
	</b></font><br></center></div>

<?php


$categories = array();
$percentages = array();
$itemcount = array();
$totalgrades = array();
$converted = array();
$displaytotal = 0;
$totalnumberofitems = 0;

$queryallsubj = mysql_query("SELECT s.subject_code, s.dep_code, e.level_id, e.class_name FROM schedule s LEFT JOIN enroll e ON s.level_id = e.level_id AND s.class_name = e.class_name LEFT JOIN departments d ON s.dep_code = d.dep_code WHERE e.student_id = '$sid' AND d.unofficial = 'no' AND s.subject_code = '$subj' GROUP BY 1"); 

while($getallsubj = mysql_fetch_array($queryallsubj)) {

	$index = 0;
	$dept = $getallsubj['dep_code'];
	$subject = $getallsubj['subject_code'];
	$section = $getallsubj['class_name'];
	$level = $getallsubj['level_id'];

	$getempname = mysql_query("SELECT CONCAT(fname, ' ', SUBSTR(mname, 1, 1), ' ', lname) FROM subjects WHERE subject_code = '$subject'");
	
	$getstudents = mysql_query("SELECT s.* FROM student s LEFT JOIN enroll e ON s.student_id = e.student_id WHERE e.student_id = '$studentid' AND e.class_name = '$section' AND e.level_id = '$level' AND e.estatus = 'enrolled' ORDER BY s.lname");
	
	
	$getcategories = mysql_query("SELECT g.category, COUNT( g.gi_code ), c.percentage 'numitems'
									FROM category_percentage c
									LEFT JOIN gradeitems g ON c.category = g.category
									WHERE g.level_id = '$level'
									AND g.class_name = '$section'
									AND g.subject_code =  '$subject'
									AND c.dep_code =  '$dept' 
									AND g.quarter = '$quarter' 
									AND g.school_year = '$schoolyear'
									GROUP BY 1");
															
									
	$columns = 0;
	
	while($record = mysql_fetch_array($getcategories)) {
		$categories[0][$index] = $record[0]; // Name of Category
		$categories[1][$index] = $record[1]; // Number of items per category
		$percentages[$index] = $record[2];
		$itemcount[$index] = $record[1];
		$index++;
		$columns += $record[1];
		$totalnumberofitems += $record[1];
	}
	
	if($index < 1) {
		echo "<center>Grading Unavailable</center>";
	}
	else {
	?>
	
	<table border="1" cellpadding="3" cellspacing="0" align="center">
		<?php
				$converted = array();
				$totalgrades = array();
				$exemptions = array();
				$displaytotal = 0;
			for($index_2 = 0; $index_2 < $index; $index_2++) {
				$index_3 = 0;
				$getitems = mysql_query("SELECT * FROM gradeitems WHERE category = '".$categories[0][$index_2]."' AND subject_code = '$subject' AND quarter = '$quarter' AND school_year = '$schoolyear' ORDER BY gi_code");
				
				while($record = mysql_fetch_array($getitems)) {
										
					$temprecgrade = 0;
					
					$getgrades = mysql_query("SELECT student_grade FROM grades WHERE subject_code = '$subject' AND student_id = '" . $sid . "' AND school_year = '$schoolyear' AND quarter = '$quarter' AND gi_code = '" . $record['gi_code'] . "'");	
						if($index_3 == 0) {
							?>
								<tr>
									<td rowspan="<?php echo $categories[1][$index_2]; ?>"><?php echo $categories[0][$index_2]; ?></td>
									<td><?php echo $record['gi_code'] ?></td>
									<td>
										<?php
										if(mysql_num_rows($getgrades) < 1) {
											?>
												<font color="#de751a">n/a</font>
											<?php
												$temprecgrade = 0; // Zero for Temporary Grade for Totalling
												$exemptions[$index_2] =+ 1;
											}
											else {
											
												while($record2 = mysql_fetch_array($getgrades)) {					
													if($record2[0] < 0) {
													?>
														<font color="613ec7">Exempted</font>
													<?php
													$exemptions[$index_2] =+ 1;
													}
													else {
													
													?>
														<?php echo $record2[0]; ?>
													<?php
													$temprecgrade = $record2[0]; // Insert record into Temporary Grade for Totalling
													break;	
													}
												}
											
											}
											$totalgrades[$index_2] += $temprecgrade;

										?>
									</td>
								</tr>
							<?php
							$index_3 += 1;
						}
						else {
							?>
							<tr>
								<td><?php echo $record['gi_code'] ?></td>
								<td>
										<?php
										if(mysql_num_rows($getgrades) < 1) {
											?>
												<font color="#de751a">n/a</font>
											<?php
												$temprecgrade = 0; // Zero for Temporary Grade for Totalling
											}
											else {
											
												while($record2 = mysql_fetch_array($getgrades)) {					
													if($record2[0] < 0) {
													?>
														<font color="613ec7">Exempted</font>
													<?php
													$exemptions[$index_2] =+ 1;
													}
													else {
													
													?>
														<?php echo $record2[0]; ?>
													<?php
													$temprecgrade = $record2[0]; // Insert record into Temporary Grade for Totalling
													break;	
													}
												}
											
											}
											$totalgrades[$index_2] += $temprecgrade;

										?>
								</td>
							</tr>
							<?php
						}
				}
			}
			for($i = 0; $i < count($totalgrades); $i++) {
					if($exemptions[$i] > 0 && $itemcount[$i] != 1) {
						$converted[$i] = ($totalgrades[$i] / ($itemcount[$i] - $exemptions[$i])) * ($percentages[$i]/100);
						$displaytotal += $converted[$i];
					}
					else {
						$converted[$i] = ($totalgrades[$i] / $itemcount[$i]) * ($percentages[$i]/100);
						$displaytotal += $converted[$i];
					}
					
				}
			?>
			<tr>
			<td style="background-color:#ef968b" colspan="2">Total</td>
			<td><?php echo number_format($displaytotal, 2, '.', ''); ?></td>
			</tr>
			<?php
			
		?>
	</table>
	
	<?php
		}
	}
	?>
</body>

</html>