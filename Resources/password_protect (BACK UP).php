<?php
session_start();
include('connection.php');

# Usage:
# Set usernames / passwords below between SETTINGS START and SETTINGS END.
# Open it in browser with "help" parameter to get the code
# to add to all files being protected. 
#    Example: password_protect.php?help
# Include protection string which it gave you into every file that needs to be protected
#
# Add following HTML code to your page where you want to have logout link
# <a href="http://www.example.com/path/to/protected/page.php?logout=1">Logout</a>
#
###############################################################

/*
-------------------------------------------------------------------
SAMPLE if you only want to request login and password on login form.
Each row represents different user.

$LOGIN_INFORMATION = array(
  'test' => 'testpass',
  'admin' => 'passwd'
);

--------------------------------------------------------------------
SAMPLE if you only want to request only password on login form.
Note: only passwords are listed

$LOGIN_INFORMATION = array(
  'testpass',
  'passwd'
);

--------------------------------------------------------------------
*/

##################################################################
#  SETTINGS START
##################################################################

// Add login/password pairs below, like described above
// NOTE: all rows except last must have comma "," at the end of line
$LOGIN_INFORMATION = $_SESSION['LOGIN_INFORMATION'];
if(!$LOGIN_INFORMATION) {
	$LOGIN_INFORMATION = array();
}
$is_emp;
$gp = $_SESSION['group_permissions'];
$userid = $_SESSION['userid'];
//if(!$gp) {
//	$gp;
//}

// request login? true - show login and password boxes, false - password box only
define('USE_USERNAME', true);

// User will be redirected to this page after logout
define('LOGOUT_URL', 'index.php');

// time out after NN minutes of inactivity. Set to 0 to not timeout
define('TIMEOUT_MINUTES', 0);

// This parameter is only useful when TIMEOUT_MINUTES is not zero
// true - timeout time from last activity, false - timeout time from login
define('TIMEOUT_CHECK_ACTIVITY', true);

##################################################################
#  SETTINGS END
##################################################################


///////////////////////////////////////////////////////
// do not change code below
///////////////////////////////////////////////////////

// show usage example
if(isset($_GET['help'])) {
  die('Include following code into every page you would like to protect, at the very beginning (first line):<br>&lt;?php include("' . str_replace('\\','\\\\',__FILE__) . '"); ?&gt;');
}

// timeout in seconds
$timeout = (TIMEOUT_MINUTES == 0 ? 0 : time() + TIMEOUT_MINUTES * 60);

// logout?
if(isset($_GET['logout'])) {
  setcookie("verify", '', $timeout, '/'); // clear password;
  header('Location: ' . LOGOUT_URL);
  session_destroy();
  exit();
}

if(!function_exists('showLoginPasswordProtect')) {

// show login form
function showLoginPasswordProtect($error_msg) {
?>
<html>
<head>
  <title>Spurgeon School Management System</title>
  <link rel="shortcut icon" href="favicon.ico" />
  <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
  <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
  <link href="main_style.css" rel="stylesheet" type="text/css">
  
</head>
<body>
  
  
<table id="pp" border="0" cellpadding="0" cellspacing="0" align="center">
<tr>
<td rowspan="5">
<img src="images/login_01.png" width="119" height="600" alt=""></td>
<td colspan="3">
<img src="images/login_02.png" width="584" height="152" alt=""></td>
<td rowspan="5">
<img src="images/login_03.png" width="97" height="600" alt=""></td>
</tr>
<tr>
<td colspan="3">
<img src="images/login_04.png" width="584" height="29" alt=""></td>
</tr>
<tr>
<td rowspan="2">
<img src="images/login_05.png" width="14" height="183" alt=""></td>
<td id="login_area" background="images/login_06.png" width="557" height="172">
<form method="post">
    <table border="0" cellspacing="0" cellpadding="3" align="center">
	<tr>
    <td colspan="2" style="text-align:center;"><font color="white">Sign in</font></td>
	</tr>
	<?php if($error_msg) { ?>
	<tr>
    <td colspan="2" style="text-align:center;"><font color="red"><?php echo $error_msg; ?></font><td>
	</tr>
	<?php } ?>
	<tr>
	<td style="text-align:right;"><font color="white">Username:</font></td><td><input type="input" name="access_login" /></td>
	</tr>
	<tr>
	<td style="text-align:right;"><font color="white">Password:</font></td><td><input type="password" name="access_password" /></td>
	</tr>
	<tr>
	<td colspan="2" style="text-align:center;"><input type="submit" name="Submit" value="Submit" /></td>
	</tr>
	</table>
</form>


</td>
<td rowspan="2">
<img src="images/login_07.png" width="13" height="183" alt=""></td>
</tr>
<tr>
<td>
<img src="images/login_08.png" width="557" height="11" alt=""></td>
</tr>
<tr>
<td colspan="3">
<img src="images/login_09.png" width="584" height="236" alt=""></td>
</tr>
</table>
  
</body>
</html>

<?php
  // stop at this point
  die();
}
}

// user provided password
if (isset($_POST['access_password'])) {

  $login = isset($_POST['access_login']) ? $_POST['access_login'] : '';
  $pass = $_POST['access_password'];
  
  $query = mysql_query("SELECT * FROM user_access WHERE user_name = '$login' AND user_pass = '$pass'");

  while($getaccounts = mysql_fetch_array($query)) {
	$LOGIN_INFORMATION[$getaccounts[0]] = $getaccounts[1];
	$group = $getaccounts[2];
	$is_emp = $getaccounts[3];
  }
  
  $_SESSION['LOGIN_INFORMATION'] = $LOGIN_INFORMATION;

  if (!USE_USERNAME && !in_array($pass, $LOGIN_INFORMATION)
  || (USE_USERNAME && ( !array_key_exists($login, $LOGIN_INFORMATION) || $LOGIN_INFORMATION[$login] != $pass ) ) 
  ) {
    showLoginPasswordProtect("Incorrect password.");
  }
  else {

	$userlogged = $_POST['access_login'];
	$fullname;
	$userid;
	$group_permissions = array();

	
	if($is_emp == 1) {
		$query = mysql_query("SELECT employee_id, CONCAT(fname, ' ', SUBSTR(mname, 1, 1), '. ', lname) 'fullname' FROM employee WHERE user_name = '$userlogged'");
	}
	else {
		$query = mysql_query("SELECT g.guardian_id, CONCAT(g.fname, ' ', SUBSTR(g.mname, 1, 1), '. ', g.lname) 'fullname' FROM guardian g LEFT JOIN connection c ON g.guardian_id = c.guardian_id WHERE c.user_name = '$userlogged'");
	}
	
	while($getrecords = mysql_fetch_array($query)) {
		$userid = $getrecords[0];
		$fullname = $getrecords[1];
	}
	
	$query = mysql_query("SELECT gp.* FROM user_access ua LEFT JOIN group_permissions gp ON ua.group_name = gp.group_name WHERE ua.group_name = '$group'");
	while($getrecords = mysql_fetch_array($query)) {
		$group_permissions = $getrecords;
	}
	
	$_SESSION['username'] = $userlogged;
	$_SESSION['fullname'] = $fullname;
	$_SESSION['userid'] = $userid;
	$_SESSION['is_emp'] = $is_emp;
	$_SESSION['group'] = $group;
	$_SESSION['group_permissions'] = $group_permissions;
	$gp = $group_permissions;
	
    // set cookie if password was validated
    setcookie("verify", md5($login.'%'.$pass), $timeout, '/');
    
    // Some programs (like Form1 Bilder) check $_POST array to see if parameters passed
    // So need to clear password protector variables
    unset($_POST['access_login']);
    unset($_POST['access_password']);
    unset($_POST['Submit']);
  }
}

else {

  $group_permissions;

  $query = mysql_query("SELECT gp.* FROM user_access ua LEFT JOIN group_permissions gp ON ua.group_name = gp.group_name WHERE ua.group_name = '".$_SESSION['group']."'");
	while($getrecords = mysql_fetch_array($query)) {
		$group_permissions = $getrecords;
  }
  
  $sysde = array();
  
  $query = mysql_query("SELECT * FROM system_default WHERE system_default = 0");
  while($getdefault = mysql_fetch_array($query)) {
	$sysde['ssy'] = substr($getdefault[1], 0, 4);
	$sysde['esy'] = substr($getdefault[1], 5, 8);
	$sysde['qtr'] = $getdefault[2];

  }
  
  $_SESSION['userid'] = $userid;
  $_SESSION['group_permissions'] = $group_permissions;
  $_SESSION['sysde'] = $sysde;
  $gp = $group_permissions;

  // check if password cookie is set
  if (!isset($_COOKIE['verify'])) {
    showLoginPasswordProtect("");
  }

  // check if cookie is good
  $found = false;
  foreach($LOGIN_INFORMATION as $key=>$val) {
    $lp = (USE_USERNAME ? $key : '') .'%'.$val;
    if ($_COOKIE['verify'] == md5($lp)) {
      $found = true;
      // prolong timeout
      if (TIMEOUT_CHECK_ACTIVITY) {
        setcookie("verify", md5($lp), $timeout, '/');
      }
      break;
    }
  }
  if (!$found) {
    showLoginPasswordProtect("");
  }

}

?>
