-- phpMyAdmin SQL Dump
-- version 2.11.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 22, 2013 at 11:02 AM
-- Server version: 5.1.57
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `a1732472_pnold`
--

-- --------------------------------------------------------

--
-- Table structure for table `bulletin`
--

CREATE TABLE `bulletin` (
  `bulletin_num` int(11) NOT NULL,
  `posted_date` date NOT NULL,
  `posted_time` time NOT NULL,
  `employee_id` varchar(9) NOT NULL,
  `title` varchar(200) NOT NULL,
  `bulletin_message` text NOT NULL,
  PRIMARY KEY (`bulletin_num`),
  KEY `employee_id` (`employee_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bulletin`
--

INSERT INTO `bulletin` VALUES(1, '2013-01-04', '19:53:16', '2011-0001', 'Welcome to SMGTS!', 'SMGTS or School Management System is an application to be deployed on a local server. The core features are the following: <br />\r\n<br /><br />\r\n<ul><br />\r\n<li>Grade System</li><br />\r\n<li>Tuition Management</li><br />\r\n<li>Class and Loading Management</li><br />\r\n</ul>');
INSERT INTO `bulletin` VALUES(2, '2013-01-04', '19:54:06', '2011-0001', 'Welcome to SMGTS!', 'A project still under development.');
INSERT INTO `bulletin` VALUES(3, '2013-01-08', '20:18:50', '2011-0001', 'Welcome to SMGTS!', 'This is the Spurgeon School Management System created by Project Novo.<br />\r\n<br />\r\nIt avails the following:<br />\r\n- Assessment and Enrollment of Students<br />\r\n- Scheduling of Classes<br />\r\n- Detailed Progress Reports regarding subjects');

-- --------------------------------------------------------

--
-- Table structure for table `bulletin_allow`
--

CREATE TABLE `bulletin_allow` (
  `bulletin_num` int(11) NOT NULL,
  `group_name` varchar(45) NOT NULL,
  PRIMARY KEY (`bulletin_num`,`group_name`),
  KEY `group_name` (`group_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bulletin_allow`
--

INSERT INTO `bulletin_allow` VALUES(3, 'Guardian');
INSERT INTO `bulletin_allow` VALUES(3, 'Registrar');
INSERT INTO `bulletin_allow` VALUES(3, 'System Admin');
INSERT INTO `bulletin_allow` VALUES(3, 'Teacher');

-- --------------------------------------------------------

--
-- Table structure for table `category_percentage`
--

CREATE TABLE `category_percentage` (
  `subject_code` varchar(45) NOT NULL,
  `dep_code` varchar(45) NOT NULL,
  `category` varchar(45) NOT NULL,
  `percentage` decimal(5,2) NOT NULL,
  PRIMARY KEY (`subject_code`,`dep_code`,`category`),
  KEY `dep_code` (`dep_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_percentage`
--

INSERT INTO `category_percentage` VALUES('Math Grade 1', 'Mathematics', 'Exam', 40.00);
INSERT INTO `category_percentage` VALUES('Math Grade 1', 'Mathematics', 'HW', 10.00);
INSERT INTO `category_percentage` VALUES('Math Grade 1', 'Mathematics', 'Project', 15.00);
INSERT INTO `category_percentage` VALUES('Math Grade 1', 'Mathematics', 'Recitation', 25.00);
INSERT INTO `category_percentage` VALUES('Math Grade 1', 'Mathematics', 'SW', 10.00);
INSERT INTO `category_percentage` VALUES('WORLITT 5', 'Philosophy and Literature', 'Attendance', 5.00);
INSERT INTO `category_percentage` VALUES('WORLITT 5', 'Philosophy and Literature', 'Homework', 15.00);
INSERT INTO `category_percentage` VALUES('WORLITT 5', 'Philosophy and Literature', 'Mid Quarter Exam', 20.00);
INSERT INTO `category_percentage` VALUES('WORLITT 5', 'Philosophy and Literature', 'Participation', 10.00);
INSERT INTO `category_percentage` VALUES('WORLITT 5', 'Philosophy and Literature', 'Projects', 30.00);
INSERT INTO `category_percentage` VALUES('WORLITT 5', 'Philosophy and Literature', 'Quarter Exam', 20.00);

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE `class` (
  `class_name` varchar(45) NOT NULL,
  `level_id` varchar(45) NOT NULL,
  `class_adviser` varchar(9) DEFAULT NULL,
  `co_adviser` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`class_name`,`level_id`),
  KEY `class_adviser` (`class_adviser`),
  KEY `co_adviser` (`co_adviser`),
  KEY `level_id` (`level_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class`
--

INSERT INTO `class` VALUES('St. John', 'Grade 1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `class_level`
--

CREATE TABLE `class_level` (
  `level_id` varchar(45) NOT NULL,
  `category` int(1) DEFAULT NULL,
  `indexnum` int(1) NOT NULL,
  PRIMARY KEY (`level_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_level`
--

INSERT INTO `class_level` VALUES('Grade 1', 1, 1);
INSERT INTO `class_level` VALUES('Grade 2', 1, 2);
INSERT INTO `class_level` VALUES('Grade 3', 1, 3);
INSERT INTO `class_level` VALUES('Grade 4', 1, 4);
INSERT INTO `class_level` VALUES('Grade 5', 1, 5);
INSERT INTO `class_level` VALUES('Grade 6', 1, 6);
INSERT INTO `class_level` VALUES('Grade 7', 1, 7);

-- --------------------------------------------------------

--
-- Table structure for table `connection`
--

CREATE TABLE `connection` (
  `student_id` varchar(9) NOT NULL,
  `guardian_id` int(11) NOT NULL,
  `relationship` varchar(45) NOT NULL,
  `user_name` varchar(45) NOT NULL,
  PRIMARY KEY (`student_id`,`guardian_id`,`relationship`),
  KEY `guardian_id` (`guardian_id`),
  KEY `user_name` (`user_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `connection`
--

INSERT INTO `connection` VALUES('1314-0001', 1, 'father', 'limsiacofamily');
INSERT INTO `connection` VALUES('1314-0001', 1, 'guardian', 'limsiacofamily');
INSERT INTO `connection` VALUES('1314-0001', 1, 'mother', 'limsiacofamily');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `dep_code` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `unofficial` varchar(3) NOT NULL,
  PRIMARY KEY (`dep_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` VALUES('Mathematics', '', 'no');
INSERT INTO `departments` VALUES('Philosophy and Literature', 'Philosophy and Literature courses for higher years', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `employee_id` varchar(9) NOT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `mname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `address_home` varchar(200) DEFAULT NULL,
  `address_mail` varchar(200) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `citizenship` varchar(45) DEFAULT NULL,
  `civil_status` varchar(45) DEFAULT NULL,
  `spouse` varchar(45) DEFAULT NULL,
  `sss_number` varchar(12) DEFAULT NULL,
  `pagibig_number` varchar(45) DEFAULT NULL,
  `philhealth_number` varchar(45) DEFAULT NULL,
  `TIN` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `user_name` varchar(45) NOT NULL,
  PRIMARY KEY (`employee_id`),
  KEY `user_name` (`user_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` VALUES('2011-0001', 'Gerard Igmedio', 'Sorrera', 'Cruz', '1992-03-25', 'm', 'Pasay City', 'VAB, Pasay City', 'Filipino', 'Filipino', 'single', '', '', '', '', '', 'emp_img/gerard.jpg', 'admin');
INSERT INTO `employee` VALUES('2013-0001', 'Kristina Teresa', 'Sorrera', 'Cruz', '1990-12-15', 'f', '9006-70 SDU Recto Extension, Camp Colonel Jesus Villamor Air Base, Pasay City', '9006-70 SDU Recto Extension, Camp Colonel Jesus Villamor Air Base, Pasay City', 'Filipino', 'Filipino', 'single', '', '', '', '', '', 'emp_img/noimage.jpg', 'kristinascruz');

-- --------------------------------------------------------

--
-- Table structure for table `enroll`
--

CREATE TABLE `enroll` (
  `student_id` varchar(9) NOT NULL,
  `school_year` varchar(45) NOT NULL,
  `class_name` varchar(45) NOT NULL,
  `level_id` varchar(45) NOT NULL,
  `estatus` varchar(45) DEFAULT NULL,
  `payment_option` varchar(45) DEFAULT NULL,
  `date_enrolled` date NOT NULL,
  PRIMARY KEY (`student_id`,`school_year`),
  KEY `class_name` (`class_name`),
  KEY `level_id` (`level_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enroll`
--

INSERT INTO `enroll` VALUES('1314-0001', '2013-2014', 'St. John', 'Grade 1', 'enrolled', 'annual', '2013-01-04');

-- --------------------------------------------------------

--
-- Table structure for table `fees`
--

CREATE TABLE `fees` (
  `fee_id` int(11) NOT NULL,
  `level_id` varchar(45) DEFAULT NULL,
  `fee_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`fee_id`),
  KEY `level_id` (`level_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fees`
--

INSERT INTO `fees` VALUES(1, 'Grade 1', 'tuition');
INSERT INTO `fees` VALUES(2, 'Grade 1', 'tuition');
INSERT INTO `fees` VALUES(3, 'Grade 1', 'tuition');
INSERT INTO `fees` VALUES(4, 'Grade 1', 'other');

-- --------------------------------------------------------

--
-- Table structure for table `gradeitems`
--

CREATE TABLE `gradeitems` (
  `level_id` varchar(45) NOT NULL,
  `class_name` varchar(45) NOT NULL,
  `subject_code` varchar(45) NOT NULL,
  `gi_code` varchar(45) NOT NULL,
  `quarter` int(11) NOT NULL,
  `school_year` varchar(9) NOT NULL,
  `category` varchar(45) NOT NULL,
  PRIMARY KEY (`level_id`,`class_name`,`subject_code`,`gi_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gradeitems`
--

INSERT INTO `gradeitems` VALUES('Grade 1', 'St. John', 'Math Grade 1', '1', 1, '2013-2014', 'HW');
INSERT INTO `gradeitems` VALUES('Grade 1', 'St. John', 'Math Grade 1', '2', 1, '2013-2014', 'HW');
INSERT INTO `gradeitems` VALUES('Grade 1', 'St. John', 'WORLITT 5', 'Final Exam', 1, '2013-2014', 'Quarter Exam');
INSERT INTO `gradeitems` VALUES('Grade 1', 'St. John', 'WORLITT 5', 'HW1', 1, '2013-2014', 'Homework');
INSERT INTO `gradeitems` VALUES('Grade 1', 'St. John', 'WORLITT 5', 'HW2', 1, '2013-2014', 'Homework');
INSERT INTO `gradeitems` VALUES('Grade 1', 'St. John', 'WORLITT 5', 'HW3', 1, '2013-2014', 'Homework');
INSERT INTO `gradeitems` VALUES('Grade 1', 'St. John', 'WORLITT 5', 'HW4', 1, '2013-2014', 'Homework');
INSERT INTO `gradeitems` VALUES('Grade 1', 'St. John', 'WORLITT 5', 'HW5', 1, '2013-2014', 'Homework');
INSERT INTO `gradeitems` VALUES('Grade 1', 'St. John', 'WORLITT 5', 'Mid-Exam', 1, '2013-2014', 'Mid Quarter Exam');
INSERT INTO `gradeitems` VALUES('Grade 1', 'St. John', 'WORLITT 5', 'Participation', 1, '2013-2014', 'Participation');
INSERT INTO `gradeitems` VALUES('Grade 1', 'St. John', 'WORLITT 5', 'PR1', 1, '2013-2014', 'Projects');
INSERT INTO `gradeitems` VALUES('Grade 1', 'St. John', 'WORLITT 5', 'PR2', 1, '2013-2014', 'Projects');
INSERT INTO `gradeitems` VALUES('Grade 1', 'St. John', 'WORLITT 5', 'PR3', 1, '2013-2014', 'Projects');
INSERT INTO `gradeitems` VALUES('Grade 1', 'St. John', 'WORLITT 5', 'PR4', 1, '2013-2014', 'Projects');
INSERT INTO `gradeitems` VALUES('Grade 1', 'St. John', 'WORLITT 5', 'Total Attendance', 1, '2013-2014', 'Attendance');

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `subject_code` varchar(45) NOT NULL,
  `student_id` varchar(45) NOT NULL,
  `school_year` varchar(45) NOT NULL,
  `quarter` int(1) NOT NULL,
  `gi_code` varchar(45) NOT NULL,
  `student_grade` decimal(5,2) NOT NULL,
  PRIMARY KEY (`subject_code`,`student_id`,`school_year`,`quarter`,`gi_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` VALUES('Math Grade 1', '1314-0001', '2013-2014', 1, '1', 60.00);
INSERT INTO `grades` VALUES('WORLITT 5', '1314-0001', '2013-2014', 1, 'Final Exam', 100.00);
INSERT INTO `grades` VALUES('WORLITT 5', '1314-0001', '2013-2014', 1, 'HW1', 100.00);
INSERT INTO `grades` VALUES('WORLITT 5', '1314-0001', '2013-2014', 1, 'HW2', 97.00);
INSERT INTO `grades` VALUES('WORLITT 5', '1314-0001', '2013-2014', 1, 'HW3', 100.00);
INSERT INTO `grades` VALUES('WORLITT 5', '1314-0001', '2013-2014', 1, 'HW4', 100.00);
INSERT INTO `grades` VALUES('WORLITT 5', '1314-0001', '2013-2014', 1, 'HW5', 95.00);
INSERT INTO `grades` VALUES('WORLITT 5', '1314-0001', '2013-2014', 1, 'Mid-Exam', 76.00);
INSERT INTO `grades` VALUES('WORLITT 5', '1314-0001', '2013-2014', 1, 'Participation', 75.00);
INSERT INTO `grades` VALUES('WORLITT 5', '1314-0001', '2013-2014', 1, 'PR1', 97.00);
INSERT INTO `grades` VALUES('WORLITT 5', '1314-0001', '2013-2014', 1, 'PR2', 88.00);
INSERT INTO `grades` VALUES('WORLITT 5', '1314-0001', '2013-2014', 1, 'PR3', 97.00);
INSERT INTO `grades` VALUES('WORLITT 5', '1314-0001', '2013-2014', 1, 'PR4', 87.00);
INSERT INTO `grades` VALUES('WORLITT 5', '1314-0001', '2013-2014', 1, 'Total Attendance', 98.00);

-- --------------------------------------------------------

--
-- Table structure for table `group_permissions`
--

CREATE TABLE `group_permissions` (
  `group_name` varchar(45) NOT NULL,
  `is_VIP` int(11) NOT NULL,
  `control_add` int(11) DEFAULT NULL,
  `control_edit` int(11) DEFAULT NULL,
  `control_delete` int(11) DEFAULT NULL,
  `ref_students` int(11) DEFAULT NULL,
  `ref_guardian` int(11) DEFAULT NULL,
  `ref_employee` int(11) DEFAULT NULL,
  `ref_update_img` int(11) NOT NULL,
  `ref_assess_enroll` int(11) NOT NULL,
  `ref_department` int(11) DEFAULT NULL,
  `ref_class` int(11) DEFAULT NULL,
  `ref_class_level` int(11) DEFAULT NULL,
  `ref_fees` int(11) DEFAULT NULL,
  `ref_payment_dues` int(11) DEFAULT NULL,
  `ref_subject_list` int(11) DEFAULT NULL,
  `ref_schedule` int(11) DEFAULT NULL,
  `ref_grade_item` int(11) DEFAULT NULL,
  `ref_grade_item_type` int(11) DEFAULT NULL,
  `ref_grade` int(11) DEFAULT NULL,
  `ref_connection` int(11) DEFAULT NULL,
  `ref_timeslot` int(11) NOT NULL,
  `ref_update_adviser` int(11) NOT NULL,
  `user_access` int(11) DEFAULT NULL,
  `user_groups` int(11) DEFAULT NULL,
  `sys_system_default` int(11) DEFAULT NULL,
  `sys_bulletin` int(11) NOT NULL,
  `sys_backup` int(11) NOT NULL,
  `sys_log` int(11) NOT NULL,
  `ref_all_load` int(11) NOT NULL,
  `delete_logs` int(11) NOT NULL,
  PRIMARY KEY (`group_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_permissions`
--

INSERT INTO `group_permissions` VALUES('Guardian', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `group_permissions` VALUES('No Access / Disabled', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0);
INSERT INTO `group_permissions` VALUES('Registrar', 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, NULL, NULL, 1, 0, 1, 1, 1);
INSERT INTO `group_permissions` VALUES('System Admin', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO `group_permissions` VALUES('Teacher', 0, 0, 1, 0, 0, NULL, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `guardian`
--

CREATE TABLE `guardian` (
  `guardian_id` int(11) NOT NULL,
  `relationship` varchar(45) NOT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `mname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `occupation` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone_landline` varchar(45) DEFAULT NULL,
  `phone_mobile` varchar(45) DEFAULT NULL,
  `emergency_contact` int(11) DEFAULT NULL,
  `living_with` int(11) DEFAULT NULL,
  PRIMARY KEY (`guardian_id`,`relationship`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guardian`
--

INSERT INTO `guardian` VALUES(1, 'father', '', '', '', '', '', '', '', 0, 0);
INSERT INTO `guardian` VALUES(1, 'guardian', '', '', '', '', '', '', '', 0, 0);
INSERT INTO `guardian` VALUES(1, 'mother', 'Geraldine', 'Ricafort', 'Limsiaco', '', '', '', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `optional`
--

CREATE TABLE `optional` (
  `fee_id` int(11) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `cost` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`fee_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `optional`
--


-- --------------------------------------------------------

--
-- Table structure for table `other`
--

CREATE TABLE `other` (
  `fee_id` int(11) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `cost` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`fee_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `other`
--

INSERT INTO `other` VALUES(4, 'Computer Lab', '100');

-- --------------------------------------------------------

--
-- Table structure for table `payment_dues`
--

CREATE TABLE `payment_dues` (
  `payment_type` varchar(45) NOT NULL,
  `installment` int(11) NOT NULL,
  `due_date` date NOT NULL,
  `school_year` varchar(9) NOT NULL,
  PRIMARY KEY (`payment_type`,`installment`,`school_year`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_dues`
--

INSERT INTO `payment_dues` VALUES('quarterly', 2, '0000-00-00', '2013-2014');
INSERT INTO `payment_dues` VALUES('quarterly', 3, '0000-00-00', '2013-2014');
INSERT INTO `payment_dues` VALUES('quarterly', 4, '0000-00-00', '2013-2014');
INSERT INTO `payment_dues` VALUES('semi-annual', 2, '0000-00-00', '2013-2014');

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE `schedule` (
  `schedule_day` varchar(15) NOT NULL,
  `level_id` varchar(45) NOT NULL,
  `class_name` varchar(45) NOT NULL,
  `timeslot_num` int(11) NOT NULL,
  `subject_code` varchar(45) NOT NULL,
  `dep_code` varchar(45) NOT NULL,
  `room_code` varchar(45) NOT NULL,
  PRIMARY KEY (`schedule_day`,`level_id`,`class_name`,`timeslot_num`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` VALUES('friday', 'Grade 1', 'St. John', 1, 'Math Grade 1', 'Mathematics', '301');
INSERT INTO `schedule` VALUES('monday', 'Grade 1', 'St. John', 1, 'Math Grade 1', 'Mathematics', '301');
INSERT INTO `schedule` VALUES('thursday', 'Grade 1', 'St. John', 1, 'WORLITT 5', 'Philosophy and Literature', '666');
INSERT INTO `schedule` VALUES('tuesday', 'Grade 1', 'St. John', 1, 'WORLITT 5', 'Philosophy and Literature', '666');
INSERT INTO `schedule` VALUES('wednesday', 'Grade 1', 'St. John', 1, 'Math Grade 1', 'Mathematics', '301');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `student_id` varchar(9) NOT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `mname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `address_home` text,
  `address_mail` text,
  `citizenship` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` VALUES('1314-0001', 'Keiko Ellaine', 'Ricafort', 'Limsiaco', '2001-08-01', 'f', 'Para�aque City', 'Para�aque City', 'Filipino', 'student_img/noimage.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `student_has_fees`
--

CREATE TABLE `student_has_fees` (
  `student_id` varchar(9) NOT NULL,
  `fee_id` int(11) NOT NULL,
  `adjustments` decimal(8,2) DEFAULT NULL,
  `school_year` varchar(45) NOT NULL,
  PRIMARY KEY (`student_id`,`fee_id`,`school_year`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_has_fees`
--

INSERT INTO `student_has_fees` VALUES('1314-0001', 1, 0.00, '2013-2014');
INSERT INTO `student_has_fees` VALUES('1314-0001', 2, 0.00, '2013-2014');
INSERT INTO `student_has_fees` VALUES('1314-0001', 3, 0.00, '2013-2014');
INSERT INTO `student_has_fees` VALUES('1314-0001', 4, -50.00, '2013-2014');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `subject_code` varchar(45) NOT NULL,
  `dep_code` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `employee_id` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`subject_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` VALUES('Math Grade 1', 'Mathematics', '', '2011-0001');
INSERT INTO `subjects` VALUES('WORLITT 5', 'Philosophy and Literature', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `system_default`
--

CREATE TABLE `system_default` (
  `system_default` int(11) NOT NULL,
  `school_year` varchar(45) NOT NULL,
  `quarter` int(11) NOT NULL,
  PRIMARY KEY (`system_default`,`school_year`,`quarter`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_default`
--

INSERT INTO `system_default` VALUES(0, '2013-2014', 1);

-- --------------------------------------------------------

--
-- Table structure for table `system_logs`
--

CREATE TABLE `system_logs` (
  `log_num` int(11) NOT NULL,
  `module` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `employee_id` varchar(9) NOT NULL,
  `date_processed` date NOT NULL,
  `time_processed` time NOT NULL,
  PRIMARY KEY (`log_num`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_logs`
--

INSERT INTO `system_logs` VALUES(1, 'User Groups', 'Permission "ref_class" for "Teacher" was SET to Yes', '2011-0001', '2013-01-04', '19:50:42');
INSERT INTO `system_logs` VALUES(2, 'Assess & Enroll', 'An Adjustment value of ''-50'' for Fee ID ''4'' was given to Student ID ''1314-0001''', '2011-0001', '2013-01-04', '19:58:45');
INSERT INTO `system_logs` VALUES(3, 'Assess & Enroll', 'An Adjustment value of ''-50'' for Fee ID ''4'' was given to Student ID ''1314-0001''', '2011-0001', '2013-01-04', '19:58:46');
INSERT INTO `system_logs` VALUES(4, 'Assess & Enroll', 'Student ID ''1314-0001'' - Status changed to: ''enrolled''', '2011-0001', '2013-01-04', '20:00:11');

-- --------------------------------------------------------

--
-- Table structure for table `timeslot`
--

CREATE TABLE `timeslot` (
  `timeslot_num` int(11) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  PRIMARY KEY (`timeslot_num`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timeslot`
--

INSERT INTO `timeslot` VALUES(1, '09:30:00', '11:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tuition`
--

CREATE TABLE `tuition` (
  `fee_id` int(11) NOT NULL,
  `payment_option` varchar(45) DEFAULT NULL,
  `upon_enrollment` decimal(8,2) DEFAULT NULL,
  `installment` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`fee_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tuition`
--

INSERT INTO `tuition` VALUES(1, 'annual', 30000.00, 0.00);
INSERT INTO `tuition` VALUES(2, 'semi_annual', 15500.00, 15500.00);
INSERT INTO `tuition` VALUES(3, 'quarterly', 10200.00, 10300.00);

-- --------------------------------------------------------

--
-- Table structure for table `user_access`
--

CREATE TABLE `user_access` (
  `user_name` varchar(45) NOT NULL,
  `user_pass` char(128) NOT NULL,
  `group_name` varchar(45) NOT NULL,
  `is_emp` int(11) NOT NULL,
  PRIMARY KEY (`user_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_access`
--

INSERT INTO `user_access` VALUES('admin', 'p@ssw0rd', 'System Admin', 1);
INSERT INTO `user_access` VALUES('kristinascruz', 'p@ssw0rd', 'Teacher', 1);
INSERT INTO `user_access` VALUES('limsiacofamily', 'p@ssw0rd', 'guardian', 0);
