-- MySQL dump 10.13  Distrib 5.1.41, for Win32 (ia32)
--
-- Host: .    Database: pn4
-- ------------------------------------------------------
-- Server version	5.1.41

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bulletin`
--

DROP TABLE IF EXISTS `bulletin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bulletin` (
  `bulletin_num` int(11) NOT NULL,
  `posted_date` date NOT NULL,
  `posted_time` time NOT NULL,
  `employee_id` varchar(9) NOT NULL,
  `title` varchar(200) NOT NULL,
  `bulletin_message` text NOT NULL,
  PRIMARY KEY (`bulletin_num`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `bulletin_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bulletin`
--

LOCK TABLES `bulletin` WRITE;
/*!40000 ALTER TABLE `bulletin` DISABLE KEYS */;
INSERT INTO `bulletin` VALUES (1,'2013-01-04','19:53:16','2011-0001','Welcome to SMGTS!','SMGTS or School Management System is an application to be deployed on a local server. The core features are the following: <br />\r\n<br /><br />\r\n<ul><br />\r\n<li>Grade System</li><br />\r\n<li>Tuition Management</li><br />\r\n<li>Class and Loading Management</li><br />\r\n</ul>'),(2,'2013-01-04','19:54:06','2011-0001','Welcome to SMGTS!','A project still under development.'),(3,'2013-01-08','20:18:50','2011-0001','Welcome to SMGTS!','This is the Spurgeon School Management System created by Project Novo.<br />\r\n<br />\r\nIt avails the following:<br />\r\n- Assessment and Enrollment of Students<br />\r\n- Scheduling of Classes<br />\r\n- Detailed Progress Reports regarding subjects'),(4,'2013-01-29','06:51:22','2011-0001','Annouyncement','Not final grade yet');
/*!40000 ALTER TABLE `bulletin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bulletin_allow`
--

DROP TABLE IF EXISTS `bulletin_allow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bulletin_allow` (
  `bulletin_num` int(11) NOT NULL,
  `group_name` varchar(45) NOT NULL,
  PRIMARY KEY (`bulletin_num`,`group_name`),
  KEY `group_name` (`group_name`),
  CONSTRAINT `bulletin_allow_ibfk_1` FOREIGN KEY (`bulletin_num`) REFERENCES `bulletin` (`bulletin_num`) ON UPDATE CASCADE,
  CONSTRAINT `bulletin_allow_ibfk_2` FOREIGN KEY (`group_name`) REFERENCES `group_permissions` (`group_name`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bulletin_allow`
--

LOCK TABLES `bulletin_allow` WRITE;
/*!40000 ALTER TABLE `bulletin_allow` DISABLE KEYS */;
INSERT INTO `bulletin_allow` VALUES (3,'Guardian'),(4,'Guardian'),(3,'Registrar'),(3,'System Admin'),(4,'System Admin'),(3,'Teacher'),(4,'Teacher');
/*!40000 ALTER TABLE `bulletin_allow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_percentage`
--

DROP TABLE IF EXISTS `category_percentage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_percentage` (
  `subject_code` varchar(45) NOT NULL,
  `dep_code` varchar(45) NOT NULL,
  `category` varchar(45) NOT NULL,
  `percentage` decimal(5,2) NOT NULL,
  PRIMARY KEY (`subject_code`,`dep_code`,`category`),
  KEY `dep_code` (`dep_code`),
  CONSTRAINT `category_percentage_ibfk_1` FOREIGN KEY (`subject_code`) REFERENCES `subjects` (`subject_code`) ON UPDATE CASCADE,
  CONSTRAINT `category_percentage_ibfk_2` FOREIGN KEY (`dep_code`) REFERENCES `departments` (`dep_code`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_percentage`
--

LOCK TABLES `category_percentage` WRITE;
/*!40000 ALTER TABLE `category_percentage` DISABLE KEYS */;
INSERT INTO `category_percentage` VALUES ('Math Grade 1','Mathematics','Exam','40.00'),('Math Grade 1','Mathematics','HW','10.00'),('Math Grade 1','Mathematics','Project','15.00'),('Math Grade 1','Mathematics','Recitation','25.00'),('Math Grade 1','Mathematics','SW','10.00'),('WORLITT 5','Philosophy and Literature','Attendance','5.00'),('WORLITT 5','Philosophy and Literature','Homework','15.00'),('WORLITT 5','Philosophy and Literature','Mid Quarter Exam','20.00'),('WORLITT 5','Philosophy and Literature','Participation','10.00'),('WORLITT 5','Philosophy and Literature','Projects','30.00'),('WORLITT 5','Philosophy and Literature','Quarter Exam','20.00');
/*!40000 ALTER TABLE `category_percentage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class`
--

DROP TABLE IF EXISTS `class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class` (
  `class_name` varchar(45) NOT NULL,
  `level_id` varchar(45) NOT NULL,
  `class_adviser` varchar(9) DEFAULT NULL,
  `co_adviser` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`class_name`,`level_id`),
  KEY `class_adviser` (`class_adviser`),
  KEY `co_adviser` (`co_adviser`),
  KEY `level_id` (`level_id`),
  CONSTRAINT `class_ibfk_2` FOREIGN KEY (`level_id`) REFERENCES `class_level` (`level_id`) ON UPDATE CASCADE,
  CONSTRAINT `class_ibfk_3` FOREIGN KEY (`class_adviser`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `class_ibfk_4` FOREIGN KEY (`co_adviser`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class`
--

LOCK TABLES `class` WRITE;
/*!40000 ALTER TABLE `class` DISABLE KEYS */;
INSERT INTO `class` VALUES ('St. John','Grade 1',NULL,NULL),('St. Peter','Grade 1',NULL,NULL);
/*!40000 ALTER TABLE `class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_level`
--

DROP TABLE IF EXISTS `class_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_level` (
  `level_id` varchar(45) NOT NULL,
  `category` int(1) DEFAULT NULL,
  `indexnum` int(1) NOT NULL,
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_level`
--

LOCK TABLES `class_level` WRITE;
/*!40000 ALTER TABLE `class_level` DISABLE KEYS */;
INSERT INTO `class_level` VALUES ('Grade 1',1,1),('Grade 2',1,2),('Grade 3',1,3),('Grade 4',1,4),('Grade 5',1,5),('Grade 6',1,6),('Grade 7',1,7);
/*!40000 ALTER TABLE `class_level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `connection`
--

DROP TABLE IF EXISTS `connection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `connection` (
  `student_id` varchar(9) NOT NULL,
  `guardian_id` int(11) NOT NULL,
  `relationship` varchar(45) NOT NULL,
  `user_name` varchar(45) NOT NULL,
  PRIMARY KEY (`student_id`,`guardian_id`,`relationship`),
  KEY `guardian_id` (`guardian_id`),
  KEY `user_name` (`user_name`),
  CONSTRAINT `connection_ibfk_1` FOREIGN KEY (`guardian_id`) REFERENCES `guardian` (`guardian_id`) ON UPDATE CASCADE,
  CONSTRAINT `connection_ibfk_2` FOREIGN KEY (`user_name`) REFERENCES `user_access` (`user_name`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `connection`
--

LOCK TABLES `connection` WRITE;
/*!40000 ALTER TABLE `connection` DISABLE KEYS */;
INSERT INTO `connection` VALUES ('1314-0001',1,'father','limsiacofamily'),('1314-0001',1,'guardian','limsiacofamily'),('1314-0001',1,'mother','limsiacofamily');
/*!40000 ALTER TABLE `connection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments` (
  `dep_code` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `unofficial` varchar(3) NOT NULL,
  PRIMARY KEY (`dep_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments`
--

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` VALUES ('Mathematics','','no'),('Philosophy and Literature','Philosophy and Literature courses for higher years','no');
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `employee_id` varchar(9) NOT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `mname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `address_home` varchar(200) DEFAULT NULL,
  `address_mail` varchar(200) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `citizenship` varchar(45) DEFAULT NULL,
  `civil_status` varchar(45) DEFAULT NULL,
  `spouse` varchar(45) DEFAULT NULL,
  `sss_number` varchar(12) DEFAULT NULL,
  `pagibig_number` varchar(45) DEFAULT NULL,
  `philhealth_number` varchar(45) DEFAULT NULL,
  `TIN` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `user_name` varchar(45) NOT NULL,
  PRIMARY KEY (`employee_id`),
  KEY `user_name` (`user_name`),
  CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`user_name`) REFERENCES `user_access` (`user_name`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES ('2011-0001','Gerard Igmedio','Sorrera','Cruz','1992-03-25','m','Pasay City','VAB, Pasay City','Filipino','Filipino','single','','','','','','emp_img/gerard.jpg','admin'),('2013-0001','Kristina Teresa','Sorrera','Cruz','1990-12-15','f','9006-70 SDU Recto Extension, Camp Colonel Jesus Villamor Air Base, Pasay City','9006-70 SDU Recto Extension, Camp Colonel Jesus Villamor Air Base, Pasay City','Filipino','Filipino','single','','','','','','emp_img/noimage.jpg','kristinascruz');
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enroll`
--

DROP TABLE IF EXISTS `enroll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enroll` (
  `student_id` varchar(9) NOT NULL,
  `school_year` varchar(45) NOT NULL,
  `class_name` varchar(45) NOT NULL,
  `level_id` varchar(45) NOT NULL,
  `estatus` varchar(45) DEFAULT NULL,
  `payment_option` varchar(45) DEFAULT NULL,
  `date_enrolled` date NOT NULL,
  PRIMARY KEY (`student_id`,`school_year`),
  KEY `class_name` (`class_name`),
  KEY `level_id` (`level_id`),
  CONSTRAINT `enroll_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`) ON UPDATE CASCADE,
  CONSTRAINT `enroll_ibfk_2` FOREIGN KEY (`class_name`) REFERENCES `class` (`class_name`) ON UPDATE CASCADE,
  CONSTRAINT `enroll_ibfk_3` FOREIGN KEY (`level_id`) REFERENCES `class` (`level_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enroll`
--

LOCK TABLES `enroll` WRITE;
/*!40000 ALTER TABLE `enroll` DISABLE KEYS */;
INSERT INTO `enroll` VALUES ('1314-0001','2013-2014','St. John','Grade 1','enrolled','quarterly','2013-01-22');
/*!40000 ALTER TABLE `enroll` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fees`
--

DROP TABLE IF EXISTS `fees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fees` (
  `fee_id` int(11) NOT NULL,
  `level_id` varchar(45) DEFAULT NULL,
  `fee_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`fee_id`),
  KEY `level_id` (`level_id`),
  CONSTRAINT `fees_ibfk_1` FOREIGN KEY (`level_id`) REFERENCES `class_level` (`level_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fees`
--

LOCK TABLES `fees` WRITE;
/*!40000 ALTER TABLE `fees` DISABLE KEYS */;
INSERT INTO `fees` VALUES (1,'Grade 1','tuition'),(2,'Grade 1','tuition'),(3,'Grade 1','tuition'),(4,'Grade 1','other'),(5,'Grade 1','other'),(6,'Grade 1','other');
/*!40000 ALTER TABLE `fees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gradeitems`
--

DROP TABLE IF EXISTS `gradeitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gradeitems` (
  `level_id` varchar(45) NOT NULL,
  `class_name` varchar(45) NOT NULL,
  `subject_code` varchar(45) NOT NULL,
  `gi_code` varchar(45) NOT NULL,
  `quarter` int(11) NOT NULL,
  `school_year` varchar(9) NOT NULL,
  `category` varchar(45) NOT NULL,
  PRIMARY KEY (`level_id`,`class_name`,`subject_code`,`gi_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gradeitems`
--

LOCK TABLES `gradeitems` WRITE;
/*!40000 ALTER TABLE `gradeitems` DISABLE KEYS */;
INSERT INTO `gradeitems` VALUES ('Grade 1','St. John','Math Grade 1','1',1,'2013-2014','HW'),('Grade 1','St. John','Math Grade 1','2',1,'2013-2014','HW'),('Grade 1','St. John','WORLITT 5','Final Exam',1,'2013-2014','Quarter Exam'),('Grade 1','St. John','WORLITT 5','HW1',1,'2013-2014','Homework'),('Grade 1','St. John','WORLITT 5','HW2',1,'2013-2014','Homework'),('Grade 1','St. John','WORLITT 5','HW3',1,'2013-2014','Homework'),('Grade 1','St. John','WORLITT 5','HW4',1,'2013-2014','Homework'),('Grade 1','St. John','WORLITT 5','HW5',1,'2013-2014','Homework'),('Grade 1','St. John','WORLITT 5','Mid-Exam',1,'2013-2014','Mid Quarter Exam'),('Grade 1','St. John','WORLITT 5','Participation',1,'2013-2014','Participation'),('Grade 1','St. John','WORLITT 5','PR1',1,'2013-2014','Projects'),('Grade 1','St. John','WORLITT 5','PR2',1,'2013-2014','Projects'),('Grade 1','St. John','WORLITT 5','PR3',1,'2013-2014','Projects'),('Grade 1','St. John','WORLITT 5','PR4',1,'2013-2014','Projects'),('Grade 1','St. John','WORLITT 5','Total Attendance',1,'2013-2014','Attendance');
/*!40000 ALTER TABLE `gradeitems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grades`
--

DROP TABLE IF EXISTS `grades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grades` (
  `subject_code` varchar(45) NOT NULL,
  `student_id` varchar(45) NOT NULL,
  `school_year` varchar(45) NOT NULL,
  `quarter` int(1) NOT NULL,
  `gi_code` varchar(45) NOT NULL,
  `student_grade` decimal(5,2) NOT NULL,
  PRIMARY KEY (`subject_code`,`student_id`,`school_year`,`quarter`,`gi_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grades`
--

LOCK TABLES `grades` WRITE;
/*!40000 ALTER TABLE `grades` DISABLE KEYS */;
INSERT INTO `grades` VALUES ('Math Grade 1','1314-0001','2013-2014',1,'1','60.00'),('WORLITT 5','1314-0001','2013-2014',1,'Final Exam','100.00'),('WORLITT 5','1314-0001','2013-2014',1,'HW1','100.00'),('WORLITT 5','1314-0001','2013-2014',1,'HW2','97.00'),('WORLITT 5','1314-0001','2013-2014',1,'HW3','100.00'),('WORLITT 5','1314-0001','2013-2014',1,'HW4','100.00'),('WORLITT 5','1314-0001','2013-2014',1,'HW5','95.00'),('WORLITT 5','1314-0001','2013-2014',1,'Mid-Exam','76.00'),('WORLITT 5','1314-0001','2013-2014',1,'Participation','75.00'),('WORLITT 5','1314-0001','2013-2014',1,'PR1','97.00'),('WORLITT 5','1314-0001','2013-2014',1,'PR2','88.00'),('WORLITT 5','1314-0001','2013-2014',1,'PR3','97.00'),('WORLITT 5','1314-0001','2013-2014',1,'PR4','87.00'),('WORLITT 5','1314-0001','2013-2014',1,'Total Attendance','98.00');
/*!40000 ALTER TABLE `grades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_permissions`
--

DROP TABLE IF EXISTS `group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_permissions` (
  `group_name` varchar(45) NOT NULL,
  `is_VIP` int(11) NOT NULL,
  `control_add` int(11) DEFAULT NULL,
  `control_edit` int(11) DEFAULT NULL,
  `control_delete` int(11) DEFAULT NULL,
  `ref_students` int(11) DEFAULT NULL,
  `ref_guardian` int(11) DEFAULT NULL,
  `ref_employee` int(11) DEFAULT NULL,
  `ref_update_img` int(11) NOT NULL,
  `ref_assess_enroll` int(11) NOT NULL,
  `ref_department` int(11) DEFAULT NULL,
  `ref_class` int(11) DEFAULT NULL,
  `ref_class_level` int(11) DEFAULT NULL,
  `ref_fees` int(11) DEFAULT NULL,
  `ref_payment_dues` int(11) DEFAULT NULL,
  `ref_subject_list` int(11) DEFAULT NULL,
  `ref_schedule` int(11) DEFAULT NULL,
  `ref_grade_item` int(11) DEFAULT NULL,
  `ref_grade_item_type` int(11) DEFAULT NULL,
  `ref_grade` int(11) DEFAULT NULL,
  `ref_connection` int(11) DEFAULT NULL,
  `ref_timeslot` int(11) NOT NULL,
  `ref_update_adviser` int(11) NOT NULL,
  `user_access` int(11) DEFAULT NULL,
  `user_groups` int(11) DEFAULT NULL,
  `sys_system_default` int(11) DEFAULT NULL,
  `sys_bulletin` int(11) NOT NULL,
  `sys_backup` int(11) NOT NULL,
  `sys_log` int(11) NOT NULL,
  `ref_all_load` int(11) NOT NULL,
  `delete_logs` int(11) NOT NULL,
  PRIMARY KEY (`group_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_permissions`
--

LOCK TABLES `group_permissions` WRITE;
/*!40000 ALTER TABLE `group_permissions` DISABLE KEYS */;
INSERT INTO `group_permissions` VALUES ('Guardian',0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),('No Access / Disabled',0,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0),('Registrar',0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,NULL,NULL,1,0,1,1,1),('System Admin',1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1),('Teacher',0,0,1,0,0,NULL,0,0,0,0,1,0,0,0,0,0,1,1,1,0,0,0,0,0,0,1,0,0,0,0);
/*!40000 ALTER TABLE `group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guardian`
--

DROP TABLE IF EXISTS `guardian`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guardian` (
  `guardian_id` int(11) NOT NULL,
  `relationship` varchar(45) NOT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `mname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `occupation` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone_landline` varchar(45) DEFAULT NULL,
  `phone_mobile` varchar(45) DEFAULT NULL,
  `emergency_contact` int(11) DEFAULT NULL,
  `living_with` int(11) DEFAULT NULL,
  PRIMARY KEY (`guardian_id`,`relationship`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guardian`
--

LOCK TABLES `guardian` WRITE;
/*!40000 ALTER TABLE `guardian` DISABLE KEYS */;
INSERT INTO `guardian` VALUES (1,'father','Edgar','Allan','Limsiaco','Seaman','ealimsiaco@yahoo.com','911-1111','09112223333',1,1),(1,'guardian','','','','','','','',0,0),(1,'mother','Geraldine','Ricafort','Limsiaco','Accountant','grlimsiaco@yahoo.com','911-1111','09445556666',1,1);
/*!40000 ALTER TABLE `guardian` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `optional`
--

DROP TABLE IF EXISTS `optional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `optional` (
  `fee_id` int(11) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `cost` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`fee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `optional`
--

LOCK TABLES `optional` WRITE;
/*!40000 ALTER TABLE `optional` DISABLE KEYS */;
/*!40000 ALTER TABLE `optional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `other`
--

DROP TABLE IF EXISTS `other`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `other` (
  `fee_id` int(11) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `cost` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`fee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `other`
--

LOCK TABLES `other` WRITE;
/*!40000 ALTER TABLE `other` DISABLE KEYS */;
INSERT INTO `other` VALUES (4,'Computer Lab','100'),(5,'Computer Fee','0.0'),(6,'System Fee','1000');
/*!40000 ALTER TABLE `other` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_dues`
--

DROP TABLE IF EXISTS `payment_dues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_dues` (
  `payment_type` varchar(45) NOT NULL,
  `installment` int(11) NOT NULL,
  `due_date` date NOT NULL,
  `school_year` varchar(9) NOT NULL,
  PRIMARY KEY (`payment_type`,`installment`,`school_year`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_dues`
--

LOCK TABLES `payment_dues` WRITE;
/*!40000 ALTER TABLE `payment_dues` DISABLE KEYS */;
INSERT INTO `payment_dues` VALUES ('quarterly',2,'2013-09-28','2013-2014'),('quarterly',3,'2013-12-14','2013-2014'),('quarterly',4,'2014-02-27','2013-2014'),('semi-annual',2,'2013-11-30','2013-2014');
/*!40000 ALTER TABLE `payment_dues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_status`
--

DROP TABLE IF EXISTS `payment_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_status` (
  `student_id` varchar(9) NOT NULL,
  `school_year` varchar(9) NOT NULL,
  `installment` int(1) NOT NULL,
  PRIMARY KEY (`student_id`,`school_year`,`installment`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_status`
--

LOCK TABLES `payment_status` WRITE;
/*!40000 ALTER TABLE `payment_status` DISABLE KEYS */;
INSERT INTO `payment_status` VALUES ('1314-0001','2013-2014',2);
/*!40000 ALTER TABLE `payment_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule` (
  `schedule_day` varchar(15) NOT NULL,
  `level_id` varchar(45) NOT NULL,
  `class_name` varchar(45) NOT NULL,
  `timeslot_num` int(11) NOT NULL,
  `subject_code` varchar(45) NOT NULL,
  `dep_code` varchar(45) NOT NULL,
  `room_code` varchar(45) NOT NULL,
  PRIMARY KEY (`schedule_day`,`level_id`,`class_name`,`timeslot_num`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule`
--

LOCK TABLES `schedule` WRITE;
/*!40000 ALTER TABLE `schedule` DISABLE KEYS */;
INSERT INTO `schedule` VALUES ('friday','Grade 1','St. John',1,'Math Grade 1','Mathematics','301'),('monday','Grade 1','St. John',1,'Math Grade 1','Mathematics','301'),('thursday','Grade 1','St. John',1,'WORLITT 5','Philosophy and Literature','666'),('tuesday','Grade 1','St. John',1,'WORLITT 5','Philosophy and Literature','666'),('wednesday','Grade 1','St. John',1,'Math Grade 1','Mathematics','301');
/*!40000 ALTER TABLE `schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student` (
  `student_id` varchar(9) NOT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `mname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `address_home` text,
  `address_mail` text,
  `citizenship` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES ('1314-0001','Keiko Ellaine','Ricafort','Limsiaco','2001-08-01','f','Parañaque City','Parañaque City','Filipino','student_img/noimage.jpg');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_has_fees`
--

DROP TABLE IF EXISTS `student_has_fees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_has_fees` (
  `student_id` varchar(9) NOT NULL,
  `fee_id` int(11) NOT NULL,
  `adjustments` decimal(8,2) DEFAULT NULL,
  `school_year` varchar(45) NOT NULL,
  PRIMARY KEY (`student_id`,`fee_id`,`school_year`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_has_fees`
--

LOCK TABLES `student_has_fees` WRITE;
/*!40000 ALTER TABLE `student_has_fees` DISABLE KEYS */;
INSERT INTO `student_has_fees` VALUES ('1314-0001',1,'0.00','2013-2014'),('1314-0001',2,'0.00','2013-2014'),('1314-0001',3,'0.00','2013-2014'),('1314-0001',4,'0.00','2013-2014');
/*!40000 ALTER TABLE `student_has_fees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subjects`
--

DROP TABLE IF EXISTS `subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subjects` (
  `subject_code` varchar(45) NOT NULL,
  `dep_code` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `employee_id` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`subject_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subjects`
--

LOCK TABLES `subjects` WRITE;
/*!40000 ALTER TABLE `subjects` DISABLE KEYS */;
INSERT INTO `subjects` VALUES ('Math Grade 1','Mathematics','','2011-0001'),('WORLITT 5','Philosophy and Literature','',NULL);
/*!40000 ALTER TABLE `subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_default`
--

DROP TABLE IF EXISTS `system_default`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_default` (
  `system_default` int(11) NOT NULL,
  `school_year` varchar(45) NOT NULL,
  `quarter` int(11) NOT NULL,
  PRIMARY KEY (`system_default`,`school_year`,`quarter`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_default`
--

LOCK TABLES `system_default` WRITE;
/*!40000 ALTER TABLE `system_default` DISABLE KEYS */;
INSERT INTO `system_default` VALUES (0,'2013-2014',1);
/*!40000 ALTER TABLE `system_default` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_logs`
--

DROP TABLE IF EXISTS `system_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_logs` (
  `log_num` int(11) NOT NULL,
  `module` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `employee_id` varchar(9) NOT NULL,
  `date_processed` date NOT NULL,
  `time_processed` time NOT NULL,
  PRIMARY KEY (`log_num`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_logs`
--

LOCK TABLES `system_logs` WRITE;
/*!40000 ALTER TABLE `system_logs` DISABLE KEYS */;
INSERT INTO `system_logs` VALUES (1,'User Groups','Permission \"ref_class\" for \"Teacher\" was SET to Yes','2011-0001','2013-01-04','19:50:42'),(2,'Assess & Enroll','An Adjustment value of \'-50\' for Fee ID \'4\' was given to Student ID \'1314-0001\'','2011-0001','2013-01-04','19:58:45'),(3,'Assess & Enroll','An Adjustment value of \'-50\' for Fee ID \'4\' was given to Student ID \'1314-0001\'','2011-0001','2013-01-04','19:58:46'),(4,'Assess & Enroll','Student ID \'1314-0001\' - Status changed to: \'enrolled\'','2011-0001','2013-01-04','20:00:11'),(5,'Assess & Enroll','An Adjustment value of \'0\' for Fee ID \'4\' was given to Student ID \'1314-0001\'','2011-0001','2013-01-22','15:16:57'),(6,'Assess & Enroll','Student ID \'1314-0001\' - Status changed to: \'enrolled\'','2011-0001','2013-01-22','15:17:19'),(7,'Assess & Enroll','Student ID \'1314-0001\' - Status changed to: \'enrolled\'','2011-0001','2013-01-22','15:17:35'),(8,'Assess & Enroll','Student ID \'1314-0001\' - Status changed to: \'enrolled\'','2011-0001','2013-01-22','15:18:19'),(9,'Assess & Enroll','Student ID \'1314-0001\' - Status changed to: \'enrolled\'','2011-0001','2013-01-22','15:19:11'),(10,'Assess & Enroll','Student ID \'1314-0001\' - Status changed to: \'enrolled\'','2011-0001','2013-01-22','15:20:06'),(11,'Assess & Enroll','Student ID \'1314-0001\' - Status changed to: \'enrolled\'','2011-0001','2013-01-22','15:21:11'),(12,'Assess & Enroll','Student ID \'1314-0001\' - Status changed to: \'enrolled\'','2011-0001','2013-01-22','15:21:19'),(13,'Assess & Enroll','Student ID \'1314-0001\' - Status changed to: \'enrolled\'','2011-0001','2013-01-22','15:25:12'),(14,'Assess & Enroll','Student ID \'1314-0001\' - Status changed to: \'assessed\'','2011-0001','2013-01-22','15:27:52'),(15,'Assess & Enroll','Student ID \'1314-0001\' - Status changed to: \'enrolled\'','2011-0001','2013-01-22','15:27:58'),(16,'Assess & Enroll','Student ID \'1314-0001\' - Status changed to: \'assessed\'','2011-0001','2013-01-22','15:39:36'),(17,'Assess & Enroll','Student ID \'1314-0001\' - Status changed to: \'enrolled\'','2011-0001','2013-01-22','15:39:55'),(18,'Assess & Enroll','Student ID \'1314-0001\' - Status changed to: \'assessed\'','2011-0001','2013-01-22','15:40:02'),(19,'Assess & Enroll','Student ID \'1314-0001\' - Status changed to: \'enrolled\'','2011-0001','2013-01-22','15:40:35'),(20,'Assess & Enroll','Student ID \'1314-0001\' - Status changed to: \'assessed\'','2011-0001','2013-01-22','15:40:40'),(21,'Assess & Enroll','Student ID \'1314-0001\' - Status changed to: \'assessed\'','2011-0001','2013-01-22','15:42:15'),(22,'Assess & Enroll','Student ID \'1314-0001\' - Status changed to: \'assessed\'','2011-0001','2013-01-22','15:42:42'),(23,'Assess & Enroll','Student ID \'1314-0001\' - Status changed to: \'enrolled\'','2011-0001','2013-01-22','16:03:59');
/*!40000 ALTER TABLE `system_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timeslot`
--

DROP TABLE IF EXISTS `timeslot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timeslot` (
  `timeslot_num` int(11) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  PRIMARY KEY (`timeslot_num`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timeslot`
--

LOCK TABLES `timeslot` WRITE;
/*!40000 ALTER TABLE `timeslot` DISABLE KEYS */;
INSERT INTO `timeslot` VALUES (1,'09:30:00','11:00:00');
/*!40000 ALTER TABLE `timeslot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tuition`
--

DROP TABLE IF EXISTS `tuition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tuition` (
  `fee_id` int(11) NOT NULL,
  `payment_option` varchar(45) DEFAULT NULL,
  `upon_enrollment` decimal(8,2) DEFAULT NULL,
  `installment` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`fee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tuition`
--

LOCK TABLES `tuition` WRITE;
/*!40000 ALTER TABLE `tuition` DISABLE KEYS */;
INSERT INTO `tuition` VALUES (1,'annual','30000.00','0.00'),(2,'semi_annual','15500.00','15500.00'),(3,'quarterly','10200.00','10300.00');
/*!40000 ALTER TABLE `tuition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_access`
--

DROP TABLE IF EXISTS `user_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_access` (
  `user_name` varchar(45) NOT NULL,
  `user_pass` char(128) NOT NULL,
  `group_name` varchar(45) NOT NULL,
  `is_emp` int(11) NOT NULL,
  PRIMARY KEY (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_access`
--

LOCK TABLES `user_access` WRITE;
/*!40000 ALTER TABLE `user_access` DISABLE KEYS */;
INSERT INTO `user_access` VALUES ('admin','p@ssw0rd','System Admin',1),('kristinascruz','p@ssw0rd','Teacher',1),('limsiacofamily','p@ssw0rd','guardian',0);
/*!40000 ALTER TABLE `user_access` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-02-03 14:47:31
