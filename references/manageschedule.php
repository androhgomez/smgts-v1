<?php 
include('../password_protect.php');

if($gp['is_VIP'] == 1 || ($gp['ref_schedule'] == 1)) {
	if(isset($_GET['level']) && isset($_GET['section'])) {
	
		$level = $_GET['level'];
		$code = $_GET['section'];
		
		if(isset($_GET['updatefullrow'])) {
			$tslot = $_GET['tslot'];
			$rowsubj = $_GET['chosensub'];
			$room = $_GET['roomc'];
			$query = "DELETE FROM schedule WHERE class_name='$code' AND level_id='$level' AND timeslot_num=$tslot";
			$dept = $_GET['dept'];
			mysql_query($query);
			
			if(!isset($_GET['clearrow'])) {
			
				$arrdays = array("monday", "tuesday", "wednesday", "thursday", "friday",);
			
				for($i = 0; $i <= 4; $i++) {
				
					$query = "INSERT INTO schedule VALUES('".$arrdays[$i]."','$level','$code',$tslot,'$rowsubj', '$dept', '$room')";
				
					mysql_query($query);
				
				}
			
			}
			
		}
		
		if(isset($_GET['updatecellonly'])) {
			$tslot = $_GET['tslot'];
			$rowsubj = $_GET['chosensub'];
			$room = $_GET['roomc'];
			$setdate = $_GET['daygiven'];
			$dept = $_GET['dept'];
			
			$query = "DELETE FROM schedule WHERE class_name='$code' AND level_id='$level' AND timeslot_num=$tslot AND schedule_day='$setdate'";
			mysql_query($query);
			
			if(!isset($_GET['clearcell'])) {
				$query = "INSERT INTO schedule VALUES('$setdate','$level','$code',$tslot,'$rowsubj', '$dept','$room')";
				mysql_query($query);
			}
			
		}
		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Manage Schedules</title>
<link href="../main_style.css" rel="stylesheet" type="text/css" />


<script type="text/javascript">

function checksubjects(str, ut)
{
if (str=="")
  {
  document.getElementById("subjlist").innerHTML="";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("subjlist").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","checksubjectsselect.php?q="+str+"&updatetype="+ut,true);
xmlhttp.send();

}

function doconfirm() {
var con=confirm("Confirm Update?");
	if (con==true)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function disableelements()
{
	var checkb = document.getElementById("clearcell").checked;
	if(checkb == true)
	{
		document.getElementById("roomc").disabled = true;
		document.getElementById("dept").disabled = true;
		document.getElementById("chosensub").disabled = true;
	}
	else
	{
		document.getElementById("roomc").disabled = false;
		document.getElementById("dept").disabled = false;
		document.getElementById("chosensub").disabled = false;
	}
}

function disableelementsrow()
{
	var checkb = document.getElementById("clearrow").checked;
	if(checkb == true)
	{
		document.getElementById("roomc").disabled = true;
		document.getElementById("dept").disabled = true;
		document.getElementById("chosensub").disabled = true;
	}
	else
	{
		document.getElementById("roomc").disabled = false;
		document.getElementById("dept").disabled = false;
		document.getElementById("chosensub").disabled = false;
	}
}

function printAssessment() {
	var print = "<?php echo $_GET['print']?>";
	
	if(print == "1") {
		window.print();
	}
	
}

</script>
</head>
<body onLoad="printAssessment();">
<?php
if(empty($_GET['print'])) {
?>
<a href="manageschedule.php?section=<?php echo $_GET['section']; ?>&level=<?php echo $_GET['level'] ?>&print=1"><img src="../images/print.png"></a><br /><br />
<?php } ?>
<table id="tbwb" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
	<td colspan="6" style="font-size:12px"><p>Class Schedule for <font color='green'><?php echo "$level - $code"; ?></font></p></td>
  </tr>
  <tr>
    <td width="150" style="background-image:url('../images/tbwbbg.png');">Time</td>
    <td width="150" style="background-image:url('../images/tbwbbg.png');">Monday</td>
    <td width="150" style="background-image:url('../images/tbwbbg.png');">Tuesday</td>
    <td width="150" style="background-image:url('../images/tbwbbg.png');">Wednesday</td>
    <td width="150" style="background-image:url('../images/tbwbbg.png');">Thursday</td>
    <td width="150" style="background-image:url('../images/tbwbbg.png');">Friday</td>
  </tr>

<?php

$query = mysql_query("SELECT timeslot_num, start_time, end_time FROM timeslot ORDER BY start_time");

$rowarr = array();
$indexarr = 0;

while($record = mysql_fetch_array($query)) {
	
	$starttime = date("g:i a", strtotime($record['start_time']));
	$endtime = date("g:i a", strtotime($record['end_time']));
	
	$rowarr[0][$indexarr] = $record[0];
	$rowarr[1][$indexarr] = "$starttime - $endtime";
	$indexarr++;
}

$arrday;

for($i = 0; $i <= $indexarr-1; $i++) {
	$query = mysql_query("SELECT COALESCE(subject_code, '<font color=#FF0000>No Subject Yet</font>') as subj, COALESCE(room_code, '<font color=#FF0000>Pending</font>') as room FROM schedule WHERE schedule_day='monday' AND class_name='$code' AND level_id='$level' AND timeslot_num=".$rowarr[0][$i]);
	if(mysql_num_rows($query) != 0) {
		while($record = mysql_fetch_array($query)) {	
			$arrday['monday'][0][$i] = "<font color='#000000'>".$record[0]."</font>";
			$arrday['monday'][1][$i] = "<font color='black'>".$record[1]."</font>";
		}
	}
	else {
		$arrday['monday'][0][$i] = '<font color=#FF0000>No Subject Yet</font>';
		$arrday['monday'][1][$i] = '<font color="#FF0000">Pending</font>';
	}
	
	$query = mysql_query("SELECT COALESCE(subject_code, '<font color=#FF0000>No Subject Yet</font>') as subj, COALESCE(room_code, '<font color=#FF0000>Pending</font>') as room FROM schedule WHERE schedule_day='tuesday' AND class_name='$code' AND level_id='$level' AND timeslot_num=".$rowarr[0][$i]);
	
	if(mysql_num_rows($query) != 0) {
		while($record = mysql_fetch_array($query)) {	
			$arrday['tuesday'][0][$i] = "<font color='#000000'>".$record[0]."</font>";
			$arrday['tuesday'][1][$i] = "<font color='black'>".$record[1]."</font>";
		}
	}
	else {
		$arrday['tuesday'][0][$i] = '<font color=#FF0000>No Subject Yet</font>';
		$arrday['tuesday'][1][$i] = '<font color=#FF0000>Pending</font>';
	} 
	
		$query = mysql_query("SELECT COALESCE(subject_code, '<font color=#FF0000>No Subject Yet</font>') as subj, COALESCE(room_code, '<font color=#FF0000>Pending</font>') as room FROM schedule WHERE schedule_day='wednesday' AND class_name='$code' AND level_id='$level' AND timeslot_num=".$rowarr[0][$i]);
	
	if(mysql_num_rows($query) != 0) {
		while($record = mysql_fetch_array($query)) {	
			$arrday['wednesday'][0][$i] = "<font color='#000000'>".$record[0]."</font>";
			$arrday['wednesday'][1][$i] = "<font color='black'>".$record[1]."</font>";
		}
	}
	else {
		$arrday['wednesday'][0][$i] = '<font color=#FF0000>No Subject Yet</font>';
		$arrday['wednesday'][1][$i] = '<font color=#FF0000>Pending</font>';
	}

	
	$query = mysql_query("SELECT COALESCE(subject_code, '<font color=#FF0000>No Subject Yet</font>') as subj, COALESCE(room_code, '<font color=#FF0000>Pending</font>') as room FROM schedule WHERE schedule_day='thursday' AND class_name='$code' AND level_id='$level' AND timeslot_num=".$rowarr[0][$i]);
	
	if(mysql_num_rows($query) != 0) {
		while($record = mysql_fetch_array($query)) {	
			$arrday['thursday'][0][$i] = "<font color='#000000'>".$record[0]."</font>";
			$arrday['thursday'][1][$i] = "<font color='black'>".$record[1]."</font>";
		}
	}
	else {
		$arrday['thursday'][0][$i] = '<font color=#FF0000>No Subject Yet</font>';
		$arrday['thursday'][1][$i] = '<font color=#FF0000>Pending</font>';
	}
	

	
	$query = mysql_query("SELECT COALESCE(subject_code, '<font color=#FF0000>No Subject Yet</font>') as subj, COALESCE(room_code, '<font color=#FF0000>Pending</font>') as room FROM schedule WHERE schedule_day='friday' AND class_name='$code' AND level_id='$level' AND timeslot_num=".$rowarr[0][$i]);
	
	if(mysql_num_rows($query) != 0) {
		while($record = mysql_fetch_array($query)) {	
			$arrday['friday'][0][$i] = "<font color='#000000'>".$record[0]."</font>";
			$arrday['friday'][1][$i] = "<font color='black'>".$record[1]."</font>";
		}
	}
	else {
		$arrday['friday'][0][$i] = '<font color=#FF0000>No Subject Yet</font>';
		$arrday['friday'][1][$i] = '<font color=#FF0000>Pending</font>';
	}
	
}




?>

<?php
	for($i = 0; $i <= $indexarr-1; $i++) {
?>

  <tr>
    <td height="39"><a href="manageschedule.php?rowsubmit=yes&level=<?php echo $level; ?>&section=<?php echo $code; ?>&ts=<?php echo $rowarr[0][$i]; ?>&time=<?php echo $rowarr[1][$i]; ?>"><?php echo $rowarr[1][$i]; ?></a>
	</td>
    <td><a href="manageschedule.php?cellsubmit=yes&day=monday&level=<?php echo $level; ?>&section=<?php echo $code; ?>&ts=<?php echo $rowarr[0][$i]; ?>&time=<?php echo $rowarr[1][$i]; ?>">
	<?php 
	echo $arrday['monday'][0][$i] . "<br />";
	echo "Room: ".$arrday['monday'][1][$i] . "<br />";
	?></a>
    </td>
    <td><a href="manageschedule.php?cellsubmit=yes&day=tuesday&level=<?php echo $level; ?>&section=<?php echo $code; ?>&ts=<?php echo $rowarr[0][$i]; ?>&time=<?php echo $rowarr[1][$i]; ?>">
	<?php 
	echo $arrday['tuesday'][0][$i] . "<br />";

	echo "Room: ".$arrday['tuesday'][1][$i] . "<br />";
	?></a>
    </td>
    <td><a href="manageschedule.php?cellsubmit=yes&day=wednesday&level=<?php echo $level; ?>&section=<?php echo $code; ?>&ts=<?php echo $rowarr[0][$i]; ?>&time=<?php echo $rowarr[1][$i]; ?>">
	<?php 
	echo $arrday['wednesday'][0][$i] . "<br />";

	echo "Room: ".$arrday['wednesday'][1][$i] . "<br />";
	?></a>
    </td>
    <td><a href="manageschedule.php?cellsubmit=yes&day=thursday&level=<?php echo $level; ?>&section=<?php echo $code; ?>&ts=<?php echo $rowarr[0][$i]; ?>&time=<?php echo $rowarr[1][$i]; ?>">
	<?php 
	echo $arrday['thursday'][0][$i] . "<br />";

	echo "Room: ".$arrday['thursday'][1][$i] . "<br />";
	?></a>
    </td>
    <td><a href="manageschedule.php?cellsubmit=yes&day=friday&level=<?php echo $level; ?>&section=<?php echo $code; ?>&ts=<?php echo $rowarr[0][$i]; ?>&time=<?php echo $rowarr[1][$i]; ?>">
	<?php 
	echo $arrday['friday'][0][$i] . "<br />";

	echo "Room: ".$arrday['friday'][1][$i] . "<br />";
	?></a>
    </td>
  </tr>
  
<?php
	}
?>
</table><br />


<?php
if(isset($_GET['rowsubmit'])) {
?>
	<form action="manageschedule.php" method="get">
	<table id="tbwb" border="0" cellpadding="0" cellspacing="0" align="center" width="350">
    <tr><td style="background-image:url('../images/tbwbbg.png');" colspan="2">
		Update Row (Time Schedule: <?php echo $_GET['time']; ?>)
	</td>
	</tr>
	<tr>
	<td colspan="2">
		<input type="hidden" name="level" value="<?php echo $_GET['level']; ?>" />
		<input type="hidden" name="section" value="<?php echo $_GET['section']; ?>" />
		<input type="hidden" name="tslot" value="<?php echo $_GET['ts']; ?>" />
		Clear Row? <input id="clearrow" name="clearrow" type="checkbox" value="" onclick="disableelementsrow()" />
	</td>
    </tr>
	<tr>
	<td>Room Code:</td><td><input id="roomc" type="text" name="roomc" value="n/a" style="width:185px;" /></td>
	</tr>
    <tr>
	<td>Department</td><td><select name="dept" id="dept" onchange="checksubjects(this.value, 1)" style="width:200px;">
        <option value="none">Select Department...</option>
        <?php 
			$query = mysql_query("SELECT * FROM departments");
			while($record = mysql_fetch_array($query)) {
		?>
    		<option value="<?php echo $record['dep_code']; ?>"><?php echo $record['dep_code']; ?></option>
        <?php 
			}
		?>
    </select>
    </td>
	</tr>
	<tr>
	<td>Subject</td>
	<td id="subjlist">
			Select a department first
	</td>
	</tr>
	<tr>
	<td colspan="2">
		<input type='submit' name='updatefullrow' value='Update' onclick='return doconfirm();' />
    </td></tr>
	</table>
	</form>
<?php
}
?>

<?php
if(isset($_GET['cellsubmit'])) {
?>
	<form action="manageschedule.php" method="get">
	<table id="tbwb" border="0" cellpadding="0" cellspacing="0" align="center" width="350">
    <tr><td style="background-image:url('../images/tbwbbg.png');" colspan="2">
    Update Cell (Time Schedule: <?php echo $_GET['time']; ?>)
	</td>
	</tr>
	<tr>
	<td colspan="2">
    Clear Cell?<input id="clearcell" name="clearcell" type="checkbox" value="" onclick="disableelements()" /></td>
	</tr>
	<tr>
	<td>Day</td><td><?php echo ucfirst($_GET['day']); ?></td>
	</tr>
	<tr><td>
    <input type="hidden" name="level" value="<?php echo $_GET['level']; ?>" />
    <input type="hidden" name="section" value="<?php echo $_GET['section']; ?>" />
    <input type="hidden" name="tslot" value="<?php echo $_GET['ts']; ?>" />
    <input type="hidden" name="daygiven" value="<?php echo $_GET['day']; ?>" />
    Room Code</td><td><input id="roomc" type="text" name="roomc" value="n/a" style="width:185px" /></td>
	</tr>
	</tr>
	<td>Department</td><td><select name="dept" id="dept" onchange="checksubjects(this.value, 2)" style="width:200px;">
	
        <option value="none">Select Department...</option>
        <?php 
			$query = mysql_query("SELECT * FROM departments");
			while($record = mysql_fetch_array($query)) {
		?>
    		<option value="<?php echo $record['dep_code']; ?>"><?php echo $record['dep_code']; ?></option>
        <?php 
			}
		?>
    </select>
	</td>
	</tr>
	<td>Subject</td>
    <td id="subjlist">
	Select a department first
	</td>
    </tr>
	<td colspan="2">
    <input type='submit' name='updatecellonly' value='Update' onclick='return doconfirm();' />
    </td></tr>
	</table>
	</form>
<?php
}
?>

<?php if(empty($_GET['print'])) { ?>
<br />
<table id="tbwb" border="0" cellpadding="3" cellspacing="0" align="center"><tr>
<td style="text-align:left; border:0px">
Direction for changing schedule: 

<ul>
<li>If you want to change a "specific or one" schedule in a time and day<ul><li>Click that particular cell.</li></ul><br /></li>
<li>If you want to change a "row" of schedule<ul>
  <li>Click the respective time range (from the time column) for that row.</li></ul></li>
</ul>
</td></tr>
</table>
<?php } ?>
</body>
</html>
<?php
	}
	else {
		header("Location: ../home.php?erroraccess=2");
	}
	}
	else {
		header("Location: ../home.php?erroraccess=1");
	}
?>