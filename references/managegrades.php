<?php
include('../password_protect.php'); 

$studentid = $_GET['studentid'];
//Retrieve required data
#$studentyear = $_GET['studentyear'];
#$studentqueue  = $_GET['studentqueue'];
$itemcode = $_GET['itemcode'];
$newgrade = $_GET['grade'];
if($_GET['toexempt'] == 1) {
	$newgrade = -1;
}
$subject = $_GET['subject'];
#$useryear = $_SESSION['useryear'];
#$userqueue = $_SESSION['userqueue'];
#$compareyear = "";
#$comparequeue = "";
$level = $_GET['level'];
$section = $_GET['section'];
$categories = array();
$index = 0;
$quarter = 0;
$systemstartyear = 0;
$systemendyear = 0;
$dept = $_GET['dept'];

$percentages = array();
$itemcount = array();
$totalgrades = array();
$converted = array();
$displaytotal = 0;
$totalnumberofitems = 0;

$getsystemsettings = mysql_query("SELECT * FROM system_default WHERE system_default = 0");

while($settingsrecord = mysql_fetch_array($getsystemsettings)) {
	$schoolyear = $settingsrecord[1];
	$quarter = $settingsrecord[2];
	#$systemstartyear = $settingsrecord[1];
	#$systemendyear = $settingsrecord[2];
	#$quarter = $settingsrecord[3];
	break;
}

if(isset($_GET['submitgrade'])) {
	
	$checkrecord = mysql_query("SELECT * FROM grades WHERE subject_code='$subject' AND student_id = '$studentid' AND school_year='$schoolyear' AND quarter='$quarter' AND gi_code='$itemcode'");
	
	if(mysql_num_rows($checkrecord) > 0) {
	
		$updategrade = "UPDATE grades SET student_grade = $newgrade WHERE subject_code='$subject' AND student_id = '$studentid' AND school_year='$schoolyear' AND quarter='$quarter' AND gi_code='$itemcode'";
	
	}
	else {
		$updategrade = "INSERT INTO grades VALUES('$subject', '$studentid', '$schoolyear', $quarter, '$itemcode', $newgrade)";
	}

	mysql_query($updategrade);
}



if($gp['is_VIP'] == 1 || ($gp['ref_grade'] == 1)) { 

	$getstudents = mysql_query("SELECT s.* FROM student s LEFT JOIN enroll e ON s.student_id = e.student_id WHERE e.class_name = '$section' AND e.level_id = '$level' AND e.estatus = 'enrolled' ORDER BY s.lname");
	
	
	
	$getcategories = mysql_query("SELECT g.category, COUNT( g.gi_code ), c.percentage 'numitems'
									FROM category_percentage c
									LEFT JOIN gradeitems g ON c.category = g.category
									WHERE g.level_id = '$level'
									AND g.class_name = '$section'
									AND g.subject_code =  '$subject'
									AND c.dep_code =  '$dept' 
									AND g.quarter = '$quarter' 
									AND g.school_year = '$schoolyear'
									GROUP BY 1");
	/*
	SELECT c.category, COUNT( g.gi_code ), c.percentage  'numitems'
									FROM category_percentage c
									LEFT JOIN gradeitems g ON c.category = g.category
									WHERE c.subject_code =  '$subject'
									GROUP BY 1
	*/
									
	
	
	while($record = mysql_fetch_array($getcategories)) {
		$categories[0][$index] = $record[0]; // Name of Category
		$categories[1][$index] = $record[1]; // Number of items per category
		$percentages[$index] = $record[2];
		$itemcount[$index] = $record[1];
		$totalnumberofitems += $record[1];
		$index++;
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Manage Grades</title>
<link href="../main_style.css" rel="stylesheet" type="text/css" />
</head>

<body>

<table width="700" border="0" id="tbwb" cellspacing="0" cellpadding="0" align="center">
	<tr>
    	<td colspan="2" style="background-image:url('../images/tbwbbg.png'); height:35px">
        <?php
			switch($quarter) {
				case 1:
					echo "First Quarter Grades";
				break;	
				case 2:
					echo "Second Quarter Grades";
				break;	
				case 3:
					echo "Third Quarter Grades";
				break;	
				case 4:
					echo "Fourth Quarter Grades";
				break;	
			}
		?>
        </td>
        <?php
			if($index < 1) {
			?>
			<td style="background-image:url('../images/tbwbbg.png'); height:35px">
				(No Grade Items Yet Created For <?php
			switch($quarter) {
				case 1:
					echo "First Quarter Grades";
				break;	
				case 2:
					echo "Second Quarter Grades";
				break;	
				case 3:
					echo "Third Quarter Grades";
				break;	
				case 4:
					echo "Fourth Quarter Grades";
				break;	
			}
		?> S.Y. <?php echo $schoolyear ?>)
			</td> 
			<?php
			}
			else {
        	for($index_2 = 0; $index_2 < $index; $index_2++) {
		?>
        	<td colspan="<?php echo $categories[1][$index_2]; ?>" style="background-image:url('../images/tbwbbg.png'); height:35px">
            	<?php echo $categories[0][$index_2]; ?>
            </td>
        <?php
			}
			}
		?>
		<td style="background-color:#ef968b" rowspan="2">Total</td>
    </tr>
    <tr>
    	<td style="background-color:#d2f5d7">
        	Student ID
        </td>
        <td style="background-color:#d2f5d7">
        	Student Name
        </td>
        <?php
			for($index_2 = 0; $index_2 < $index; $index_2++) {
				$getitems = mysql_query("SELECT * FROM gradeitems WHERE category = '".$categories[0][$index_2]."' AND subject_code = '$subject' AND quarter = '$quarter' AND school_year = '$schoolyear' ORDER BY gi_code");
				while($record = mysql_fetch_array($getitems)) {
				?>
                	<td style="background-color:#d2f5d7">
                    	<?php echo $record['gi_code']; ?>
                    </td>
                <?php
				}
			}
			
		?>
    </tr>
    
    	<?php
			while($record = mysql_fetch_array($getstudents)) {
			$converted = array();
			$totalgrades = array();
			$exemptions = array();
			$displaytotal = 0;
			?>
            <tr>
            	<td>
                	<?php echo $record[0]; ?>
                </td>
            	<td>
                	<?php echo $record[3] . ", " . $record[1] . " " . substr($record[2],0,1) . ". "; ?>
                </td>
                
        <?php
		if($totalnumberofitems < 1) {
				?>
				<td></td>
				<?php
			}
			else {
			for($index_2 = 0; $index_2 < $index; $index_2++) {
				
				$getitems = mysql_query("SELECT * FROM gradeitems WHERE category = '".$categories[0][$index_2]."' AND quarter = '$quarter' AND subject_code = '$subject' AND school_year = '$schoolyear' ORDER BY gi_code");
				while($record2 = mysql_fetch_array($getitems)) {
				$temprecgrade = 0;
				?>
                	<td>
                    	<?php
							
							$getgrades = mysql_query("SELECT student_grade FROM grades WHERE subject_code = '$subject' AND student_id = '" . $record[0] . "' AND school_year = '$schoolyear' AND quarter = '$quarter' AND gi_code = '" . $record2['gi_code'] . "'");
							
														
							?>
							<a href="editstudentgrade.php?studentid=<?php echo $record[0]; ?>&itemcode=<?php echo $record2['gi_code']; ?>&subject=<?php echo $subject; ?>&level=<?php echo $level; ?>&section=<?php echo $section; ?>&quarter=<?php echo $quarter; ?>&dept=<?php echo $dept; ?>">
                            <?php
							if(mysql_num_rows($getgrades) < 1) {
							?>
								<font color="#de751a">n/a</font>
							<?php
								$temprecgrade = 0; // Zero for Temporary Grade for Totalling
							}
							else {
							
							while($record2 = mysql_fetch_array($getgrades)) {
								
								if($record2[0] < 0) {
								?>
									<font color="613ec7">Exempted</font>
								<?php
								$exemptions[$index_2] =+ 1;
								}
								else {
								
								?>
									<?php echo $record2[0]; ?>
								<?php
								$temprecgrade = $record2[0]; // Insert record into Temporary Grade for Totalling
								break;	
								}
							}
							
							}
							$totalgrades[$index_2] += $temprecgrade;
							
						?>
							</a>
                    </td>
                <?php
				}
				}
			}
				for($i = 0; $i < count($totalgrades); $i++) {
					
					if($exemptions[$i] > 0 && $itemcount[$i] != 1) {
						$converted[$i] = ($totalgrades[$i] / ($itemcount[$i] - $exemptions[$i])) * ($percentages[$i]/100);
						$displaytotal += $converted[$i];
					}
					else {
						$converted[$i] = ($totalgrades[$i] / $itemcount[$i]) * ($percentages[$i]/100);
						$displaytotal += $converted[$i];
					}
				}
				

		?>       
            <td><?php echo number_format($displaytotal, 2, '.', ''); ?></td>
                
            </tr>
            <?php	
				
			}
		?>
    
</table>

</body>
</html>
<?php
}
else {
	include("error.html");	
}
?>