<?php
include('../password_protect.php');

if($gp['is_VIP'] == 1 || ($gp['ref_grade_item_type'] == 1)) {

$filteremp = mysql_query("SELECT * FROM subject WHERE subject_code = '$getsubj'");

	$subjectcode = $_GET['sc'];
	$department = $_GET['dept'];
	$item = $_GET['ic'];
	$curpercen = 0;
	
	if(isset($subjectcode)) {
		
		if(isset($_GET['del'])) {
			$query = "DELETE FROM category_percentage WHERE subject_code='$subjectcode' AND category='$item'";
			mysql_query($query);	
		}
		
		if(isset($_GET['modcatsubmit'])) {
			$moditem = $_GET['modcatname'];
			$oldmodname = $_GET['oldmodname'];
			$modpercent = str_replace("-", "", $_GET['modpercen']);
			$oldpercent = $_GET['oldpercen'];
			$totalpercen = 0;
			$getsumpercentage = mysql_query("SELECT SUM(percentage) 'percentage' FROM category_percentage WHERE subject_code='$subjectcode' AND dep_code='$department'");
			
			while($record = mysql_fetch_array($getsumpercentage)) {
				$totalpercen = $record[0] - $oldpercent;
				break;
			}
			
			$totalpercen += $modpercent;
			
			if($totalpercen <= 100) {
				$query = "UPDATE category_percentage SET category='$moditem', percentage='$modpercent' WHERE subject_code='$subjectcode' AND category='$oldmodname'";
				
				mysql_query($query);	
			}
			else {
				?>
                <script type="text/javascript">alert("Update Failed: Exceeded maximum total percentage");</script>
                <?php
			}
		}
		
		if(isset($_GET['newcatsubmit'])) {
		
		$category = $_GET['newcatname'];
		$percentage = str_replace("-", "", $_GET['newpercen']);
		$dept = $_GET['dept'];
			if($percentage > $_GET['remainingp']) {
				?>
				<script type="text/javascript">alert("Add Category Failed: Exceeded maximum total percentage");</script>
				<?php
			}
			else {
			$query = "INSERT INTO category_percentage VALUES('$subjectcode', '$dept', '$category', '$percentage')";
			mysql_query($query);
			}
		}
			
		$query = mysql_query("SELECT * FROM category_percentage WHERE subject_code = '$subjectcode' AND dep_code = '$department'");
		
		$getsumpercentage = mysql_query("SELECT SUM(percentage) 'percentage' FROM category_percentage WHERE subject_code='$subjectcode'");
		
	$remainingpercentage = 0;
		
	while($record = mysql_fetch_array($getsumpercentage)) {
			$remainingpercentage = 100 - $record[0];
			break;	
	}
		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Manage Grade Categories</title>
<link href="../main_style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function confirmdel() {
		var con=confirm("Are you sure you want to delete this record?");
		if (con==true)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
</script>

<!--FOR SITE INDICATOR---->

<script src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
//  Developed by Roshan Bhattarai 
//  Visit http://roshanbh.com.np for this script and more.
//  This notice MUST stay intact for legal use
$(document).ready(function()
{
	//scroll the message box to the top offset of browser's scrool bar
	$(window).scroll(function()
	{
  		$('#message_box2').animate({top:$(window).scrollTop()+"px" },{queue: false, duration: 350});  
	});
    //when the close button at right corner of the message box is clicked 
	$('#close_message').click(function()
	{
  		//the messagebox gets scrool down with top property and gets hidden with zero opacity 
		$('#message_box2').animate({ top:"+=15px",opacity:0 }, "slow");
		$('#siteindicator').animate({ top:"-=40px" }, "slow")
	});
});
</script>


<!------>

</head>

<body topmargin="0">
<div id="message_box2"><img id="close_message" style="float:right;cursor:pointer"  src="../images/cross.png" />
<img src="../images/arrow.png"> <font color="green"><b>Manage Grade Categories</b></font>
</div>
<div id="siteindicator" style="position:absolute; top:55px; width:800px">
<?php
	if(isset($_GET['mod'])) {
		
		$querygetpercen = mysql_query("SELECT percentage FROM category_percentage WHERE subject_code='$subjectcode' AND category='$item'");
		
		while($record = mysql_fetch_array($querygetpercen)) {
			$curpercen = $record[0];
			break;
		}
		
?>
            <form id="form1" name="form1" method="get" action="managecategory.php">
			<table width="250" id="tbwb" cellpadding="2" cellspacing="0" align="center">
			<tr>
			<td colspan="2" style="background-image:url('images/tbwbbg.png');">Modify Item</td>
			</tr>
			<tr>
			<td style="text-align:right;">
				  Category Name:</td><td style="text-align:left"><input name="modcatname" type="text" style="width:120px" value="<?php echo $item ?>" /></td>
			</tr>
			<tr>
			<td style="text-align:right;">Percentage:</td><td style="text-align:left"><input name="modpercen" type="text" style="width:50px" value="<?php echo $curpercen; ?>" /></td>
			</tr>
            <tr>
            <td style="text-align:right;">
            	Remaining %: 
            </td>
            <td style="text-align:left">
            <?php
				echo $remainingpercentage;
			?>
            </td>
            </tr>
			<tr>
			<td colspan="2">
            <input name="oldmodname" type="hidden" value="<?php echo $item; ?>" />
			<input name="dept" type="hidden" value="<?php echo $department; ?>" />
            <input name="sc" type="hidden" value="<?php echo $subjectcode; ?>" />
            <input name="oldpercen" type="hidden" value="<?php echo $curpercen; ?>" />
                <input name="modcatsubmit" type="submit" />
			</td>
			</tr>
				
			</table>
		</form>

<?php 
	}
?>
<?php


	if(isset($_GET['new']) && $remainingpercentage != 0) {
		
?>

        <form id="form1" name="form1" method="get" action="managecategory.php">
			<table width="250" id="tbwb" border="0" cellpadding="2" cellspacing="0" align="center">
			<tr>
			<td colspan="2" style="background-image:url('images/tbwbbg.png');">New Item Input</td>
			</tr>
			<tr>
			<td style="text-align:right;">
				  Category Name:</td><td style="text-align:left"><input name="newcatname" type="text" style="width:120px" /></td>
			</tr>
			<tr>
			<td style="text-align:right;">Percentage:</td><td style="text-align:left"><input name="newpercen" type="text" style="width:30px" /></td>
			</tr>
			<tr>
			</tr>
            <tr>
            <td style="text-align:right;">
            	Remaining %: 
            </td>
            <td style="text-align:left">
            <?php
				echo $remainingpercentage;
			?>
            </td>
            </tr>
			<tr>
			<td colspan="2"><input name="dept" type="hidden" value="<?php echo $department; ?>" /><input name="sc2" type="hidden" value="<?php echo $subjectcode; ?>" />
		    <input name="sc" type="hidden" value="<?php echo $subjectcode; ?>" />
			<input name="remainingp" type="hidden" value="<?php echo $remainingpercentage; ?>" />
                <input name="newcatsubmit" type="submit" />
			</td>
			</tr>
				
			</table>
		</form>

<?php 
}
elseif(isset($_GET['new']) && $remainingpercentage == 0) {
	?>
    <script type="text/javascript">alert("Remaining Grade Percentage: <?php echo $remainingpercentage; ?>%\nYou cannot add more categories.");</script>
    <?php	
}
?>
<br />

<table id="tbwb" border="0" cellspacing="3" cellpadding="2" align="center">
  <tr>
    <td width="127" style="background-image:url('images/tbwbbg.png');">Grade Item Category</td>
    <td width="68" style="background-image:url('images/tbwbbg.png');">Percentage</td>
    <td width="289" style="background-image:url('images/tbwbbg.png');">Controls</td>
  </tr>
  <?php 
  	while($record = mysql_fetch_array($query)) {
  ?>
  	<tr>
		<td><?php echo $record[2] ?></td>
		<td><?php echo $record[3] ?>%</td>
		<td>
        <a href="managecategory.php?mod=yes&sc=<?php echo $subjectcode; ?>&ic=<?php echo $record[2] ?>&dept=<?php echo $department; ?>">Modify Category</a> <img src="../images/button.png" /> 
        <a href="managecategory.php?del=yes&sc=<?php echo $subjectcode; ?>&ic=<?php echo $record[2] ?>&dept=<?php echo $department; ?>" onClick="return confirmdel()">Delete Category</a>
        </td>
	</tr>
  	
  <?php
	}
  ?>
	<tr>
  		<td colspan="3"><a href="managecategory.php?new=yes&sc=<?php echo $subjectcode; ?>&dept=<?php echo $department; ?>"><img src="../images/addnewcat.png" /></a></td>
	</tr>
</table><br />

</body>
</div>
</html>
<?php
	}
	else {
		header("Location: ../home.php?erroraccess=2");
	}
	}
	else {
		header("Location: ../home.php?erroraccess=1");
	}
	
?>