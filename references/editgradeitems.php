<?php
include('../password_protect.php'); 

if($gp['is_VIP'] == 1 || ($gp['ref_grade_item'] == 1)) {

$getsubj = $_GET['sc'];
$department = $_GET['dept'];
$querydefaults = mysql_query("SELECT * FROM system_default WHERE system_default = 0");
$getdefaults = mysql_fetch_array($querydefaults);
	
	if(isset($_GET['newgisubmit'])) {
		$query = mysql_query("SELECT gi_code FROM gradeitems WHERE class_name = '".$_GET['cc']."' AND level_id = '".$_GET['cl']."' AND subject_code='".$_GET['sc']."' AND gi_code='".$_GET['newginame']."' AND quarter = '".$getdefaults[2]."' AND school_year = '".$getdefaults[1]."'");
		
		if(mysql_num_rows($query) > 0) {
?>
		<script type="text/javascript">alert("Grade item name already exists");</script>
<?php
		}
		else {
			$query = "INSERT INTO gradeitems VALUES('".$_GET['cl']."', '".$_GET['cc']."', '".$_GET['sc']."', '".str_replace("#", "", $_GET['newginame'])."', '".$getdefaults[2]."', '".$_GET['sy']."', '".$_GET['newgicat']."')";
			mysql_query($query);	
		}
		
	}
	
	if(isset($_GET["delitem"])) {
		$query = "DELETE FROM gradeitems WHERE class_name = '".$_GET['cc']."' AND level_id = '".$_GET['cl']."' AND subject_code='".$_GET['sc']."' AND gi_code='".$_GET['ic']."' AND quarter = '".$getdefaults[2]."'";

		mysql_query($query);
?>
	<script type="text/javascript">alert("Grade item deleted");</script>
<?php
	}
	
	if(isset($_GET['sc'])) {
	
	$chosensubj = $_GET['sc'];
	
	$query = mysql_query(
					"SELECT category, percentage FROM category_percentage
					 WHERE subject_code = '$chosensubj'
					"
			);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Manage Grade Items for <?php echo $_GET['sc']; ?></title>
<link href="../main_style.css" rel="stylesheet" type="text/css" />
<!--FOR SITE INDICATOR---->

<script src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
//  Developed by Roshan Bhattarai 
//  Visit http://roshanbh.com.np for this script and more.
//  This notice MUST stay intact for legal use
$(document).ready(function()
{
	//scroll the message box to the top offset of browser's scrool bar
	$(window).scroll(function()
	{
  		$('#message_box2').animate({top:$(window).scrollTop()+"px" },{queue: false, duration: 350});  
	});
    //when the close button at right corner of the message box is clicked 
	$('#close_message').click(function()
	{
  		//the messagebox gets scrool down with top property and gets hidden with zero opacity 
		$('#message_box2').animate({ top:"+=15px",opacity:0 }, "slow");
		$('#siteindicator').animate({ top:"-=40px" }, "slow")
	});
});
</script>


<!------>
<script type="text/javascript">

function checkinput() {
	var ic = document.getElementById("newgicat").value;
	var inme = document.getElementById("newginame").value;
	
	if(ic == "none" || inme == "" || inme == null) {
		alert("Incomplete details either category or name");
		return false;
	}
	else {
		return true;
	}
}

</script>
</head>
<body topmargin="0">
<div id="message_box2"><img id="close_message" style="float:right;cursor:pointer"  src="../images/cross.png" />
<img src="../images/arrow.png"> <font color="green"><b>Manage Grade Categories</b></font>
</div>
<div id="siteindicator" style="position:absolute; top:55px; width:800px;">
<?php if(isset($_GET['additem'])) { ?>

        <form id="form1" name="form1" method="get" action="editgradeitems.php" onSubmit="return checkinput()">
			<table id="tbwb" width="250" cellpadding="2" cellspacing="0" align="center">
            <tr>
            <td colspan="2" style="background-image:url('../images/tbwbbg.png');">Add Grade Item</td>
            </tr>
			<tr>
			<td style="text-align:right;">
				  Item Category:</td><td>
				  <select name="newgicat" id="newgicat" style="width:120px">
				  <option value="none">Select Category</option>
				  <?php 
					while($record = mysql_fetch_array($query)) {
				  ?>
				  <option value="<?php echo $record['0'] ?>"><?php echo $record['0'] ?></option>
				  <?php
					}
				  ?>
				  </select>
			</td>
			</tr>
			<tr>
			<td style="text-align:right;">
				Grade Item Name:</td><td><input name="newginame" id="newginame" type="text" style="width:120px" /></td>
			</tr>
			<tr>
			<td colspan="2">
				<input name="sc" type="hidden" value="<?php echo $_GET['sc'] ?>" />
                <input name="cc" type="hidden" value="<?php echo $_GET['cc'] ?>" />
                <input name="cl" type="hidden" value="<?php echo $_GET['cl'] ?>" />
				<input name="sy" type="hidden" value="<?php echo $getdefaults[1] ?>" />
                <input name="viewgradeitems" type="hidden" value="yes" />
                <input name="newgisubmit" type="submit" />
			</td>
			</tr>
				
			</table>
		</form>
<br />
<?php } ?>
<table border="0" cellpadding="3" cellspacing="0" align="center">
  <tr>
    <td width="167" valign="top">
    
    <table id="tbwb" border="0" cellspacing="0" cellpadding="2" align="center">
  <tr>
    <td width="203" style="background-image:url('../images/tbwbbg.png');">Classes With <?php echo $_GET['sc']; ?></td>
  </tr>
  <tr>
  	<td>
    
    <?php
	
	$query = mysql_query("SELECT c.* FROM class c
				LEFT JOIN schedule sc on c.class_name = sc.class_name AND c.level_id = sc.level_id
				LEFT JOIN subjects sb on sc.subject_code = sb.subject_code AND sc.dep_code = sb.dep_code
				WHERE sb.subject_code = '$chosensubj'
				GROUP BY 1, 2
	");
	
	while($record = mysql_fetch_array($query)) {
	?>
    <a href="editgradeitems.php?sc=<?php echo $chosensubj; ?>&cc=<?php echo $record['class_name']; ?>&cl=<?php echo $record['level_id']; ?>&viewgradeitems=yes"><?php echo $record['level_id'] . " - " . $record['class_name'] ?></a><br />
    <?php
	}
	?>
    
    </td>
  </tr>
</table>

    
    </td>
    <td width="430" valign="top">
    
    <?php 
		if(isset($_GET['viewgradeitems'])) {
			$classcode = $_GET['cc'];
			$classlevel = $_GET['cl'];
			$sc = $_GET['sc'];
			
			$query = mysql_query(
					"SELECT category, percentage FROM category_percentage
					 WHERE subject_code = '$sc'
					"
			);
	
	?>
    
    <table width="400" border="0" cellpadding="2" cellspacing="0" id="tbwb" align="center">
	<tr>
    	<td colspan="2">
    		<?php echo $classlevel . " - " . $classcode; ?>
    	</td>
    </tr>
	<tr>
		<td width="179" style="background-image:url('../images/tbwbbg.png');">
			Category
		</td>
		<td width="209" style="background-image:url('../images/tbwbbg.png');">Current Available
			Grade Items<br />
			(<?php 
			switch($getdefaults[2]) {
				case 1:
					echo "First";
				break;
				case 1:
					echo "Second";
				break;
				case 1:
					echo "Third";
				break;
				case 1:
					echo "Fourth";
				break;
				
			}
			?> Quarter - S.Y. <?php echo $getdefaults[1]; ?>)</td>
	</tr>
	<?php while($record = mysql_fetch_array($query)) { 
	
		$query2 = mysql_query(
				"SELECT gi.* FROM gradeitems gi
						LEFT JOIN class cl ON gi.class_name = cl.class_name AND gi.level_id = cl.level_id
						LEFT JOIN subjects s ON gi.subject_code = s.subject_code
						WHERE s.subject_code = '$sc' AND gi.category = '". $record[0] ."' AND gi.class_name = '$classcode' AND gi.level_id = '$classlevel' AND gi.quarter = '".$getdefaults[2]."' AND school_year = '".$getdefaults[1]."';
				"
		);
	
	?>
	<tr>
		<td>
			<?php echo $record[0]; ?> [ <?php echo $record[1] ?>% ]
		</td>
		<td>
			<?php 
			while($subrec = mysql_fetch_array($query2)) {
			?>
				<?php echo $subrec['gi_code']; ?> <a href='editgradeitems.php?sc=<?php echo $_GET['sc']?>&cc=<?php echo $_GET['cc']?>&cl=<?php echo $_GET['cl']?>&viewgradeitems=yes&delitem=yes&ic=<?php echo $subrec['gi_code']?>'>[ Delete Item ]</a><br />
         	<?php
            }
			?>
		</td>
	</tr>
	
	<?php } ?>
    
	<tr>
		<td colspan="2">
		<a href="editgradeitems.php?sc=<?php echo $sc; ?>&cc=<?php echo $classcode; ?>&cl=<?php echo $classlevel; ?>&viewgradeitems=yes&additem=yes"><img src="../images/ngi.png" /></a>
		</td>
	</tr>
	
	</table>

    
    <?php
		}
	?>
    </td>

  </tr>
  
</table>
</div>
</body>
</html>
<?php
	}
	else {
		header("Location: ../home.php?erroraccess=2");
	}
	}
	else {
		header("Location: ../home.php?erroraccess=1");
	}
?>