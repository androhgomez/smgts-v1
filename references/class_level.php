<?php

include('../password_protect.php');
if($gp['is_VIP'] == 1 || ($gp['ref_class_level'] == 1)) {

if(isset($_POST['submitlevel']) && $gp['control_add'] == 1) {
	$newmax = 0;
	$querymaxnumber = mysql_query("SELECT MAX(indexnum) indexnum FROM class_level WHERE 1");
	while($getmax = mysql_fetch_array($querymaxnumber)) {
		$newmax = $getmax[0] + 1;
	}
	mysql_query("INSERT INTO class_level VALUES('".$_POST['levelname']."',".$_POST['category'].",$newmax)");
}
elseif(isset($_POST['submitlevel']) && $gp['control_add'] != 1) {
	header("Location: ../home.php?erroraccess=1");
}

if(isset($_GET['delete']) && $gp['control_delete'] == 1) {
	$checkclass = mysql_query("SELECT * FROM class WHERE level_id = '".$_GET['levelid']."'");
		if(mysql_num_rows($checkclass) > 0) {
			$error = 1;
		}
		else {
			mysql_query("DELETE FROM class_level WHERE level_id = '".$_GET['levelid']."'");
		}
}
elseif(isset($_GET['delete']) && $gp['control_delete'] != 1) {
	header("Location: ../home.php?erroraccess=1");
}

?>
<html>


<head>

<title>Class Levels</title>
<link href="../main_style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="../dialog_box.css" />
<script type="text/javascript" src="../dialog_box.js"></script>
<!--FOR SITE INDICATOR---->

<script src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
//  Developed by Roshan Bhattarai 
//  Visit http://roshanbh.com.np for this script and more.
//  This notice MUST stay intact for legal use
$(document).ready(function()
{
	//scroll the message box to the top offset of browser's scrool bar
	$(window).scroll(function()
	{
  		$('#message_box2').animate({top:$(window).scrollTop()+"px" },{queue: false, duration: 350});  
	});
    //when the close button at right corner of the message box is clicked 
	$('#close_message').click(function()
	{
  		//the messagebox gets scrool down with top property and gets hidden with zero opacity 
		$('#message_box2').animate({ top:"+=15px",opacity:0 }, "slow");
		$('#bulletintable').animate({ top:"-=40px" }, "slow")
	});
});
</script>


<!------>
</head>


<body topmargin="0">
<div id="message_box2"><img id="close_message" style="float:right;cursor:pointer"  src="../images/cross.png" />
<img src="../images/arrow.png"> <font color="green"><b>Class Levels</b></font>
</div>
<div id="content">
<div id="bulletintable" style="position:absolute; top:47px">
<table border="0" cellspacing="0" cellpadding="3">

<?php 
	for($i = 0; $i <= 2; $i++) {
		switch($i) {
			case 0:
			?>
			<tr><td width="100px" style="text-align:right;">Category</td><td style="padding-left:30px;"><font color="green">Preschool</font></td><td>Delete</td></tr>
			<?php
			break;
			case 1:
			?>
			<tr><td width="100px" style="text-align:right;">Category</td><td style="padding-left:30px;"><font color="green">Grade School</font></td><td>Delete</td></tr>
			<?php
			break;
			case 2:
			?>
			<tr><td width="100px" style="text-align:right;">Category</td><td style="padding-left:30px;"><font color="green">High School</font></td><td>Delete</td></tr>
			<?php
			break;
		}
		$queryclasslevels = mysql_query("SELECT level_id FROM class_level WHERE category = $i ORDER BY indexnum ASC");
		while($getlevels = mysql_fetch_array($queryclasslevels)) {
			?>
				<tr>
				<td width="100px" style="text-align:right;border-left:none;border-bottom:none;"></td>
				<td style="padding-left:30px; width:100px"><?php echo $getlevels[0]; ?></td>
				<td><center><a href="class_level.php?delete=1&levelid=<?php echo $getlevels[0]; ?>"><img src="../images/delete.png" /></a></center></td>
				</tr>
			<?php
		}
		
	}

?>
	<tr>
	<td colspan="2">
	<center><hr style="width: 95%; margin:2px;" /></center>
	</td>
	</tr>
	<form action="class_level.php" method="POST">
	<tr>
	<td width="100px" style="text-align:right;">Add Level</td><td style="padding-left:30px;"><input type="text" name="levelname" /></td>
	</tr>
	<tr>
	<td width="100px" style="text-align:right;">Category</td>
	<td style="padding-left:30px;">
	<select name="category">
	<option value="0">Preschool</option>
	<option value="1">Grade School</option>
	<option value="2">High School</option>
	</select>
	</td>
	</tr>
	<tr>
	<td width="100px" style="text-align:right;"></td><td style="padding-left:30px;"><input type="submit" name="submitlevel" value="Submit New Level" /></td>
	</tr>
	</form>

</table>
</div>
</div>
</body>

</html>

<?php

if(isset($_GET['delete']) && $error == 1) {
	?>
	<script type="text/javascript">showDialog("Delete Error", "Currently there are sections belonging to that level. <br /><br /><u>You may not delete the level until such classes or sections are deleted</u><br /><br />There are still <font color=red><?php echo mysql_num_rows($checkclass); ?></font> sections(s) in <font color=red>\'<?php echo $_GET['levelid']; ?>\' </font>", "error", 6)</script>
	<?php
}
elseif(isset($_GET['delete']) && $error == 0) {
	?>
	<script type="text/javascript">showDialog("Delete Successful", "Level \'<?php echo $_GET['levelid']; ?>\' was deleted", "success", 3)</script>
	<?php
}

?>

<?php 
} 
else {
	header("Location: ../home.php?erroraccess=1");
}

?>