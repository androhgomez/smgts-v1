<?php
	include("../password_protect.php");
	if($gp['is_VIP'] == 1 || $gp['ref_students'] == 1) {
?>
<html>
<head>
<title>Search Employee</title>
<link href="../main_style.css" rel="stylesheet" type="text/css">

<script src="js/jquery.js" type="text/javascript"></script>

<script type="text/javascript">

function clearfield(e) {
	if(e.keyCode == 8)
	{
		document.getElementById("empid").value = "";
	}
}

function showStudents(str, stype)
{

if(stype == "id") {
	document.getElementById("employeename").value = "";
}
else {
	document.getElementById("empid").value = "";
}

if (str=="")
  {
  document.getElementById("results").innerHTML="";
  } 
else {
	$("#loadingarea").show();
	$('#results').load("retrieveemp.php?q="+str+"&search="+stype, function(){ $("#loadingarea").hide(); });
}
}
</script>

</head>

<body topmargin="0">
<div id="message_box2"><img id="close_message" style="float:right;cursor:pointer"  src="../images/cross.png" />
<img src="../images/arrow.png"> <font color="green"><b>Search and View Employee</b></font>
</div>
<div id="bulletintable" style="position:absolute; top:47px; width:98%;">
<table border="0" cellpadding="3" cellspacing="0" style="width:100%">

	<tr>
	<td width="150px" style="text-align:right;">Search by Employee ID</td><td style="padding-left:30px;"><input type="text" id="empid" name="empid" style="width:100px" value="" onkeyup="showStudents(this.value, 'id'); clearfield(event)" maxlength="9" /></td>
	</tr>
	<tr>
	<td width="150px" style="text-align:right;">Search by Employee Name</td><td style="padding-left:30px;"><input type="text" id="employeename" name="employeename" style="width:200px" value="" onkeyup="showStudents(this.value, 'name')" /></td>
	</tr>
	<tr>
	<td colspan="2">
	<center><hr style="width: 95%; margin:2px;" /></center>
	</td>
	</tr>

	<tr>
	<td colspan="2"><div style="display:none" id='loadingarea'><center><img src='images/loading.gif'></center></div><div id="results"></div></td>
	</td>
	</tr>
	
</table>
</div>
</body></div>
</html>


<?php
}
	else {
		header("Location: ../home.php?erroraccess=1");
	}
?>