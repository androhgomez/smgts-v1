<?php 
	include("../password_protect.php");
	if($gp['is_VIP'] == 1 || $gp['ref_fees'] == 1) {
	$classlevel = $_GET['level'];
	
	if(isset($_GET['submitnewfee']) && $gp['control_add'] == 1) {
		$nextmax = 0;
		$querymax = mysql_query("SELECT MAX(fee_id) maxid FROM fees");
		while($getmax = mysql_fetch_array($querymax)) {
			$nextmax = $getmax[0] + 1;
		}
		mysql_query("INSERT INTO fees VALUES(". ($nextmax) .", '$classlevel', '".$_GET['feetype']."')");
		mysql_query("INSERT INTO ".$_GET['feetype']." VALUES(". ($nextmax) .", '". $_GET['newotherfee'] ."', '0.0')");
	}
	elseif(isset($_GET['changetuition']) && $gp['control_edit'] == 1) {
		$part = $_GET['part'];
		if($part == "Upon Enrollment") {
			$part = "upon_enrollment";
		}
		else {
			$part = "installment";
		}
		mysql_query("UPDATE tuition SET $part = ".$_GET['changeval']." WHERE fee_id = ".$_GET['feeid']);

	}
	elseif(isset($_GET['changemisc']) && $gp['control_edit'] == 1) {
		mysql_query("UPDATE ".$_GET['feetype']." SET cost = ".$_GET['feeamount']." WHERE fee_id = ".$_GET['feeid']);
	}
	elseif(isset($_GET['delete']) && $gp['control_delete'] == 1) {
		mysql_query("DELETE FROM ".$_GET['feetype']." WHERE fee_id = ".$_GET['selectedfee']);
		mysql_query("DELETE fees ".$_GET['feetype']." WHERE fee_id = ".$_GET['selectedfee']);
	}
	elseif((isset($_GET['changetuition']) || isset($_GET['submitnewfee'])) && $gp['control_add'] != 1) {
		header("Location: ../home.php?erroraccess=1");
	}
	
?>
<html>

<head>
<title>Manage Fees</title>
<link href="../main_style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="../dialog_box.css" />
<script type="text/javascript" src="../dialog_box.js"></script>
<!--FOR SITE INDICATOR---->

<script src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
//  Developed by Roshan Bhattarai 
//  Visit http://roshanbh.com.np for this script and more.
//  This notice MUST stay intact for legal use
$(document).ready(function()
{
	//scroll the message box to the top offset of browser's scrool bar
	$(window).scroll(function()
	{
  		$('#message_box2').animate({top:$(window).scrollTop()+"px" },{queue: false, duration: 350});  
	});
    //when the close button at right corner of the message box is clicked 
	$('#close_message').click(function()
	{
  		//the messagebox gets scrool down with top property and gets hidden with zero opacity 
		$('#message_box2').animate({ top:"+=15px",opacity:0 }, "slow");
		$('#bulletintable').animate({ top:"-=42px" }, "slow")
	});
});
</script>


<!------>
</head>

<body topmargin="0">
<div id="message_box2"><img id="close_message" style="float:right;cursor:pointer"  src="../images/cross.png" />
<img src="../images/arrow.png"> <font color="green"><b>Manage Fees</b></font>
</div>
<div id="content">
<div id="bulletintable" style="position:absolute; top:42px">
<form method="get">
<table  border="0" cellspacing="0" cellpadding="7">
<tr>
<td>Select Level</td>
<td>
<select name="level" id="level" style="width:150px">
<option value="0">Select...</option>
<?php

$queryclasslevels = mysql_query("SELECT level_id FROM class_level ORDER BY category, indexnum");
while($getlevels = mysql_fetch_array($queryclasslevels)) {
?>
<option value="<?php echo $getlevels[0]; ?>" <?php if($getlevels[0] == $classlevel) { echo "SELECTED"; } ?>><?php echo $getlevels[0]; ?></option>
<?php
}
?>
</select>
</td>
<td>
<input type="submit" name="changelevel" value="Submit" />
</td>
</tr>
</table>
</form>

<?php
	if(!empty($classlevel) || $classlevel != 0) {
	$querytuition = mysql_query("SELECT t.* FROM tuition t LEFT JOIN fees f ON t.fee_id = f.fee_id WHERE f.level_id = '$classlevel' ORDER BY t.fee_id");
	
	$tuitionarr = array();
	
	if(mysql_num_rows($querytuition) < 1) {
		$nextmax = 0;
		$querymax = mysql_query("SELECT MAX(fee_id) maxid FROM fees");
		while($getmax = mysql_fetch_array($querymax)) {
			$nextmax = $getmax[0] + 1;
		}
		mysql_query("INSERT INTO fees VALUES(". ($nextmax) .", '$classlevel', 'tuition')");
		mysql_query("INSERT INTO fees VALUES(". ($nextmax+1) .", '$classlevel', 'tuition')");
		mysql_query("INSERT INTO fees VALUES(". ($nextmax+2) .", '$classlevel', 'tuition')");
		
		mysql_query("INSERT INTO tuition VALUES(". ($nextmax) .", 'annual', 0, 0)");
		mysql_query("INSERT INTO tuition VALUES(". ($nextmax+1) .", 'semi_annual', 0, 0)");
		mysql_query("INSERT INTO tuition VALUES(". ($nextmax+2) .", 'quarterly', 0, 0)");
		
		$querytuition = mysql_query("SELECT t.* FROM tuition t LEFT JOIN fees f ON t.fee_id = f.fee_id WHERE f.level_id = '$classlevel' ORDER BY t.fee_id");
	}
	$index = 0;
	while($gettuition = mysql_fetch_array($querytuition)) {
		$tuitionarr[$index] = $gettuition;
		$index++;
	}

?>

<p style="margin-left:5px">Note: Click the amounts (Table Cell) to change the values of existing fees.</p>

<table border="0" cellpadding="3" cellspacing="0">
<tr>
<td style="vertical-align:top;">
<table id="tbwb" border="0" cellspacing="0" cellpadding="7">
<tr>
<td colspan="3"><center>Tuition Fee</center></td>
</tr>
<tr>
<td style="background-image:url('../images/tbwbbg.png');">Payment Method</td>
<td style="background-image:url('../images/tbwbbg.png');">Upon Enrollment Amount</td>
<td style="background-image:url('../images/tbwbbg.png');">Installment Amount</td>
</tr>
<tr>
	<td>Annual</td>
	<td><a href="javascript:changetuition('<?php echo $classlevel; ?>',<?php echo $tuitionarr[0][0]; ?>,0,0)"><?php echo number_format($tuitionarr[0][2], 2); ?></td>
	
</tr>
<tr>
	<td>Semi-Annual</td>
	<td><a href="javascript:changetuition('<?php echo $classlevel; ?>',<?php echo $tuitionarr[1][0]; ?>,1,0)"><?php echo number_format($tuitionarr[1][2],2); ?></td>
	<td><a href="javascript:changetuition('<?php echo $classlevel; ?>',<?php echo $tuitionarr[1][0]; ?>,1,1)"><?php echo number_format($tuitionarr[1][3],2); ?></td>
</tr>
<tr>
	<td>Quarterly</td>
	<td><a href="javascript:changetuition('<?php echo $classlevel; ?>',<?php echo $tuitionarr[2][0]; ?>,2,0)"><?php echo number_format($tuitionarr[2][2],2); ?></td>
	<td><a href="javascript:changetuition('<?php echo $classlevel; ?>',<?php echo $tuitionarr[2][0]; ?>,2,1)"><?php echo number_format( $tuitionarr[2][3],2); ?></td>
</tr>

</table>
</td>
<td style="vertical-align:top;">
<table id="tbwb" border="0" cellspacing="0" cellpadding="7">
<tr>
<td colspan="3"><center>Other Fees [ <a href="javascript:addnewfee('<?php echo $classlevel; ?>', 'other');">Add New?</a> ]</center></td>
</tr>
<tr>
<td style="background-image:url('../images/tbwbbg.png'); width:150px">Fee Name</td>
<td style="background-image:url('../images/tbwbbg.png');">Amount</td>
<td style="background-image:url('../images/tbwbbg.png');">Delete?</td>
</tr>
<?php

	$queryother = mysql_query("SELECT o.* FROM other o LEFT JOIN fees f ON o.fee_id = f.fee_id WHERE f.level_id = '$classlevel'");
	while($getother = mysql_fetch_array($queryother)) {
		?>
		<tr>
		<td><?php echo $getother[1] ?></td><td><a href="javascript:changeofee('<?php echo $classlevel; ?>','other',<?php echo $getother[0]; ?>,'<?php echo $getother[1]; ?>');"><?php echo number_format($getother[2],2) ?></a></td><td><a href="javascript:deleteother('<?php echo $classlevel; ?>',<?php echo $getother[0]; ?>,'<?php echo $getother[1]; ?>','other')"><img src="../images/delete.png" /></a></td>
		</tr>
		<?php
	}

?>


</table>
</td>



</tr>
</table>
</div>
</div>
<?php
}
?>
</body>

</html>
<?php
	}
	else {
		header("Location: ../home.php?erroraccess=1");
	}
?>