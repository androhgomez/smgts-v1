<?php
include('../password_protect.php'); 

if($gp['is_VIP'] == 1 || ($gp['ref_timeslot'] == 1)) {
	
	if(isset($_GET['del'])) {
		$ts = $_GET['ts'];
		
		$query = "DELETE FROM timeslot WHERE timeslot_num=$ts";
		mysql_query($query);	
	}
	
	if(isset($_POST['submit'])) {
		$hour1 = $_POST['hour1'];
		$hour2 = $_POST['hour2'];
		$minutes1 = $_POST['minutes1'];
		$minutes2 = $_POST['minutes2'];
		$ampm1 = $_POST['amorpm1'];
		$ampm2 = $_POST['amorpm2'];
		
		if($hour1 <= 12 || $hour2 <= 12 || $minutes1 <= 59 || $minutes2 <= 59) {
		
		if ($ampm1 == "pm") {
			if ($hour1 != 12) {
				$hour1 = $hour1 + 12;
			}
			elseif ($hour1 == 24) {
				$hour1 = "00";
			}
			elseif ($hour1 == 0) {
				$hour1 = "00";	
			}
			
			
		}
		
		if ($ampm2 == "pm") {
			if ($hour2 != 12) {
				$hour2 = $hour2 + 12;
			}
			elseif ($hour2 == 24) {
				$hour2 = "00";
			}
			elseif ($hour2 == 0) {
				$hour2 = "00";	
			}
		}
		
		$time1 = "$hour1:$minutes1:00";
		$time2 = "$hour2:$minutes2:00";
		$maxnum = 0;
			
		$query = mysql_query("SELECT MAX(timeslot_num) AS tsn FROM timeslot");
		
		while($record = mysql_fetch_array($query)) {
			$maxnum = $record['tsn'] + 1;
			break;
		}
		
		$query = "INSERT INTO timeslot VALUES($maxnum, '$time1', '$time2')";
		
		mysql_query($query);
		}
		else {
			?>
        <script type="text/javascript">alert("Invalid Time Input. Follow the 12 hour format.\n\nHours should be less than or equal to 12\nMinutes should be less than or equal to 59");</script>
            <?php
		}
	}

	?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Timeslot</title>
<link href="../main_style.css" rel="stylesheet" type="text/css" />
<!--FOR SITE INDICATOR---->

<script src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
//  Developed by Roshan Bhattarai 
//  Visit http://roshanbh.com.np for this script and more.
//  This notice MUST stay intact for legal use
$(document).ready(function()
{
	//scroll the message box to the top offset of browser's scrool bar
	$(window).scroll(function()
	{
  		$('#message_box2').animate({top:$(window).scrollTop()+"px" },{queue: false, duration: 350});  
	});
    //when the close button at right corner of the message box is clicked 
	$('#close_message').click(function()
	{
  		//the messagebox gets scrool down with top property and gets hidden with zero opacity 
		$('#message_box2').animate({ top:"+=15px",opacity:0 }, "slow");
		$('#siteindicator').animate({ top:"-=40px" }, "slow")
	});
});
</script>


<!------>
<script type="text/javascript">

function validhour(t, name) {
	if (t > 12 || t < 0) {
		document.getElementById(name).value = ""
	}
}

function validminutes(t, name) {
	if (t > 60 || t < 0) {
		document.getElementById(name).value = ""
	}
}

function checkform() {
	var h1 = document.getElementById("hour1").value;
	var h2 = document.getElementById("hour2").value;
	var m1 = document.getElementById("minutes1").value;
	var m2 = document.getElementById("minutes2").value;	
		
	if(h1 == "" || h2 == "" || m1 == "" || m2 == "") {
		alert("Some fields are missing");
		return false;	
	}
	else {
		return true;
	}
}

</script>

</head>
<body style="margin-top:0px">
<div id="message_box2"><img id="close_message" style="float:right;cursor:pointer"  src="../images/cross.png" />
<img src="../images/arrow.png"> <font color="green"><b>Manage Subjects</b></font>
</div>
<div id="siteindicator" style="position:absolute; left:19%; top:60px">
<table width="538" align="center" id="tbwb">
  <tr>
    <td width="200" style="background-image:url('../images/tbwbbg.png'); height:30px;">Current Timeslots</td>
    <td width="212" style="background-image:url('../images/tbwbbg.png'); height:30px;">Add Timeslot <font color="#FF0000">(12 hour format)</font></td>
  </tr>
  <tr>
    <td style="text-align:left; vertical-align:top;">
	<table id="tbwb" border="0" width="100%">
	
	<?php
		$query = mysql_query("SELECT * FROM timeslot ORDER BY 2");
		
		while($record = mysql_fetch_array($query)) {
			
			
			$starttime = date("g:i a", strtotime($record['start_time']));
			$endtime = date("g:i a", strtotime($record['end_time']));

	?>
	
		<tr><td style="border:none;"><?php echo "$starttime - $endtime"; ?></td><td style="border:none;"><a href="managetimeslot.php?del=yes&ts=<?php echo $record['timeslot_num']; ?>"><img src="../images/delete.png"></a></td></tr>
	
	<?php
		}
	?>
	
	</table>
	</td>
    <td><br />
    <form id="form1" name="form1" method="post" action="managetimeslot.php" onSubmit="return checkform()">
      Starting Time<br />
      <input type="text" name="hour1" id="hour1" size="1" maxlength="2" style="text-align:center;" onKeyUp="validhour(this.value, 'hour')" /> : 
      <input type="text" name="minutes1" id="minutes1" size="1" maxlength="2" style="text-align:center" onKeyUp="validminutes(this.value, 'minutes')" /> 
       <select name="amorpm1" id="amorpm">
         <option value="am">AM</option>
         <option value="pm">PM</option>
       </select><br /><br />
      Ending Time<br />
      <input type="text" name="hour2" id="hour2" size="1" maxlength="2" style="text-align:center;" onKeyUp="validhour(this, 'hour2')" /> : 
      <input type="text" name="minutes2" id="minutes2" size="1" maxlength="2" style="text-align:center" onKeyUp="validminutes(this, 'minutes2')" /> 
       <select name="amorpm2" id="amorpm">
         <option value="am">AM</option>
         <option value="pm">PM</option>
       </select><br /><br />
	   <input type="submit" id="submit" name="submit" value="Submit Timeslot" />
    </form><br /><br />
    </td>
  </tr>
</table>
</div>
</body>
</html>
<?php
}
else {
	header("Location: ../home.php?erroraccess=1");
}
?>