<?php
	include("../password_protect.php");
	if($gp['is_VIP'] == 1 || $gp['ref_students'] == 1) {
?>
<html>
<head>
<title>Search Student</title>
<link href="../main_style.css" rel="stylesheet" type="text/css">

<script src="js/jquery.js" type="text/javascript"></script>

<script type="text/javascript">

function clearfield(e) {
	if(e.keyCode == 8)
	{
		document.getElementById("studentid").value = "";
	}
}

function showStudents(str, stype)
{


if (str=="")
{
  document.getElementById("results").innerHTML="";
}
else {
	if(stype == "id") {
	document.getElementById("studentname").value = "";
	}
	else {
	document.getElementById("studentid").value = "";
	}

	$("#loadingarea").show();
	$('#results').load("retrievestudents.php?q="+str+"&search="+stype, function(){ $("#loadingarea").hide(); });
}
}
</script>
<!--FOR SITE INDICATOR---->

<script src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
//  Developed by Roshan Bhattarai 
//  Visit http://roshanbh.com.np for this script and more.
//  This notice MUST stay intact for legal use
$(document).ready(function()
{
	//scroll the message box to the top offset of browser's scrool bar
	$(window).scroll(function()
	{
  		$('#message_box2').animate({top:$(window).scrollTop()+"px" },{queue: false, duration: 350});  
	});
    //when the close button at right corner of the message box is clicked 
	$('#close_message').click(function()
	{
  		//the messagebox gets scrool down with top property and gets hidden with zero opacity 
		$('#message_box2').animate({ top:"+=15px",opacity:0 }, "slow");
		$('#bulletintable').animate({ top:"-=40px" }, "slow")
	});
});
</script>


<!------>

</head>

<body topmargin="0">
<div id="message_box2"><img id="close_message" style="float:right;cursor:pointer"  src="../images/cross.png" />
<img src="../images/arrow.png"> <font color="green"><b>Search and View Student</b></font>
</div>
<div id="bulletintable" style="position:absolute; top:47px; width:98%">
<table border="0" cellpadding="3" cellspacing="0" style="width:100%">

	<tr>
	<td width="150px" style="text-align:right;">Search by Student ID</td><td style="padding-left:30px;"><input type="text" id="studentid" name="studentid" style="width:100px" value="" onkeyup="showStudents(this.value, 'id'); clearfield(event)" maxlength="9" /></td>
	</tr>
	<tr>
	<td width="150px" style="text-align:right;">Search by Student Name</td><td style="padding-left:30px;"><input type="text" id="studentname" name="studentname" style="width:200px" value="" onkeyup="showStudents(this.value, 'name')" /></td>
	</tr>
	<tr>
	<td colspan="2">
	<center><hr style="width: 95%; margin:2px;" /></center>
	</td>
	</tr>

	<tr>
	<td colspan="2"><div style="display:none" id='loadingarea'><center><img src='images/loading.gif'></center></div><div id="results"></div></td>
	</td>
	</tr>
	
	
</table>
</div>
</body>

</html>


<?php
}
	else {
		header("Location: ../home.php?erroraccess=1");
	}
?>