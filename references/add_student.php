<?php
	include("../password_protect.php");
	if($gp['is_VIP'] == 1 || ($gp['ref_students'] == 1 && $gp['control_add'] == 1 && $gp['control_edit'] == 1)) {
		if($_GET['update'] != 1 || empty($_GET['idtobeup'])) {
			$proceedpage = "add_student.php";
			
			if(isset($_POST['guardianexists']) || isset($_POST['newguardian'])) {
				$proceedpage = "../submitdetails.php";
			}
			
			$idsy = substr($_SESSION['sysde']['ssy'], 2, 3) . substr($_SESSION['sysde']['esy'], 2, 3);
			
			$getmaxid = mysql_query("SELECT MAX(substr(student_id,6,4)) unique_num FROM student WHERE student_id LIKE '$idsy-%'");
			
			$uniquenum = 0;
			
			while($getuniquenum = mysql_fetch_array($getmaxid)) {
				$uniquenum = $getuniquenum[0] + 1;
			}
			switch(strlen($uniquenum)) {
			case 1:
				$uniquenum = "000" . $uniquenum;
				break;
			case 2:
				$uniquenum = "00" . $uniquenum;
				break;
			case 3:
				$uniquenum = "0" . $uniquenum;
				break;
			default:
				break;

			}
			
			$datefield = "";
			$mon = 0;
			$day = 0;
			if(!empty($_POST['yearb']) || !empty($_POST['monb']) || !empty($_POST['dayb'])){
				
				switch(strlen($_POST['monb'])) {
					case 0:
						$mon = "00";
					break;
					case 1:
						$mon = "0".$_POST['monb'];
					break;
					case 2:
						$mon = $_POST['monb'];
					break;
				}
				
				switch(strlen($_POST['dayb'])) {
					case 0:
						$day = "00";
					break;
					case 1:
						$day = "0".$_POST['dayb'];
					break;
					case 2:
						$day = $_POST['dayb'];
					break;
				}
				
				$datefield = $_POST['yearb']."-".$mon."-".$day;
			}
			
			$UNIQUE_ID = $idsy . "-" . $uniquenum;
			
			if(!empty($_POST['studentid'])) {
				$UNIQUE_ID = $_POST['studentid'];
			}
			
			$studentinfo = array($UNIQUE_ID, $_POST['fname'], $_POST['mname'], $_POST['lname'], $datefield, $_POST['gender'], $_POST['haddress'], $_POST['maddress'], $_POST['citi']);
			
			if(isset($_POST['guardianexists'])) {
				header("Location: ../confirmpg.php?studentid=".$_POST['studentid']."&fname=".$_POST['fname']."&mname=".$_POST['mname']."&lname=".$_POST['lname']."&dob=".$_POST['yearb']."-".$_POST['monb']."-".$_POST['dayb']."&gender=".$_POST['gender']."&haddress=".$_POST['haddress']."&maddress=".$_POST['maddress']."&citi=".$_POST['citi']."");
			}
		}
		elseif($_GET['update'] == 1 && !empty($_GET['idtobeup'])) {
		
			$proceedpage = "../submitdetails.php";
			
			$querystudent = mysql_query("SELECT * FROM student WHERE student_id = '".$_GET['idtobeup']."'");
			
			$studentinfo = array();
			
			while($getrecords = mysql_fetch_array($querystudent)) {
				for($i = 0; $i<=8; $i++) {
					$studentinfo[$i] = $getrecords[$i];
				}
			}
		}
?>
<html>
<head>
<title><?php echo isset($_GET['update']) ? "Update Student" : "Add Student"; ?></title>
<link href="../main_style.css" rel="stylesheet" type="text/css">

<!--FOR SITE INDICATOR---->

<script src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
//  Developed by Roshan Bhattarai 
//  Visit http://roshanbh.com.np for this script and more.
//  This notice MUST stay intact for legal use
$(document).ready(function()
{
	//scroll the message box to the top offset of browser's scrool bar
	$(window).scroll(function()
	{
  		$('#message_box2').animate({top:$(window).scrollTop()+"px" },{queue: false, duration: 350});  
	});
    //when the close button at right corner of the message box is clicked 
	$('#close_message').click(function()
	{
  		//the messagebox gets scrool down with top property and gets hidden with zero opacity 
		$('#message_box2').animate({ top:"+=15px",opacity:0 }, "slow");
		$('#siteindicator').animate({ top:"-=40px" }, "slow")
	});
});
</script>


<!------>

<script type="text/javascript">

	

	function checkyear() {
		var d = new Date();
		var curyear = d.getFullYear();
		var inputyear = parseInt(document.getElementById("yearb").value);
		if(inputyear > curyear) {
			document.getElementById("yearb").value = "";
			alert("Impossible: Birth Year > Current Year");
		}
	}

	function numdays() {
		var monv = parseInt(document.getElementById("monb").value);
		var d = new Date();
		var curyear = d.getFullYear();
		var checkleap = curyear % 4;
		switch(monv) {
			case 2:
				if (checkleap != 0) {
					changedays(28);
				}
				else {
					changedays(29);
				}
			break;
			case 4:
			case 6:
			case 9:
			case 11:
				changedays(30);
			break;
			default:
				changedays(31);
			break;
		
		}
	}
	
	function changedays(d) {
		document.getElementById("dayb").length = 0;
		var i = 0;
		for(i = 1; i <= d; i++) {
			if (i == 1) {
				document.getElementById("dayb").options[i-1] = new Option(i, i, false, true);
			}
			else {
				document.getElementById("dayb").options[i-1] = new Option(i, i, false, false);
			}
		}
	}
	
function checkuserin(str)
{
	var xmlhttp;
	if (str.length==0)
	{
		document.getElementById("usernamenotice").innerHTML="";
		return;
	}
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
		xmlhttp.onreadystatechange=function()
	{
	if (xmlhttp.readyState==4 && xmlhttp.status==200)
	{
		document.getElementById("usernamenotice").innerHTML=xmlhttp.responseText;
	}
	}
	xmlhttp.open("GET","../checkuser.php?q="+str,true);
	xmlhttp.send();
	checkplength(document.getElementById("newpass").value);
}

function checkplength(l)
{
	if(l.length >= 6) {
		document.getElementById("passwordnotice").innerHTML = "<font color='green'>Valid Password</font><input id='stp' name='stp' type='hidden' value='true'>";
	}
	else {
		document.getElementById("passwordnotice").innerHTML = "<font color='red'>Password must be at least 6 characters</font><input id='stp' name='stp' type='hidden' value='false'>";
	}
	
	var su = document.getElementById('stu').value;
	var sp = document.getElementById('stp').value;
	
	if(su == 'true')
	{
		if(sp == 'true')
		{
			document.getElementById('submit2').disabled = false;
		}
		else
		{
			document.getElementById('submit2').disabled = true;	
		}
	}
	else
	{
		document.getElementById('submit2').disabled = true;	
	}
}

function validateform()
{
	var fname = document.getElementById("fname").value;
	var mname = document.getElementById("mname").value;
	var lname = document.getElementById("lname").value;
	var yearb = document.getElementById("yearb").value;
	var haddress = document.getElementById("haddress").value;

	var citi = document.getElementById("citi").value;
	
	if(fname=="" || mname=="" || lname=="" || yearb=="" || haddress=="" || maddress=="" || citi=="") {
		alert("Some fields are empty");
		return false;
		
	}
	else {
		var a = confirm("Are you sure?")
		if(a == true) {
			return true;
		}
		else {
			return false;
		}
	}
	
	
}


</script>
</head>
<body topmargin="0">
<div id="message_box2"><img id="close_message" style="float:right;cursor:pointer"  src="../images/cross.png" />
<img src="../images/arrow.png"> <font color="green"><b><?php echo isset($_GET['update']) ? "<a href='view_student.php?id=".$_GET['idtobeup']."'>View Student</a> > Update Student" : "Add Student"; ?></b></font>
</div>
<div id="siteindicator" style="position:absolute; top:47px; width:100%">
	<form name="formstudent" id="formstudent" action="<?php echo $proceedpage; ?>" method="POST" onSubmit="return validateform();">
	<table border="0" cellpadding="3" cellspacing="0" width="100%">
	<tr>
	<td width="100px" style="text-align:right;">Student ID</td><td style="padding-left:30px;"><input type="text" name="studentid" <?php if($_GET['update'] == 1) {echo "READONLY";} ?> style="width:100px" value="<?php echo $studentinfo[0]; ?>" /><?php if($_GET['update'] != 1) {?>&nbsp;&nbsp;&nbsp;<font color="red">Note: Student ID will be <u>PERMANENT</u> after adding the student to the DB.<?php } ?></font></td>
	</tr>
	
	<tr>
	<td style="text-align:right;">First Name</td><td style="padding-left:30px;"><input type="text" id="fname" name="fname" value="<?php echo $studentinfo[1]; ?>" /></td>
	</tr>
	
	<tr>
	<td style="text-align:right;">Middle Name</td><td style="padding-left:30px;"><input type="text" id="mname" name="mname" value="<?php echo $studentinfo[2]; ?>" /></td>
	</tr>
	
	<tr>
	<td style="text-align:right;">Last Name</td><td style="padding-left:30px;"><input type="text" id="lname" name="lname" value="<?php echo $studentinfo[3]; ?>" /></td>
	</tr>
	
	<tr>
	<td style="text-align:right;">Date of Birth</td>
	<td style="padding-left:30px;">
	Month: 
        <select name="monb" id="monb" onChange="javascript:numdays()">
        <option value="1">January</option>
        <option value="2">February</option>
        <option value="3">March</option>
        <option value="4">April</option>
        <option value="5">May</option>
        <option value="6">June</option>
        <option value="7">July</option>
        <option value="8">August</option>
        <option value="9">September</option>
        <option value="10">October</option>
        <option value="11">November</option>
        <option value="12">December</option>
      </select>
      Day: 
      <select name="dayb" id="dayb">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
		<option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
        <option value="19">19</option>
        <option value="20">20</option>
        <option value="21">21</option>
        <option value="22">22</option>
        <option value="23">23</option>
        <option value="24">24</option>
        <option value="25">25</option>
        <option value="26">26</option>
        <option value="27">27</option>
        <option value="28">28</option>
        <option value="30">29</option>
        <option value="31">31</option>
      
      </select>
      Year: 
      <input type="text" name="yearb" id="yearb" size="3" maxlength="4" onKeyUp="javascript:checkyear()" value="<?php echo substr($studentinfo[4], 0, 4) ?>" />
	</td>
	</tr>
	
	<tr>
	<td style="text-align:right;">Gender</td>
	<td style="padding-left:30px;">
	<select name="gender" id="gender">
        <option value="m" <?php if($studentinfo[5] == "m") {echo " selected";} ?> >Male</option>
        <option value="f" <?php if($studentinfo[5] == "f") {echo " selected";} ?> >Female</option>
    </select>
	</td>
	</tr>
	
	<tr>
	<td style="text-align:right;">Home Address</td>
	<td style="padding-left:30px;"><textarea name="haddress" id="haddress" cols="45" rows="5"><?php echo $studentinfo[6]; ?></textarea>
	</td>
	</tr>
	
	<tr>
	<td style="text-align:right;">Mailing Address<br />(Optional)</td>
	<td style="padding-left:30px;"><textarea name="maddress" id="maddress" cols="45" rows="5"><?php echo $studentinfo[7]; ?></textarea>
	</td>
	</tr>
	
	<tr>
	<td style="text-align:right;">Citizenship</td>
	<td style="padding-left:30px;">
	<input type="text" name="citi" id="citi" value="<?php echo $studentinfo[8]; ?>" />
	</td>
	</tr>
	<tr>
	<td colspan="2">
	<center><hr style="width: 95%; margin:2px;" /></center>
	</td>
	</tr>
	<?php if(!isset($_POST['guardianexists']) && !isset($_POST['newguardian']) && $_GET['update'] != 1) { ?>
	<tr>
	<td style="text-align:right;">
	Guardian Account
	</td>
	<td style="padding-left:30px;">
	<input type="submit" name="guardianexists" value="Guardian already exist in database" style="width:250px" /> 
	<input type="submit" name="newguardian" value="Assign new guardian"  style="width:250px"  /></td>
	</tr>
	<?php }
		elseif($_GET['update'] == 1) {
		?>
		</td>
		<td></td>
		<td style="padding-left:30px;">
		<input type="submit" name="updatestudent" value="Update Info" /> 
		</tr>
		<?php
		}
		elseif(isset($_POST['newguardian'])) { ?>
		<tr>
		<td colspan="2">
		<table border="0" cellspacing="0" cellpadding="3">
		<tr>
		<td style="text-align:right;" width="100px">Relationship</td>
		<td style="padding-left:30px;">Father</td>
		<td style="padding-left:30px;">Mother</td>
		<td style="padding-left:30px;">Guardian</td>
		</tr>
		<tr>
		<td style="text-align:right;">First Name</td>
		<td style="padding-left:30px;"><input type="text" name="fname0" /></td>
		<td style="padding-left:30px;"><input type="text" name="fname1" /></td>
		<td style="padding-left:30px;"><input type="text" name="fname2" /></td>
		</tr>
		
		<tr>
		<td style="text-align:right;">Middle Name</td>
		<td style="padding-left:30px;"><input type="text" name="mname0" /></td>
		<td style="padding-left:30px;"><input type="text" name="mname1" /></td>
		<td style="padding-left:30px;"><input type="text" name="mname2" /></td>
		</tr>
		
		<tr>
		<td style="text-align:right;">Last Name</td>
		<td style="padding-left:30px;"><input type="text" name="lname0" /></td>
		<td style="padding-left:30px;"><input type="text" name="lname1" /></td>
		<td style="padding-left:30px;"><input type="text" name="lname2" /></td>
		</tr>
		
		<tr>
		<td style="text-align:right;">Occupation</td>
		<td style="padding-left:30px;"><input type="text" name="occupation0" /></td>
		<td style="padding-left:30px;"><input type="text" name="occupation1" /></td>
		<td style="padding-left:30px;"><input type="text" name="occupation2" /></td>
		</tr>
		
		<tr>
		<td style="text-align:right;">E-mail</td>
		<td style="padding-left:30px;"><input type="text" name="email0" /></td>
		<td style="padding-left:30px;"><input type="text" name="email1" /></td>
		<td style="padding-left:30px;"><input type="text" name="email2" /></td>
		</tr>
		
		<tr>
		<td style="text-align:right;">Landline</td>
		<td style="padding-left:30px;"><input type="text" name="landline0" /></td>
		<td style="padding-left:30px;"><input type="text" name="landline1" /></td>
		<td style="padding-left:30px;"><input type="text" name="landline2" /></td>
		</tr>
		
		<tr>
		<td style="text-align:right;">Mobile</td>
		<td style="padding-left:30px;"><input type="text" name="mobile0" /></td>
		<td style="padding-left:30px;"><input type="text" name="mobile1" /></td>
		<td style="padding-left:30px;"><input type="text" name="mobile2" /></td>
		</tr>
		
		<tr>
		<td style="text-align:right;">Can Fetch Student?</td>
		<td style="padding-left:30px;"><input type="checkbox" name="cfs0" value="1" /></td>
		<td style="padding-left:30px;"><input type="checkbox" name="cfs1" value="1" /></td>
		<td style="padding-left:30px;"><input type="checkbox" name="cfs2" value="1" /></td>
		</tr>
		
		<tr>
		<td style="text-align:right;">Student living with?</td>
		<td style="padding-left:30px;"><input type="checkbox" name="livingwith0" value="1" /></td>
		<td style="padding-left:30px;"><input type="checkbox" name="livingwith1" value="1" /></td>
		<td style="padding-left:30px;"><input type="checkbox" name="livingwith2" value="1" /></td>
		</tr>
		</table>
		</td>
		</tr>
		
		<tr>
		<td colspan="2">
		<center><hr style="width: 95%; margin:2px;" /></center>
		</td>
		</tr>
		<tr>
		<td colspan="2">
		<table border="0" cellpadding="3" cellspacing="0">
		<tr>
		<td style="text-align:right;" width="100px">New Username</td><td style="padding-left:30px;"><input type="text" id="newuser" name="newuser" onKeyUp="checkuserin(this.value)" /></td>
		<td style="padding-left:30px; text-align:justify;"><div id="usernamenotice"></div></td>
		</tr>
		<tr>
		<td style="text-align:right;">Password</td><td style="padding-left:30px;"><input type="password" id="newpass" name="newpass" onKeyUp="checkplength(this.value)" /></td>
		<td style="padding-left:30px; text-align:justify;"><div id="passwordnotice"></div></td>
		</tr>
		</table>
		</td>
		</tr>
		<tr>
		<td colspan="2">
		<center><hr style="width: 95%; margin:2px;" /></center>
		</td>
		</tr>
		<tr>
		<td style="text-align:right;">Submit all?</td>
		<td style="padding-left:30px;"><input type="submit" id="submit2" name="submit2" value="Yes" disabled /></td>
		</tr>
		<?php }
		?>
	</table>
	</form>
	</div>
	<script type="text/javascript">
		document.getElementById("monb").options[<?php echo substr($studentinfo[4], 5, 6) - 1; ?>].selected = true;
		document.getElementById("dayb").options[<?php echo substr($studentinfo[4], 8, 9) - 1; ?>].selected = true;
	</script>
	
</body>
<html>
<?php
	}
	else {
		header("Location: ../home.php?erroraccess=1");
	}
?>