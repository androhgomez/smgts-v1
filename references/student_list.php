<?php
include('../password_protect.php');
$getsystemsettings = mysql_query("SELECT * FROM system_default WHERE system_default = 0");
	

while($settingsrecord = mysql_fetch_array($getsystemsettings)) {
	$schoolyear = $settingsrecord[1];
	$quarter = $settingsrecord[2];
	#$systemstartyear = $settingsrecord[1];
	#$systemendyear = $settingsrecord[2];
	#$quarter = $settingsrecord[3];
	break;
}

$studentQuery = mysql_query("SELECT student.*, enroll.level_id, enroll.class_name FROM student, enroll WHERE student.student_id = enroll.student_id AND enroll.estatus = 'enrolled' AND enroll.school_year = '$schoolyear'");


	?>
	<html>
	<head>
		<title>Student List</title>
		<script type="text/javascript" src="../js/jquery.js"></script>
		<script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
		<link href="../css/jquery.dataTables.css" rel="stylesheet" type="text/css">
	</head>
	<body topmargin="0">
		<div id="loadtable" style="position:absolute; top:45px">
		<table class="displayTable" id="studentTable">
		<thead>
			<tr>
				<th>
					ID
				</th>
				<th>
					Name
				</th>
				<th>
					Date of Birth
				</th>
				<th>
					Gender
				</th>
				<th>
					Home Address
				</th>
				<th>
					Mailing Address
				</th>
				<th>
					Level
				</th>
				<th>
					Class
				</th>
			</tr>
		</thead>
		<tbody>
		<?php
			while($studentRow = mysql_fetch_array($studentQuery)){
		?>
			<tr>
				<td>
					<?php echo $studentRow['student_id']; ?>
				</td>
				<td>
					<?php echo $studentRow['lname'].", ".$studentRow['fname']." ".$studentRow['mname']; ?>
				</td>
				<td>
					<?php echo $studentRow['birthdate']; ?>
				</td>
				<td>
					<?php echo $studentRow['gender']; ?>
				</td>
				<td>
					<?php echo $studentRow['address_home']; ?>
				</td>
				<td>
					<?php echo $studentRow['address_mail']; ?>
				</td>
				<td>
					<?php echo $studentRow['level_id']; ?>
				</td>
				<td>
					<?php echo $studentRow['class_name']; ?>
				</td>
			</tr>
		<?php
			}
		?>
		</tbody>
		</table>
		<script type="text/javascript">
			$(document).ready(function() {
    			var studentTable = $('#studentTable').dataTable();
    		});

			function fnShowHide(iCol,visible){
				var studentTable = $('#studentTable').dataTable();
     			var bVis = studentTable.fnSettings().aoColumns[iCol].bVisible;
    			studentTable.fnSetColumnVis(iCol,visible);
			}
		</script>
		<div id="colOptions" style="padding-top: 25px">
			<label for="checkId">ID</label>
			<input type="checkbox" checked="true" id="checkId" name="checkId" onClick="fnShowHide(0, $('#checkId').is(':checked'))"/>
			<label for="checkName">Name</label>
			<input type="checkbox" checked="true" id="checkName" name="checkName" onClick="fnShowHide(1, $('#checkName').is(':checked'))"/>
			<label for="checkDoB">DoB</label>
			<input type="checkbox" checked="true" id="checkDoB" name="checkDoB" onClick="fnShowHide(2, $('#checkDoB').is(':checked'))"/>
			<label for="checkGender">Gender</label>
			<input type="checkbox" checked="true" id="checkGender" name="checkGender" onClick="fnShowHide(3, $('#checkGender').is(':checked'))"/>
			<label for="checkHome">Home Add</label>
			<input type="checkbox" checked="true" id="checkHome" name="checkHome" onClick="fnShowHide(4, $('#checkHome').is(':checked'))"/>
			<label for="checkMailing">Mailing Add</label>
			<input type="checkbox" checked="true" id="checkMailing" name="checkMailing" onClick="fnShowHide(5, $('#checkMailing').is(':checked'))"/>
			<label for="checkLevel">Level</label>
			<input type="checkbox" checked="true" id="checkLevel" name="checkLevel" onClick="fnShowHide(6, $('#checkLevel').is(':checked'))"/>
			<label for="checkClass">Class</label>
			<input type="checkbox" checked="true" id="checkClass" name="checkClass" onClick="fnShowHide(7, $('#checkClass').is(':checked'))"/><br/>
			<input type="button" onClick="window.print();" value="Print"/>
		</div>
		</div>
	</body>
	</html>
	<?php

?>