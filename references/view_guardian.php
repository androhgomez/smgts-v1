<?php

	include("../password_protect.php");
	$studentid = $_GET['eid'];
	$CHECKSTUDENT = mysql_query("SELECT * FROM student WHERE student_id = '$studentid'");
	
	$checkid = array();
	$index = -1;
	$queryconnection = mysql_query("SELECT guardian_id FROM connection WHERE student_id = '$studentid'");
	while($getconnection = mysql_fetch_array($queryconnection)) {
		$index++;
		$checkid[$index] = $getconnection[0];
	}
	
	if($gp['is_VIP'] || ($gp['ref_guardian'] == 1 && $studentid) || $_SESSION['is_emp'] != 1) {
	
	if($_SESSION['is_emp'] != 1) {
		$queryguardian = mysql_query("SELECT g.* FROM guardian g LEFT JOIN connection c ON g.guardian_id = c.guardian_id WHERE c.guardian_id = '".$_SESSION['userid']."' GROUP BY g.relationship ORDER BY g.relationship");
	}
	else {
		$queryguardian = mysql_query("SELECT g.* FROM guardian g LEFT JOIN connection c ON g.guardian_id = c.guardian_id WHERE c.student_id = '$studentid' GROUP BY g.relationship ORDER BY g.relationship");
	}
	$gi = array();
	
	$index = 0;
	
	while($getguardian = mysql_fetch_array($queryguardian)) {
		for($i = 1; $i <= 10; $i++) {
			$gi[$index][$i-1] = $getguardian[$i];
		}
		$index++;
	}
	
?>

<html>

<head>
<title>View Guardian</title>
<link href="../main_style.css" rel="stylesheet" type="text/css">
<!--FOR SITE INDICATOR---->

<script src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
//  Developed by Roshan Bhattarai 
//  Visit http://roshanbh.com.np for this script and more.
//  This notice MUST stay intact for legal use
$(document).ready(function()
{
	//scroll the message box to the top offset of browser's scrool bar
	$(window).scroll(function()
	{
  		$('#message_box2').animate({top:$(window).scrollTop()+"px" },{queue: false, duration: 350});  
	});
    //when the close button at right corner of the message box is clicked 
	$('#close_message').click(function()
	{
  		//the messagebox gets scrool down with top property and gets hidden with zero opacity 
		$('#message_box2').animate({ top:"+=15px",opacity:0 }, "slow");
		$('#siteindicator').animate({ top:"-=40px" }, "slow")
	});
});
</script>


<!------>
</head>

<body topmargin="0">
<div id="message_box2"><img id="close_message" style="float:right;cursor:pointer"  src="../images/cross.png" />
<img src="../images/arrow.png"> <font color="green"><b>
<?php 
	if($_SESSION['is_emp'] != 1) {
		echo "Parent & Guardian Details";
	}
	else {
		echo "<a href='view_student.php?id=".$_GET['eid']."'>View Student</a> > View Guardian Details";
	}
	
?>
</b></font>
</div>
<div id="siteindicator" style="position:absolute; top:55px; width:98%">
<table id="tbwb" width="100%" align="center">
<tr>
<td colspan="4" style="background-image:url('../images/tbwbbg.png');" >Parent and Guardian Information</td>
</tr>
<tr>
<td width="130" style="background-image:url('../images/tbwbbg.png');" >Relationship</td><td width="130">Father</td><td width="130">Guardian</td><td width="130">Mother</td>
</tr>
<tr>
<td width="130" style="background-image:url('../images/tbwbbg.png');" >Name</td>
<td width="130"><?php echo $gi[0][1] . " " . $gi[0][2] . " " . $gi[0][3]; ?></td>
<td width="130"><?php if(!empty($gi[1][1]) || !empty($gi[1][2]) || !empty($gi[1][3])) {echo $gi[1][1] . " " . $gi[1][2] . " " . $gi[1][3]; } ?></td>
<td width="130"><?php echo $gi[2][1] . " " . $gi[2][2] . " " . $gi[2][3]; ?></td>
</tr>
<tr>
<td style="background-image:url('../images/tbwbbg.png');" >Occupation</td>
<td><?php echo $gi[0][4]; ?></td>
<td><?php echo $gi[1][4]; ?></td>
<td><?php echo $gi[2][4]; ?></td>
</tr>
<tr>
<td style="background-image:url('../images/tbwbbg.png');" >Email</td>
<td><a href="mailto:<?php echo $gi[0][5]; ?>"><?php echo $gi[0][5]; ?></a></td>
<td><a href="mailto:<?php echo $gi[1][5]; ?>"><?php echo $gi[1][5]; ?></a></td>
<td><a href="mailto:<?php echo $gi[2][5]; ?>"><?php echo $gi[2][5]; ?></a></td>
</tr>
<tr>
<td style="background-image:url('../images/tbwbbg.png');" >Landline Number</td>
<td><?php echo $gi[0][6]; ?></td>
<td><?php echo $gi[1][6]; ?></td>
<td><?php echo $gi[2][6]; ?></td>
</tr>
<tr>
<td style="background-image:url('../images/tbwbbg.png');" >Mobile Number</td>
<td><?php echo $gi[0][7]; ?></td>
<td><?php echo $gi[1][7]; ?></td>
<td><?php echo $gi[2][7]; ?></td>
</tr>
<tr>
<td style="background-image:url('../images/tbwbbg.png');" >Can Fetch Student?</td>
<td><?php if($gi[0][8] == 1) { echo "Yes"; } else { echo "No"; } ?></td>
<td><?php if($gi[1][8] == 1) { echo "Yes"; } else { echo "No"; } ?></td>
<td><?php if($gi[2][8] == 1) { echo "Yes"; } else { echo "No"; } ?></td>
</tr>
<tr>
<td style="background-image:url('../images/tbwbbg.png');" >Is Living With?</td>
<td><?php if($gi[0][9] == 1) { echo "Yes"; } else { echo "No"; } ?></td>
<td><?php if($gi[1][9] == 1) { echo "Yes"; } else { echo "No"; } ?></td>
<td><?php if($gi[2][9] == 1) { echo "Yes"; } else { echo "No"; } ?></td>
</tr>
</table>
</div>
</body>

</html>

<?php
}
else {
	header("Location: ../home.php?erroraccess=1");
}
?>