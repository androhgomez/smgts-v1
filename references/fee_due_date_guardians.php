<?php
include("../password_protect.php");
if ($_SESSION['is_emp'] == 0) {
    $querydefaults = mysql_query("SELECT * FROM system_default WHERE system_default = 0");

    $getdefaults = mysql_fetch_array($querydefaults);

    $querycheckduedates = mysql_query("SELECT * FROM payment_dues WHERE school_year = '" . $getdefaults[1] . "'");

    if (mysql_num_rows($querycheckduedates) < 1) {
        mysql_query("INSERT INTO payment_dues VALUES('quarterly', '2', '0000-00-00', '" . $getdefaults[1] . "')");
        mysql_query("INSERT INTO payment_dues VALUES('quarterly', '3', '0000-00-00', '" . $getdefaults[1] . "')");
        mysql_query("INSERT INTO payment_dues VALUES('quarterly', '4', '0000-00-00', '" . $getdefaults[1] . "')");
        mysql_query("INSERT INTO payment_dues VALUES('semi-annual', '2', '0000-00-00', '" . $getdefaults[1] . "')");
    }

    if (isset($_GET['chgdate'])) {
        $chgdate = $_GET['year'] . "-" . $_GET['mon'] . "-" . $_GET['day'];
        mysql_query("UPDATE payment_dues SET due_date = '" . $chgdate . "' WHERE payment_type = '" . $_GET['pt'] . "' AND installment = '" . $_GET['in'] . "' AND school_year = '" . $_GET['sy'] . "'");
    }

    $semiannual = array();
    $quarterly = array();

    $queryduedates = mysql_query("SELECT COALESCE(DATE_FORMAT(due_date, '%M %d, %Y'), 'NOT SET') due FROM payment_dues WHERE payment_type = 'semi-annual' AND school_year = '" . $getdefaults[1] . "'");

    $index = 0;

    while ($getduedates = mysql_fetch_array($queryduedates)) {
        $semiannual[$index] = $getduedates;
        $index++;
    }

    $queryduedates = mysql_query("SELECT COALESCE(DATE_FORMAT(due_date, '%M %d, %Y'), 'NOT SET') due FROM payment_dues WHERE payment_type = 'quarterly' AND school_year = '" . $getdefaults[1] . "'");

    $index = 0;

    while ($getduedates = mysql_fetch_array($queryduedates)) {
        $quarterly[$index] = $getduedates;
        $index++;
    }
    ?>

    <html>

        <head>
            <title>Payment Due Dates</title>
            <link href="../main_style.css" rel="stylesheet" type="text/css">
            <link rel="stylesheet" type="text/css" href="../dialog_box.css" />
            <script type="text/javascript" src="../dialog_box.js"></script>

            <!--FOR SITE INDICATOR---->

            <script src="js/jquery.js" type="text/javascript"></script>
            <script type="text/javascript" language="javascript">
                //  Developed by Roshan Bhattarai 
                //  Visit http://roshanbh.com.np for this script and more.
                //  This notice MUST stay intact for legal use
                $(document).ready(function()
                {
                    //scroll the message box to the top offset of browser's scrool bar
                    $(window).scroll(function()
                    {
                        $('#message_box2').animate({top:$(window).scrollTop()+"px" },{queue: false, duration: 350});  
                    });
                    //when the close button at right corner of the message box is clicked 
                    $('#close_message').click(function()
                    {
                        //the messagebox gets scrool down with top property and gets hidden with zero opacity 
                        $('#message_box2').animate({ top:"+=15px",opacity:0 }, "slow");
                        $('#bulletintable').animate({ top:"-=40px" }, "slow")
                    });
                });
            </script>


            <!------>

        </head>

        <body topmargin="0">
            <div id="message_box2"><img id="close_message" style="float:right;cursor:pointer"  src="../images/cross.png" />
                <img src="../images/arrow.png"> <font color="green"><b>Payment Due Dates</b></font>
            </div>
            <div id="content">

                <div id="bulletintable" style="position:absolute; top:47px; width:98%">
                    <table id="tbwb" border="0" cellspacing="0" cellpadding="6" width="100%">
                        <tr>
                            <td>SY : <?php echo $getdefaults[1]; ?></td><td colspan="3">Due Date of Payments</td>
                        </tr>
                        <tr>
                            <td width="140" style="background-image:url('../images/tbwbbg.png');">Payment Option</td>
                            <td width="140" style="background-image:url('../images/tbwbbg.png');">Annual</td>
                            <td width="140" style="background-image:url('../images/tbwbbg.png');">Semi-Annual</td>
                            <td width="140" style="background-image:url('../images/tbwbbg.png');">Quarterly</td>
                        </tr>
                        <tr>
                            <td>First Installment</td>
                            <td>Upon Enrollment</td>
                            <td>Upon Enrollment</td>
                            <td>Upon Enrollment</td>
                        </tr>
                        <tr>
                            <td>Second Installment</td>
                            <td rowspan="3" style="background-color:#ef968b"></td>
                            <td><?php echo $semiannual[0][0]; ?></td>
                            <td><?php echo $quarterly[0][0]; ?></td>
                        </tr>
                        <tr>
                            <td>Third Installment</td>
                            <td rowspan="2" style="background-color:#ef968b"></td>
                            <td><?php echo $quarterly[1][0]; ?></td>
                        </tr>
                        <tr>
                            <td>Fourth Instalment</td>
                            <td><?php echo $quarterly[2][0]; ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </body>

    </html>
    <?php
} else {
    header("Location: ../home.php?erroraccess=1");
}
?>