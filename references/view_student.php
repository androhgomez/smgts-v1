<?php
	include("../password_protect.php");
	
	#FOR GRADE VIEWING
	
	$studentid = $_GET['studentid'];
	//Retrieve required data
	#$studentyear = $_GET['studentyear'];
	#$studentqueue  = $_GET['studentqueue'];
	$itemcode = $_GET['itemcode'];
	$newgrade = $_GET['grade'];
	$subject = $_GET['subject'];
	#$useryear = $_SESSION['useryear'];
	#$userqueue = $_SESSION['userqueue'];
	#$compareyear = "";
	#$comparequeue = "";
	$level = $_GET['level'];
	$section = $_GET['section'];
	$categories = array();
	$index = 0;
	$quarter = 0;
	$systemstartyear = 0;
	$systemendyear = 0;
	$dept = $_GET['dept'];

	$percentages = array();
	$itemcount = array();
	$totalgrades = array();
	$converted = array();
	$displaytotal = 0;
	$totalnumberofitems = 0;

	$currentlevel = "";
	$currentsection = "";
	$currentstatus = "";
	$ae = "";

	if(isset($_GET['syfrom']) && isset($_GET['syto']) && isset($_GET['sysquarter'])){
		$schoolyear = $_GET['syfrom']."-".$_GET['syto'];
		$quarter = $_GET['sysquarter'];

	} else {
		
		if(isset($_GET['sy'])){
			$schoolyear = $_GET['sy'];
		} else {
			$getsystemsettings = mysql_query("SELECT * FROM system_default WHERE system_default = 0");
			

			while($settingsrecord = mysql_fetch_array($getsystemsettings)) {
				$schoolyear = $settingsrecord[1];
				$quarter = $settingsrecord[2];
			#$systemstartyear = $settingsrecord[1];
			#$systemendyear = $settingsrecord[2];
			#$quarter = $settingsrecord[3];
			break;
			}
		}
		
		if($_GET['qtr']) {
			$quarter = $_GET['qtr'];
		}

	}
	
	

#END OF GRADE VIEWING VARIABLES
	
	$studentid = $_GET['id'];
	
	$CHECKSTUDENT = mysql_query("SELECT * FROM student WHERE student_id = '$studentid'");
	$checkid = array();
	
	if($_SESSION['is_emp'] != 1) {
		
		$index = -1;
		$queryconnection = mysql_query("SELECT guardian_id FROM connection WHERE student_id = '$studentid'");
		while($getconnection = mysql_fetch_array($queryconnection)) {
			$index++;
			$checkid[$index] = $getconnection[0];
		}
	}
	else {
		$queryteacher = mysql_query("SELECT * FROM class c LEFT JOIN enroll e ON c.class_name = e.class_name AND c.level_id = e.level_id WHERE e.student_id = '$studentid' AND (c.class_adviser = '".$_SESSION['userid']."' OR c.co_adviser = '".$_SESSION['userid']."')");
	}
	
	
	if($gp['is_VIP'] == 1 || in_array($userid, $checkid) || mysql_num_rows($queryteacher) > 0 || ($gp['ref_students'] == 1 && !empty($studentid) && mysql_num_rows($CHECKSTUDENT) > 0)) {
	
	$studentname = "";
	$dob = "";
	$gender = "";
	$homeaddress = "";
	$mailingaddress = "";
	$citizenship = "";
	$image = "";
	
	$querystudent = mysql_query("SELECT * FROM student WHERE student_id = '$studentid'");
	
	while($getstudent = mysql_fetch_array($querystudent)) {
		$studentname = $getstudent[1] . " " . substr($getstudent[2], 0, 1) . ". " . $getstudent[3];
		$dob = $getstudent[4];
		if($getstudent[5] == "m") {
			$gender = "Male";
		}
		else {
			$gender = "Female";
		}
		$homeaddress = $getstudent[6];
		$mailingaddress = $getstudent[7];
		$citizenship = $getstudent[8];
		$image = $getstudent[9];
	}
	
	
	
	$queryls = mysql_query("SELECT class_name, level_id, estatus FROM enroll WHERE student_id = '$studentid' AND school_year = '$schoolyear'");
	if(mysql_num_rows($queryls) > 0) {
		while($getls = mysql_fetch_array($queryls)) {
			$currentlevel = ucfirst($getls[1]);
			$currentsection = ucfirst($getls[0]);
			$ae = $getls[2];
			if($getls[2] == "enrolled") {
				$currentstatus = "<font color='green'>".ucfirst($getls[2])."</font>";
			}
			else {
				$currentstatus = "<font color='#de751a'>".ucfirst($getls[2])."</font>";
			}
		}
	}
	else {
		$currentlevel = "(Not Available)";
		$currentsection = "(Not Available)";
		$currentstatus = "Neither assessed or enrolled";
	}
	
?>
<html>

<head>
<title>View Student</title>
<link href="../main_style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="../dialog_box.css" />
<script type="text/javascript" src="../dialog_box.js"></script>

<!--FOR SITE INDICATOR---->

<script src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
//  Developed by Roshan Bhattarai 
//  Visit http://roshanbh.com.np for this script and more.
//  This notice MUST stay intact for legal use
$(document).ready(function()
{
	//scroll the message box to the top offset of browser's scrool bar
	$(window).scroll(function()
	{
  		$('#message_box2').animate({top:$(window).scrollTop()+"px" },{queue: false, duration: 350});  
	});
    //when the close button at right corner of the message box is clicked 
	$('#close_message').click(function()
	{
  		//the messagebox gets scrool down with top property and gets hidden with zero opacity 
		$('#message_box2').animate({ top:"+=15px",opacity:0 }, "slow");
		$('#bulletintable').animate({ top:"-=42px" }, "slow")
	});
});
</script>


<!------>

</head>
<body topmargin="0">

<div id="message_box2"><img id="close_message" style="float:right;cursor:pointer"  src="../images/cross.png" />
<img src="../images/arrow.png"> <font color="green"><b>Student Information</b></font>
</div>

<div id="content">
<div id="bulletintable" style="position:absolute; top:42px; width:100%;">
<table border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
	<td background="../images/submenus.png" width="552" height="27" style="text-align:center;">
	<?php if($_SESSION['is_emp'] == 1) { ?>
	<a href="add_student.php?update=1&idtobeup=<?php echo $studentid; ?>">Update Info</a> <img src="../images/button.png" /> 
	<a href="uploadimage.php?user=student&idtobeup=<?php echo $studentid; ?>">Update Image</a> <img src="../images/button.png" /> 
	<a href="assess_enroll.php?eid=<?php echo $studentid; ?>">Assess/Enroll</a> <img src="../images/button.png" /> 
	<a href="view_guardian.php?eid=<?php echo $studentid; ?>">View Guardian Details</a>
	<?php } else { ?>
	<a href="assess_enroll.php?eid=<?php echo $studentid; ?>">View Assessment / Enroll Information</a>
	<?php } ?>
	<img src="../images/button.png" /> 
	<a href="view_student_archive.php?id=<?php echo $studentid; ?>">View Archive</a>
	</td>
	</tr>
</table>
<table border="0" cellpadding="3" cellspacing="0" style="margin-top:15px; width:100%">
	<tr><td style="text-align:right;" width="150px">Student ID:</td>
	<td width="400px" align="padding-right:30px"><?php echo $studentid; ?></td>
	<td rowspan="8" style="text-align:center; vertical-align:top;"><img src="../<?php echo $image; ?>" style="border: 1px dashed #cecece" />
	</td></tr>
	
	<tr><td style="text-align:right;" width="100px">Name:</td><td align="padding-right:30px"><?php echo $studentname; ?></td></tr>
	<tr><td style="text-align:right;" width="100px">Date of Birth:</td><td align="padding-right:30px"><?php echo date("F d, Y", strtotime($dob)); ?></td></tr>
	<tr><td style="text-align:right;" width="100px">Gender:</td><td align="padding-right:30px"><?php echo $gender; ?></td></tr>
	<tr><td style="text-align:right;" width="100px">Home Address:</td><td align="padding-right:30px"><?php echo $homeaddress; ?></td></tr>
	<tr><td style="text-align:right;" width="100px">Mailing Address:</td><td align="padding-right:30px"><?php echo $mailingaddress; ?></td></tr>
	<tr><td style="text-align:right;" width="100px">Citizenship:</td><td align="padding-right:30px"><?php echo $citizenship; ?></td></tr>
	<tr><td style="text-align:right;" width="100px">Current Status:</td><td align="padding-right:30px"><?php echo $currentstatus; ?></td></tr>
	<tr><td style="text-align:right;" width="100px">Current Level:</td><td align="padding-right:30px"><?php echo $currentlevel; ?></td></tr>
	<tr><td style="text-align:right;" width="100px">Current Section:</td><td align="padding-right:30px"><?php echo $currentsection; ?></td></tr>
	<tr>
	<td colspan="3"><center><hr style="width: 90%; margin:15px;" /></center></td>
	</tr>
</table>

<?php 
if($ae == "enrolled") {
?>
<center>See Grades for - 
	<a href="view_student.php?id=<?php echo $studentid; ?>&qtr=1&viewallgrades=1<?php
	 if(isset($_GET['syfrom']) && isset($_GET['syto']) && isset($_GET['sysquarter']) || isset($_GET['sy'])){
	 	echo "&sy=".$schoolyear;
	 }
	 	?>">
		First Quarter
	</a> 
	<img src="../images/button.png" />
	<a href="view_student.php?id=<?php echo $studentid; ?>&qtr=2&viewallgrades=1<?php
	 if(isset($_GET['syfrom']) && isset($_GET['syto']) && isset($_GET['sysquarter']) || isset($_GET['sy'])){
	 	echo "&sy=".$schoolyear;
	 }
	 	?>">Second Quarter</a> 
	 <img src="../images/button.png" />
	 <a href="view_student.php?id=<?php echo $studentid; ?>&qtr=3&viewallgrades=1<?php
	 if(isset($_GET['syfrom']) && isset($_GET['syto']) && isset($_GET['sysquarter']) || isset($_GET['sy'])){
	 	echo "&sy=".$schoolyear;
	 }
	 	?>">Third Quarter </a><img src="../images/button.png" /> 
	 <a href="view_student.php?id=<?php echo $studentid; ?>&qtr=4&viewallgrades=1<?php
	 if(isset($_GET['syfrom']) && isset($_GET['syto']) && isset($_GET['sysquarter']) || isset($_GET['sy'])){
	 	echo "&sy=".$schoolyear;
	 }
	 	?>">Fourth Quarter</a></center>
<br /><br />
<center>Grades for 
<?php
			switch($quarter) {
				case 1:
					echo "First Quarter";
				break;	
				case 2:
					echo "Second Quarter";
				break;	
				case 3:
					echo "Third Quarter";
				break;	
				case 4:
					echo "Fourth Quarter";
				break;	
			}
		?></center><br />
<?php
$queryallsubj = mysql_query("SELECT s.subject_code, s.dep_code, e.level_id, e.class_name FROM schedule s LEFT JOIN enroll e ON s.level_id = e.level_id AND s.class_name = e.class_name LEFT JOIN departments d ON s.dep_code = d.dep_code WHERE e.student_id = '$studentid' AND d.unofficial = 'no'  AND e.school_year = '$schoolyear' GROUP BY 1"); 

while($getallsubj = mysql_fetch_array($queryallsubj)) {
	$index = 0;
	$dept = $getallsubj['dep_code'];
	$subject = $getallsubj['subject_code'];
	$section = $getallsubj['class_name'];
	$level = $getallsubj['level_id'];
	
	$getempname = mysql_query("SELECT CONCAT(fname, ' ', SUBSTR(mname, 1, 1), ' ', lname) FROM subjects WHERE subject_code = '$subject'");
	
	$getstudents = mysql_query("SELECT s.* FROM student s LEFT JOIN enroll e ON s.student_id = e.student_id WHERE e.student_id = '$studentid' AND e.class_name = '$section' AND e.level_id = '$level' AND e.estatus = 'enrolled' AND e.school_year = '$schoolyear' ORDER BY s.lname");
	
	
	$getcategories = mysql_query("SELECT g.category, COUNT( g.gi_code ), c.percentage 'numitems'
									FROM category_percentage c
									LEFT JOIN gradeitems g ON c.category = g.category
									WHERE g.level_id = '$level'
									AND g.class_name = '$section'
									AND g.subject_code =  '$subject'
									AND c.dep_code =  '$dept' 
									AND g.quarter = '$quarter' 
									AND g.school_year = '$schoolyear'
									GROUP BY 1");
	/*
	SELECT c.category, COUNT( g.gi_code ), c.percentage  'numitems'
									FROM category_percentage c
									LEFT JOIN gradeitems g ON c.category = g.category
									WHERE c.subject_code =  '$subject'
									GROUP BY 1
	*/
									
	
	$columns = 0;
	while($record = mysql_fetch_array($getcategories)) {
		$categories[0][$index] = $record[0]; // Name of Category
		$categories[1][$index] = $record[1]; // Number of items per category
		$percentages[$index] = $record[2];
		$itemcount[$index] = $record[1];
		$index++;
		$columns += $record[1];
		$totalnumberofitems += $record[1];
	}
	$columns += 3;

	#START OF GRADE VIEWING
	?>
	
	<table width="700" border="0" id="tbwb" cellspacing="3" cellpadding="3" align="center">
	<tr>
		<td colspan="<?php if($totalnumberofitems > 0) { echo $columns; } else { echo $columns+1; } ?>" style="background-image:url('../images/tbwbbg.png');"><?php echo $subject; ?></td>
	</tr>
	<tr>
    	<td colspan="2"  style="background-image:url('../images/tbwbbg.png');">
        <?php
			switch($quarter) {
				case 1:
					echo "First Quarter Grades";
				break;	
				case 2:
					echo "Second Quarter Grades";
				break;	
				case 3:
					echo "Third Quarter Grades";
				break;	
				case 4:
					echo "Fourth Quarter Grades";
				break;	
			}
		?>
        </td>
        <?php
			if($index < 1) {
			?>
			<td style="background-image:url('../images/tbwbbg.png');">
				(No Grade Items Yet Created For <?php
			switch($quarter) {
				case 1:
					echo "First Quarter Grades";
				break;	
				case 2:
					echo "Second Quarter Grades";
				break;	
				case 3:
					echo "Third Quarter Grades";
				break;	
				case 4:
					echo "Fourth Quarter Grades";
				break;	
			}
		?>)
			</td> 
			<?php
			}
			else {
        	for($index_2 = 0; $index_2 < $index; $index_2++) {
		?>
        	<td colspan="<?php echo $categories[1][$index_2]; ?>"  style="background-image:url('../images/tbwbbg.png');">
            	<?php echo $categories[0][$index_2]; ?>
				
            </td>
        <?php
			}
			}
		?>
		<td style="background-color:#ef968b" rowspan="2">Total</td>
    </tr>
    <tr>
    	<td style="background-color:#d2f5d7">
        	Student ID
        </td>
        <td style="background-color:#d2f5d7">
        	Student Name
        </td>
        <?php
			for($index_2 = 0; $index_2 < $index; $index_2++) {
				$getitems = mysql_query("SELECT * FROM gradeitems WHERE category = '".$categories[0][$index_2]."' AND subject_code = '$subject' AND quarter = '$quarter' AND school_year = '$schoolyear' ORDER BY gi_code");
				while($record = mysql_fetch_array($getitems)) {
				?>
                	<td style="background-color:#d2f5d7">
                    	<?php echo $record['gi_code']; ?>
                    </td>
                <?php
				}
			}
			
		?>
    </tr>
    
    	<?php
			while($record = mysql_fetch_array($getstudents)) {
			$converted = array();
			$totalgrades = array();
			$exemptions = array();
			$displaytotal = 0;
			?>
            <tr>
            	<td>
                	<?php echo $record[0]; ?>
                </td>
            	<td>
                	<?php echo $record[3] . ", " . $record[1] . " " . substr($record[2],0,1) . ". "; ?>
                </td>
                
        <?php
			if($totalnumberofitems < 1) {
				?>
				<td></td>
				<?php
			}
			else {
			for($index_2 = 0; $index_2 < $index; $index_2++) {
				
				$getitems = mysql_query("SELECT * FROM gradeitems WHERE category = '".$categories[0][$index_2]."' AND quarter = '$quarter' AND subject_code = '$subject' ORDER BY gi_code");

				while($record2 = mysql_fetch_array($getitems)) {
				$temprecgrade = 0;
				?>
                	<td>
                    	<?php
							
							$getgrades = mysql_query("SELECT student_grade FROM grades 
								WHERE subject_code = '$subject' AND student_id = '" 
								. $record[0] . "' AND school_year = '$schoolyear' AND quarter = '$quarter' AND gi_code = '" 
								. $record2['gi_code'] . "'");
							
														
							?>
                            <?php
							if(mysql_num_rows($getgrades) < 1) {
							?>
								<font color="#de751a">n/a</font>
							<?php
								$temprecgrade = 0; // Zero for Temporary Grade for Totalling
							}
							else {
							
								while($record2 = mysql_fetch_array($getgrades)) {					
									if($record2[0] < 0) {
									?>
										<font color="613ec7">Exempted</font>
									<?php
									$exemptions[$index_2] =+ 1;
									}
									else {
									
									?>
										<?php echo $record2[0]; ?>
									<?php
									$temprecgrade = $record2[0]; // Insert record into Temporary Grade for Totalling
									break;	
									}
								}
							
							}
							$totalgrades[$index_2] += $temprecgrade;
							
						?>
							
                    </td>
                <?php
				}
			}
			}
				for($i = 0; $i < count($totalgrades); $i++) {
					if($exemptions[$i] > 0 && $itemcount[$i] != 1) {
						$converted[$i] = ($totalgrades[$i] / ($itemcount[$i] - $exemptions[$i])) * ($percentages[$i]/100);
						$displaytotal += $converted[$i];
					}
					else {
						$converted[$i] = ($totalgrades[$i] / $itemcount[$i]) * ($percentages[$i]/100);
						$displaytotal += $converted[$i];
					}
					
				}
		?>       
            <td><?php echo number_format($displaytotal, 2, '.', ''); ?></td>
                
            </tr>
            <?php	
			}
		?>
    
</table>
<br />
<br />
	
	
	<?php
	#END OF GRADE VIEWING

}

}
elseif($_GET['viewallgrades'] && $ae != "enrolled") {
	echo "<center>Unable to display grades. Student not yet enrolled</center>";
}
?>
</div>
</div>
</body>

<?php
	}
	elseif($gp['ref_students'] == 0) {
		header("Location: ../home.php?erroraccess=1");
	}
	elseif(mysql_num_rows($CHECKSTUDENT) < 1 || empty($studentid)) {
		header("Location: ../home.php?erroraccess=2");
	}
?>
</html>