<?php
include('../password_protect.php');

if($gp['is_VIP'] == 1 || ($gp['ref_update_img'] == 1 && $gp['control_edit'] == 1 && !empty($_GET['idtobeup']))) {

$USERID = $_GET['idtobeup'];
$USERTYPE = $_GET['user'];

if(isset($_POST['uppic'])) {

$image = $_FILES['userim']['tmp_name'];

	if($image) {

		//define a maxim size for the uploaded images in Kb
		define ("MAX_SIZE","1000"); 
	
		//This function reads the extension of the file. It is used to determine if the file  is an image by checking the extension.
 		function getExtension($str) {
			$i = strrpos($str,".");
			if (!$i) { return ""; }
			$l = strlen($str) - $i;
			$ext = substr($str,$i+1,$l);
			return $ext;
 		}
		
		$errors=0;
		
		list($width, $height, $type, $attr) = getimagesize($image);
		
		if ($width == 144 || $height == 144) {
			//get the original name of the file from the clients machine
				$filename = stripslashes($_FILES['userim']['name']);
				//get the extension of the file in a lower case format
				$extension = getExtension($filename);
				$extension = strtolower($extension);
				//if it is not a known extension, we will suppose it is an error and will not  upload the file,  
				//otherwise we will do more tests
				if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) 
				{
					//print error message
					echo "<script language='javascript'>alert(\"Unknown extension!\");</script>";
					$errors=1;
				}
				else
				{
					//get the size of the image in bytes
					//$_FILES['userim']['tmp_name'] is the temporary filename of the file
					//in which the uploaded file was stored on the server
					$size=filesize($_FILES['userim']['tmp_name']);
					
					//compare the size with the maxim size we defined and print error if bigger
					if ($size > MAX_SIZE*1024)
					{
						echo "<script language='javascript'>alert(\"Exceeded File Limit!\");</script>";
						$errors=1;
					}
					
					//we will give an unique name, for example the time in unix time format
					$image_name=substr($_POST['userid'],0,4).'_'.substr($_POST['userid'],5,9).'.'.$extension;
					//the new name will be containing the full path where will be stored (images folder)
					if($_POST['usertype'] == "emp") {
						$newname="emp_img/".$image_name;
					}
					elseif($_POST['usertype'] == "student") {
						$newname="student_img/".$image_name;
					}

					//we verify if the image has been uploaded, and print error instead
					if (!$errors) {
						$copied = copy($_FILES['userim']['tmp_name'], "../$newname");
					}
					
					
					if (!$copied) 
					{
						echo "<script language='javascript'>alert(\"Copy Unsuccessful!\");</script>";
						$errors=1;
					}
				}
		
		
		if(isset($_POST['uppic']) && !$errors) 
		{
			
			if($_POST['usertype'] == "emp") {
				$query = "UPDATE employee SET image = '$newname' WHERE employee_id = '".$_POST['userid']."'";
			}
			elseif($_POST['usertype'] == "student") {
				$query = "UPDATE student SET image = '$newname' WHERE student_id = '".$_POST['userid']."'";
			}
			mysql_query($query);
			echo "<script language='javascript'>alert(\"Upload Complete!\");</script>";
		}
		}
		
		
		else {
			echo "<script language='javascript'>alert(\"Image should be exactly 144x144 pixels\");</script>";	
		}
		
		
		
		$USERID = $_POST['userid'];
		$USERTYPE = $_POST['usertype'];
		
	}

}

?>
<html>
<head>
<title>Upload Image For <?php echo $_GET['idtobeup']; ?></title>
<link href="../main_style.css" rel="stylesheet" type="text/css">
<!--FOR SITE INDICATOR---->

<script src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
//  Developed by Roshan Bhattarai 
//  Visit http://roshanbh.com.np for this script and more.
//  This notice MUST stay intact for legal use
$(document).ready(function()
{
	//scroll the message box to the top offset of browser's scrool bar
	$(window).scroll(function()
	{
  		$('#message_box2').animate({top:$(window).scrollTop()+"px" },{queue: false, duration: 350});  
	});
    //when the close button at right corner of the message box is clicked 
	$('#close_message').click(function()
	{
  		//the messagebox gets scrool down with top property and gets hidden with zero opacity 
		$('#message_box2').animate({ top:"+=15px",opacity:0 }, "slow");
		$('#siteindicator').animate({ top:"-=40px" }, "slow")
	});
});
</script>


<!------>
</head>
<body topmargin="0">
<div id="message_box2"><img id="close_message" style="float:right;cursor:pointer"  src="../images/cross.png" />
<img src="../images/arrow.png"> <font color="green"><b>
<?php 
	if($USERTYPE == "emp") {
		echo "<a href='view_emp.php?id=".$_GET['idtobeup']."'>View Employee</a> > Update Employee Image";
	}
	elseif($USERTYPE == "student") {
		echo "<a href='view_student.php?id=".$_GET['idtobeup']."'>View Student</a> > Update Student Image";
	}
	
?>
</b></font>
</div>
<div id="siteindicator" style="position:absolute; top:55px">
<form method="POST" enctype="multipart/form-data">
<table border="0" cellpadding="3" cellspacing="0" width="100%">
<tr><td style="text-align:right;" width="100px"><input type="hidden" name="usertype" value="<?php echo $USERTYPE; ?>" /><?php echo ucfirst($USERTYPE) . " ID"; ?></td><td align="padding-right:30px"><input type="text" name="userid" value="<?php echo $USERID; ?>" READONLY style="width:80px" />

</td></tr>
<tr><td style="text-align:right;" width="100px">Locate Image</td><td align="padding-right:30px"><input type="file" name="userim"></td></tr>
<tr><td style="text-align:right;" width="100px"><font color="#FF0000">Note:</font></td><td align="padding-right:30px"><font color="#FF0000">Image must be exactly 144x144 pixels.</font></td></tr>
<td colspan="3"><center><hr style="width: 90%; margin:15px;" /></center></td>
<tr><td style="text-align:right;" width="100px"></td><td align="padding-right:30px"><input type="submit" name="uppic" id="uppic" value="Submit" /></td></tr>

<table>
</form>
</body>
</div>
</html>
<?php
	}
		else {
		header("Location: ../home.php?erroraccess=1");
	}
?>