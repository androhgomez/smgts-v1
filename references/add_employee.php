<?php
	include("../password_protect.php");
	if($gp['is_VIP'] == 1 || ($gp['ref_employee'] == 1 && $gp['control_add'] == 1 && $gp['control_edit'] == 1)) {
		if($_GET['update'] != 1 || empty($_GET['idtobeup'])) {
		$idsy = $_SESSION['sysde']['ssy'];
		$getmaxid = mysql_query("SELECT MAX(substr(employee_id,6,4)) unique_num FROM employee WHERE employee_id LIKE '$idsy-%'");
		$uniquenum = 0;
			
			while($getuniquenum = mysql_fetch_array($getmaxid)) {
				$uniquenum = $getuniquenum[0] + 1;
			}
			switch(strlen($uniquenum)) {
			case 1:
				$uniquenum = "000" . $uniquenum;
				break;
			case 2:
				$uniquenum = "00" . $uniquenum;
				break;
			case 3:
				$uniquenum = "0" . $uniquenum;
				break;
			default:
				break;

			}
			
			$empinfo = array($idsy . "-" . $uniquenum);
			
		}
		elseif($_GET['update'] == 1 && !empty($_GET['idtobeup'])) {
			$proceedpage = "../submitdetails.php";
			
			$queryemployee = mysql_query("SELECT e.*, ua.group_name, ua.user_pass FROM employee e LEFT JOIN user_access ua ON e.user_name = ua.user_name WHERE employee_id = '".$_GET['idtobeup']."'");
			
			$empinfo = array();
			
			while($getrecords = mysql_fetch_array($queryemployee)) {
				for($i = 0; $i<=19; $i++) {
					$empinfo[$i] = $getrecords[$i];
				}
			}
			
			
		}
	
?>
<html>

<head>

<title><?php echo isset($_GET['update']) ? "Update Employee" : "Add Employee"; ?></title>
<link href="../main_style.css" rel="stylesheet" type="text/css">
<!--FOR SITE INDICATOR---->

<script src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
//  Developed by Roshan Bhattarai 
//  Visit http://roshanbh.com.np for this script and more.
//  This notice MUST stay intact for legal use
$(document).ready(function()
{
	//scroll the message box to the top offset of browser's scrool bar
	$(window).scroll(function()
	{
  		$('#message_box2').animate({top:$(window).scrollTop()+"px" },{queue: false, duration: 350});  
	});
    //when the close button at right corner of the message box is clicked 
	$('#close_message').click(function()
	{
  		//the messagebox gets scrool down with top property and gets hidden with zero opacity 
		$('#message_box2').animate({ top:"+=15px",opacity:0 }, "slow");
		$('#siteindicator').animate({ top:"-=40px" }, "slow")
	});
});
</script>


<!------>
<script type="text/javascript">	

	function checkyear() {
		var d = new Date();
		var curyear = d.getFullYear();
		var inputyear = parseInt(document.getElementById("yearb").value);
		if(inputyear > curyear) {
			document.getElementById("yearb").value = "";
			alert("Impossible: Birth Year > Current Year");
		}
	}

	function numdays() {
		var monv = parseInt(document.getElementById("monb").value);
		var d = new Date();
		var curyear = d.getFullYear();
		var checkleap = curyear % 4;
		switch(monv) {
			case 2:
				if (checkleap != 0) {
					changedays(28);
				}
				else {
					changedays(29);
				}
			break;
			case 4:
			case 6:
			case 9:
			case 11:
				changedays(30);
			break;
			default:
				changedays(31);
			break;
		
		}
	}
	
	function changedays(d) {
		document.getElementById("dayb").length = 0;
		var i = 0;
		for(i = 1; i <= d; i++) {
			if (i == 1) {
				document.getElementById("dayb").options[i-1] = new Option(i, i, false, true);
			}
			else {
				document.getElementById("dayb").options[i-1] = new Option(i, i, false, false);
			}
		}
	}
	
	function checkuserin(str)
	{
	var xmlhttp;
	if (str.length==0)
	{
		document.getElementById("usernamenotice").innerHTML="";
		return;
	}
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
		xmlhttp.onreadystatechange=function()
	{
	if (xmlhttp.readyState==4 && xmlhttp.status==200)
	{
		document.getElementById("usernamenotice").innerHTML=xmlhttp.responseText;
	}
	}
	<?php
		if($_GET['update'] != 1) {
	?>
	xmlhttp.open("GET","../checkuser.php?q="+str,true);
	<?php
		}
		else {
	?>
	xmlhttp.open("GET","../checkuser.php?excludeus=<?php echo $empinfo[17]; ?>&q="+str,true);
	<?php 
		} 
	?>
	xmlhttp.send();
	checkplength(document.getElementById("newpass").value);
}

	function checkplength(l)
	{
	if(l.length >= 6) {
		document.getElementById("passwordnotice").innerHTML = "<font color='green'>Valid Password</font><input id='stp' name='stp' type='hidden' value='true'>";
	}
	else {
		document.getElementById("passwordnotice").innerHTML = "<font color='red'>Password must be at least 6 characters</font><input id='stp' name='stp' type='hidden' value='false'>";
	}
	
	var su = document.getElementById('stu').value;
	var sp = document.getElementById('stp').value;
	
	if(su == 'true')
	{
		if(sp == 'true')
		{
			document.getElementById('submit3').disabled = false;
		}
		else
		{
			document.getElementById('submit3').disabled = true;	
		}
	}
	else
	{
		document.getElementById('submit3').disabled = true;	
	}
}

function validateform()
{
	var fname = document.getElementById("fname").value;
	var mname = document.getElementById("mname").value;
	var lname = document.getElementById("lname").value;
	var yearb = document.getElementById("yearb").value;
	var haddress = document.getElementById("haddress").value;

	var citi = document.getElementById("citi").value;
	
	if(fname=="" || mname=="" || lname=="" || yearb=="" || haddress=="" || maddress=="" || citi=="") {
		alert("Some fields are empty");
		return false;
		
	}
	else {
		var a = confirm("Are you sure?")
		if(a == true) {
			return true;
		}
		else {
			return false;
		}
	}
	
	
}
	
</script>
</head>

<body topmargin="0">
<div id="message_box2"><img id="close_message" style="float:right;cursor:pointer"  src="../images/cross.png" />
<img src="../images/arrow.png"> <font color="green"><b><?php echo isset($_GET['update']) ? "<a href='view_emp.php?id=".$_GET['idtobeup']."'>View Employee</a> > Update Employee" : "Add Employee"; ?></b></font>
</div>
<div id="siteindicator" style="position:absolute; top:47px">
<form action="../submitdetails.php" method="post" onSubmit="return validateform()">
<table border="0" cellpadding="3" cellspacing="0" width="100%">

	<tr>
	<td width="100px" style="text-align:right;">Employee ID</td><td style="padding-left:30px;"><input type="text" name="empid" style="width:100px" value="<?php echo $empinfo[0]; ?>" /><?php if($_GET['update'] != 1) {?>&nbsp;&nbsp;&nbsp;<font color="red">Note: Employee ID will be <u>PERMANENT</u> after adding the employee details to the DB.<?php } ?></td>
	</tr>
	
	<tr>
	<td style="text-align:right;">First Name</td><td style="padding-left:30px;"><input type="text" id="fname" name="fname" value="<?php echo $empinfo[1]; ?>" /></td>
	</tr>
	
	<tr>
	<td style="text-align:right;">Middle Name</td><td style="padding-left:30px;"><input type="text" id="mname" name="mname" value="<?php echo $empinfo[2]; ?>" /></td>
	</tr>
	
	<tr>
	<td style="text-align:right;">Last Name</td><td style="padding-left:30px;"><input type="text" id="lname" name="lname" value="<?php echo $empinfo[3]; ?>" /></td>
	</tr>
	
	<tr>
	<td style="text-align:right;">Date of Birth</td>
	<td style="padding-left:30px;">
	Month: 
        <select name="monb" id="monb" onchange="javascript:numdays()">
        <option value="1">January</option>
        <option value="2">February</option>
        <option value="3">March</option>
        <option value="4">April</option>
        <option value="5">May</option>
        <option value="6">June</option>
        <option value="7">July</option>
        <option value="8">August</option>
        <option value="9">September</option>
        <option value="10">October</option>
        <option value="11">November</option>
        <option value="12">December</option>
      </select>
      Day: 
      <select name="dayb" id="dayb">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
		<option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
        <option value="19">19</option>
        <option value="20">20</option>
        <option value="21">21</option>
        <option value="22">22</option>
        <option value="23">23</option>
        <option value="24">24</option>
        <option value="25">25</option>
        <option value="26">26</option>
        <option value="27">27</option>
        <option value="28">28</option>
        <option value="30">29</option>
        <option value="31">31</option>
      
      </select>
      Year: 
      <input type="text" name="yearb" id="yearb" size="3" maxlength="4" onkeyup="javascript:checkyear()" value="<?php echo substr($empinfo[4], 0, 4); ?>" />
	</td>
	</tr>
	
	<tr>
	<td style="text-align:right;">Gender</td>
	<td style="padding-left:30px;">
	<select name="gender" id="gender">
        <option value="m" <?php if($empinfo[5] == "m") {echo " selected";} ?> >Male</option>
        <option value="f" <?php if($empinfo[5] == "f") {echo " selected";} ?> >Female</option>
    </select>
	</td>
	</tr>
	
	<tr>
	<td style="text-align:right;">Home Address</td>
	<td style="padding-left:30px;"><textarea name="haddress" id="haddress" cols="45" rows="5"><?php echo $empinfo[6]; ?></textarea>
	</td>
	</tr>
	
	<tr>
	<td style="text-align:right;">Mailing Address<br />(Optional)</td>
	<td style="padding-left:30px;"><textarea name="maddress" id="maddress" cols="45" rows="5"><?php echo $empinfo[7]; ?></textarea>
	</td>
	</tr>
	
	<tr>
	<td style="text-align:right;">E-Mail Address<br />(Encouraged)</td>
	<td style="padding-left:30px;"><input type="text" name="email" id="email" value="<?php echo $empinfo[8]; ?>">
	</td>
	</tr>
	
	<tr>
	<td style="text-align:right;">Citizenship</td>
	<td style="padding-left:30px;">
	<input type="text" name="citi" id="citi" value="<?php echo $empinfo[9]; ?>" />
	</td>
	</tr>
	
	<tr>
	<td style="text-align:right;">Status</td>
	<td style="padding-left:30px;">
	Single <input type="radio" name="status" id="status" value="single" <?php if($empinfo[10] == "single") {echo " CHECKED";} ?> />
	Married <input type="radio" name="status" id="status" value="married" <?php if($empinfo[10] == "married") {echo " CHECKED";} ?> />
	Widow / Widower <input type="radio" name="status" id="status" value="widow" <?php if($empinfo[10] == "widow") {echo " CHECKED";} ?> />
	</td>
	</tr>
	
	<tr>
	<td style="text-align:right;">Name of Spouse</td>
	<td style="padding-left:30px;">
	<input type="text" name="spouse" id="spouse" value="<?php echo $empinfo[11]; ?>" />
	</td>
	</tr>
	
	<tr>
	<td style="text-align:right;">SSS Number</td>
	<td style="padding-left:30px;">
	<input type="text" name="sss" id="sss" value="<?php echo $empinfo[12]; ?>" />
	</td>
	</tr>
	
	<tr>
	<td style="text-align:right;">Pag-ibig Number</td>
	<td style="padding-left:30px;">
	<input type="text" name="pagibig" id="pagibig" value="<?php echo $empinfo[13]; ?>" />
	</td>
	</tr>
	
	<tr>
	<td style="text-align:right;">Philhealth Number</td>
	<td style="padding-left:30px;">
	<input type="text" name="philhealth" id="philhealth" value="<?php echo $empinfo[14]; ?>" />
	</td>
	</tr>
	
	<tr>
	<td style="text-align:right;">TIN Number</td>
	<td style="padding-left:30px;">
	<input type="text" name="tin" id="tin" value="<?php echo $empinfo[15]; ?>" />
	</td>
	</tr>
	
	<tr>
	<td colspan="2">
	<center><hr style="width: 95%; margin:2px;" /></center>
	</td>
	</tr>
	
	<?php 
		$index = 0;
		$query = mysql_query("SELECT group_name FROM group_permissions WHERE group_name <> 'guardian' ORDER BY 1 DESC");
		while($getgroups = mysql_fetch_array($query)) {
			$sys_permissions[$index] = $getgroups;
			$index++;
		}
	?>
	
	<tr>
	<td style="text-align:right;">User Group</td>
	<td style="padding-left:30px;">
	<select name="usergroup" id="usergroup" style="width:150px">
	<?php
		for($i = 0; $i <= ($index - 1); $i++) {
	?>
		<option value="<?php echo $sys_permissions[$i][0] ?>" <?php if($empinfo[18] == $sys_permissions[$i][0]) { echo "selected"; } ?>><?php echo $sys_permissions[$i][0] ?></option>
	<?php
		}
	?>
	</select>
	</td>
	</tr>
	
	<tr>
	<td colspan="2">
	<center><hr style="width: 95%; margin:2px;" /></center>
	</td>
	</tr>
	
	<tr>
	<td colspan="2">
	<table border="0" cellpadding="3" cellspacing="0">
	<tr>
	<td style="text-align:right;" width="100px">New Username</td><td style="padding-left:30px;"><input type="text" id="newuser" name="newuser" value="<?php echo $empinfo[17] ?>" onkeyup="checkuserin(this.value)" value="" /></td>
	<td style="padding-left:30px; text-align:justify;"><div id="usernamenotice"></div></td>
	</tr>
	<tr>
	<td style="text-align:right;">Password</td><td style="padding-left:30px;"><input type="password" id="newpass" name="newpass" value="<?php echo $empinfo[19] ?>" onkeyup="checkplength(this.value)" /></td>
	<td style="padding-left:30px; text-align:justify;"><div id="passwordnotice"></div></td>
	</tr>
	</table>
	<td>
	</tr>
	
	<tr>
	<td colspan="2">
	<center><hr style="width: 95%; margin:2px;" /></center>
	</td>
	</tr>
	
	<tr>
	<td style="text-align:right;"></td>
	<td style="padding-left:30px;">
	<input type="submit" name="submit3" id="submit3" value="<?php if($_GET['update']==1) {echo "Update";} else {echo "Add";} ?>" <?php if($_GET['update']==1) {echo "";} else {echo "DISABLED";} ?> />
	</td>
	</tr>
<table>
</form>
</div>
<script type="text/javascript">
		document.getElementById("monb").options[<?php echo substr($empinfo[4], 5, 6) - 1; ?>].selected = true;
		document.getElementById("dayb").options[<?php echo substr($empinfo[4], 8, 9) - 1; ?>].selected = true;
</script>

</body>

</html>
<?php
}
else {
	header("Location: ../home.php?erroraccess=1");
}
?>