<?php

if (!defined('LV_ROOT')) {
    die("Sorry. You can't access directly this file");
}

class security {

    function password_hash($password) { // Simple password hashing to comply for fast production.
        $salt = "1qaz2wsx3edc";
        $encrypted_password = sha1($salt . $password);
        return $encrypted_password;
    }

    function sanitize_input($data) { // Sanitize form inputs.
        $remove = array("'", '"');
        $newdata = trim(str_replace($remove, "", $data));
        return $newdata;
    }

}

?>
