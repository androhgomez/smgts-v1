

<div id="index_menu">

</div>

<div id="basic-accordian" ><!--Parent of the Accordion-->

<?php if($_SESSION['is_emp'] == 1) { ?>

<!--Start of each accordion item-->
  <div id="test-header" class="accordion_headings header_highlight" >System</div><!--Heading of the accordion ( clicked to show n hide ) -->
  
  <!--Prefix of heading (the DIV above this) and content (the DIV below this) to be same... eg. foo-header & foo-content-->
  
  <div id="test-content"><!--DIV which show/hide on click of header-->
  
  	<!--This DIV is for inline styling like padding...-->
    <div class="accordion_child">
		<a href="index.php">Main Page</a><br /><br />
		<?php if ($gp['is_VIP'] == 1 || $gp['sys_system_default'] == 1) { ?><a href="systemsettings.php" target="content_area">System Settings & User Groups</a><br /><br /><?php }  ?>
		<!--<?php if ($gp['is_VIP'] == 1 || $gp['sys_backup'] == 1) { ?><a href="backupdb.php" target="content_area">Back-up and Export DB (Requires Internet)</a><br /><br /><?php }  ?>-->
		<?php if ($gp['is_VIP'] == 1 || $gp['sys_log'] == 1) { ?><a href="system_logs.php" target="content_area">System Logs</a><br /><br /><?php }  ?>
		<?php if ($gp['is_VIP'] == 1 || $gp['sys_bulletin'] == 1) { ?><a href="bulletin.php" target="content_area">New Bulletin Message</a><br /><br /><?php }  ?>
		<?php if ($gp['is_VIP'] == 1 || $gp['sys_bulletin'] == 1) { ?><a href="mailall.php" target="content_area">Email All Parents/Guardians</a><br /><br /><?php }  ?>
		<!--<?php if ($gp['is_VIP'] == 1) { ?><a href="monitorserver.php" target="content_area">Server Cam</a><br /><?php }  ?>-->
	</div>
    
  </div>
<!--End of each accordion item--> 


<!--Start of each accordion item-->
  <div id="test1-header" class="accordion_headings" >References</div><!--Heading of the accordion ( clicked to show n hide ) -->
  
  <!--Prefix of heading (the DIV above this) and content (the DIV below this) to be same... eg. foo-header & foo-content-->
  
  <div id="test1-content"><!--DIV which show/hide on click of header-->
  
  	<!--This DIV is for inline styling like padding...-->
    <div class="accordion_child">
		<?php if ($gp['is_VIP'] == 1 || $gp['ref_fees'] == 1) { ?><a href="references/manage_fees.php" target="content_area">Manage Fees</a><br /><?php }  ?>
		<?php if ($gp['is_VIP'] == 1 || $gp['ref_fees'] == 1) { ?><a href="references/fee_due_date.php" target="content_area">Payment Due Dates</a><br /><br /><?php }  ?>
		<?php if ($gp['is_VIP'] == 1 || ($gp['ref_students'] == 1 && $gp['control_add'] == 1)) { ?><a href="references/add_student.php" target="content_area">Add Student</a><br /><?php }  ?>
		<?php if ($gp['is_VIP'] == 1 || $gp['ref_students'] == 1) { ?><a href="references/search_student.php" target="content_area">Search & View Student</a><br /><?php }  ?>
		<?php if ($gp['is_VIP'] == 1 ) { ?><a href="references/student_list.php" target="content_area">View Student List</a><br /><br/><?php }  ?>
		<?php if ($gp['is_VIP'] == 1 || ($gp['ref_employee'] == 1 && $gp['control_add'] == 1)) { ?><a href="references/add_employee.php" target="content_area">Add Employee</a><br /><?php }  ?>
		<?php if ($gp['is_VIP'] == 1 || $gp['ref_employee'] == 1) { ?><a href="references/search_employee.php" target="content_area">Search & View Employee</a><br /><?php }  ?>
		<?php if ($gp['is_VIP'] == 1 || $gp['ref_class_level'] == 1) { ?><br /><a href="references/class_level.php" target="content_area">Class Levels</a><br /><?php }  ?>
		<?php if ($gp['is_VIP'] == 1 || $gp['ref_class'] == 1) { ?><a href="references/sections.php" target="content_area">Classes / Sections</a><br /><br /><?php }  ?>
		<?php if ($gp['is_VIP'] == 1 || $gp['ref_department'] == 1) { ?><a href="references/department.php" target="content_area">Department</a><br /><?php }  ?>
		<?php if ($gp['is_VIP'] == 1 || $gp['ref_subject_list'] == 1) { ?><a href="references/subjects.php" target="content_area">Subjects</a><?php }  ?>
		<?php if ($gp['is_VIP'] == 1 || $gp['ref_timeslot'] == 1) { ?><br /><a href="references/managetimeslot.php" target="content_area">Timeslot</a><?php }  ?>
		<!-------javascript:popUp('references/manage_fees.php')--->
    </div>
    
  </div>
<!--End of each accordion item--> 

<!--Start of each accordion item-->
  <div id="test2-header" class="accordion_headings" >Loading</div><!--Heading of the accordion ( clicked to show n hide ) -->
  
  <!--Prefix of heading (the DIV above this) and content (the DIV below this) to be same... eg. foo-header & foo-content-->
  
  <div id="test2-content"><!--DIV which show/hide on click of header-->
  
  	<!--This DIV is for inline styling like padding...-->
    <div class="accordion_child">
		<?php if($gp['is_VIP'] || ($gp['ref_grade_item'] == 1 && $gp['ref_grade_item_type'] && $gp['ref_grade'] == 1)) { ?><a href="teacherload.php" target="content_area">Subjects and Advisory Class</a><br /><?php }  ?>
		<!-------javascript:popUp('references/manage_fees.php')--->
    </div>
    
  </div>
<!--End of each accordion item--> 

<?php }

elseif($_SESSION['is_emp'] == 0){
 ?>
 <div id="test-header" class="accordion_headings header_highlight" >Guardian Menu</div><!--Heading of the accordion ( clicked to show n hide ) -->
  
  <!--Prefix of heading (the DIV above this) and content (the DIV below this) to be same... eg. foo-header & foo-content-->
  
  <div id="test-content"><!--DIV which show/hide on click of header-->
  
  	<!--This DIV is for inline styling like padding...-->
    <div class="accordion_child">
		<a href="index.php">Main Page</a><br /><br />
		<?php 
			$querystudents = mysql_query("SELECT s.student_id, CONCAT(s.fname,' ',SUBSTR(s.mname,1,1),'. ',s.lname) fullname FROM connection c LEFT JOIN student s ON c.student_id = s.student_id WHERE c.guardian_id = $userid GROUP BY 1");
			
			while($getstudents = mysql_fetch_array($querystudents)) {
				?>
				<a href="references/view_student.php?id=<?php echo $getstudents[0]; ?>&viewallgrades=1" target="content_area"><?php echo $getstudents[1]; ?></a><br />
				<?php
			}
		?>
	</div>
    
  </div>
  <div id="test1-header" class="accordion_headings" >References</div><!--Heading of the accordion ( clicked to show n hide ) -->
  
  <!--Prefix of heading (the DIV above this) and content (the DIV below this) to be same... eg. foo-header & foo-content-->
  
  <div id="test1-content"><!--DIV which show/hide on click of header-->
  
  	<!--This DIV is for inline styling like padding...-->
    <div class="accordion_child">
		<a href="references/fee_due_date_guardians.php" target="content_area">Payment Due Dates</a><br />
	</div>
    
  </div>
<?php
}
?>


